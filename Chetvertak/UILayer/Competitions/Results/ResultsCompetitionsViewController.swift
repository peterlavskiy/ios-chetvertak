//
//  ResultsCompetitionsViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 06.08.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

struct ResultsCompetitionsFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider
    
    func makeViewController() -> UIViewController {
        let viewController = ResultsCompetitionsViewController(nibName: nil, bundle: nil)
        
        return viewController
    }
    
}

class ResultsCompetitionsViewController: UIViewController {
    
    var headerTopConstraint: NSLayoutConstraint?
    
    let composer = UITableViewComposer()
    
    let service = CompetitionsService()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .none
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = composer
        tableView.delegate = composer
        
        composer.delegate = self
        
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.titleView = TitleViewFactory(title: "Результаты", subtitle: "12-16 августа 2020").make()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "share")?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(shareAction))
        
        class SegmentCommand: CommandProtocol {
            
            func execute() {
                
            }
            
        }
        
        let content = ["Абсолют", "A", "B", "C", "Юниоры", "Женщины"].map {
            UISegment(title: $0, command: SegmentCommand())
        }
        let segmentView = UISegmentView(frame: .init(origin: .zero, size: .init(width: UIScreen.main.bounds.width, height: 52)))
        segmentView.layoutMargins = .init(top: 8.0, left: 16, bottom: 0, right: 16)
        segmentView.configure()
        segmentView.setContent(content)
        segmentView.heightAnchor.constraint(equalToConstant: 52.0).isActive = true
        
        
        let label = UILabel(frame: .zero)
        label.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.firstLineHeadIndent = 16.0
        paragraphStyle.headIndent = 16.0
        paragraphStyle.tailIndent = -16.0
        let attributedString = NSMutableAttributedString(string: "2-ой этап Кубка России")
        attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        label.attributedText = attributedString
        label.preservesSuperviewLayoutMargins = true
        let font = UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold))
        label.configure(.init(font: font, textColor: Color().label))
        
        let headerView = UIView(frame: .zero)
        headerView.backgroundColor = Color().systemBackground
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = .zero
        stackView.distribution = .equalSpacing
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(segmentView)
        
        headerView.addSubview(stackView)
        stackView.edges(to: headerView)
        
        tableView.tableHeaderView = UIView(frame: .init(origin: .zero, size: .init(width: UIScreen.main.bounds.width, height: 100)))
        
        view.addSubview(headerView)
        let headerTopConstraint = headerView.topAnchor.constraint(equalTo: view.topAnchor)
        self.headerTopConstraint = headerTopConstraint
        
        NSLayoutConstraint.activate([
            headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            headerView.rightAnchor.constraint(equalTo: view.rightAnchor),
            headerView.heightAnchor.constraint(equalToConstant: 100),
            headerTopConstraint
        ])
        
        segmentView.setSelectedIndex(0)
        
        loadData()
    }
    
    @objc func shareAction() {
        
        SocialShareCommand(source: self, title: "Результаты, 12-16 августа 2020\n 1 место - Иван Константинов\n 2 место - Борис Кулерасов\n 3 место - Рикардо Пенетти").execute()
        
    }
    
    func loadData() {
        
        service.getResults { [weak self] in
            switch $0 {
            case .success(let entity):
                
                let top = Array(entity.result[...2])
                
                let list = Array(entity.result[3...])
                
                let content = [makeTopSection(top), makeListSection(list)]
                
                self?.composer.setContent(content)
                self?.tableView?.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func makeTopSection(_ content: [CompetitionsResult]) -> UITableViewComponent {
        
        typealias View = UITableViewSectionHeader
        
        let model = UITableViewSectionHeader.Model(title: "")
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold)), textColor: Color().label)
        
        let configuration = UITableViewSectionHeader.UIConfiguration(title: titleConfiguration)
        
        var items = content.enumerated().map(makeCompetitionCell)
        
        items.append(makeSeparator())
        items.append(makeSeparator(backgrounColor: Color().systemGray6, height: 1.0))
        
        let factory = UITableViewFactoryComponent<View>(model: model,
                                                        configuration: configuration,
                                                        estimatedHeight: .zero)
        
        return factory.buildSection(View.self, items: items)
    }
    
    func makeListSection(_ content: [CompetitionsResult]) -> UITableViewComponent {
        
        typealias View = UITableViewSectionHeader
        
        let model = UITableViewSectionHeader.Model(title: "")
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold)), textColor: Color().label)
        
        let configuration = UITableViewSectionHeader.UIConfiguration(title: titleConfiguration)
        
        var items = content.enumerated().map(makeCompetitionCell)
        items.insert(makeSeparator(), at: 0)
        
        let factory = UITableViewFactoryComponent<View>(model: model,
                                                        configuration: configuration,
                                                        estimatedHeight: .zero)
        
        return factory.buildSection(View.self, items: items)
        
    }
    
    func makeSeparator(backgrounColor: UIColor = Color().systemBackground,
                       height: CGFloat = 6.0 ) -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let model = OutputRightTableViewCellModel(title: "")
        
        let configuration = View.UIConfiguration(title: .makeTitle(),
                                                 backgrounColor: backgrounColor)
        
        return UITableViewFactoryComponent<View>(model: model,
                                                 configuration: configuration,
                                                 estimatedHeight: 1.0,
                                                 height: height,
                                                 reuseIdentifier: "Separator")
            .buildCell(View.self)
    }
    
    
    func makeCompetitionCell(offset: Int,
                             content: CompetitionsResult) -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let model = OutputRightTableViewCellModel(title: content.fullName,
                                                  subTitle: makeSubtitle(content),
                                                  value: String(describing: content.index))
        
        let positionFont = UIFontMetrics(forTextStyle: .headline).scaledFont(for: .systemFont(ofSize: 18, weight: .semibold))
        
        let position = String(describing: content.position)
        let size: CGSize = .init(width: position.width(withConstrainedHeight: 22, font: positionFont), height: 22.0)
        let indexImage =
            position.image(withAttributes: [
                .foregroundColor: Color().label,
                .font: positionFont,
            ], size: size)
        
        let imageLayout = LeftImageViewLayout(leadingConstant: 32, traillingConstant: 16.0, topConstant: 27.0, layout: ImageViewLayout(height: size.height, width: size.width))
        let imageProvider = ResultsImageProvider(image: indexImage, leftImageView: imageLayout)
        
        let backgroundColor = Color().systemGray6
        
        let titleConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: .systemFont(ofSize: 15.0, weight: .semibold)), textColor: Color().label, layoutMargins: .init(top: 0, left: -16, bottom: 0, right: 0))
        
        var akcentAttributes: [UILabelStringAttribute]?
        if content.previuosRank != nil {
            let range = (makeSubtitle(content) as NSString).range(of: "→ \(content.currentRank.title)")
            akcentAttributes = [UILabelStringAttribute(attributes:
                [.foregroundColor: Color().systemIndigo], range: range)]
        }
        
        let subtitleConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: .systemFont(ofSize: 15.0, weight: .regular)), textColor: Color(alpha: 0.6).secondaryLabel, attributes: akcentAttributes)
        
        let valueConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .body).scaledFont(for: .systemFont(ofSize: 16.0, weight: .semibold)), textColor: .black, layoutMargins: .init(top: 0.0, left: 0, bottom: 0, right: -16))
        
        let configuration: OutputRightTableViewCellConfiguration = .init(title: titleConfiguration, subTitle: subtitleConfiguration, value: valueConfiguration, valueBackgroundViewColor: makeValueBackgroundColor(content), valueBackgroundRadius: 8.0, valueBackgroundStrokeColor: Color().systemGray4, backgrounColor: backgroundColor, contentRectCorners: [.allCorners], distribution: .fillEqually, margins: .init(top: 0, left: -16.0, bottom: 0, right: 16.0))
        
        return UITableViewFactoryComponent<View>(model: model,
                                                 configuration: configuration,
                                                 estimatedHeight: 98.0,
                                                 height: 82.0,
                                                 imageProvider: imageProvider).buildCell(View.self)
    }
    
    func makeSubtitle(_ content: CompetitionsResult) -> String {
        
        if let previuosRank = content.previuosRank {
            return "\(previuosRank.title) → \(content.currentRank.title)"
        } else {
            return content.currentRank.title
        }
        
    }
    
    func makeValueBackgroundColor(_ content: CompetitionsResult) -> UIColor {
        
        switch content.position {
        case 1:
            return .systemYellow
        case 2:
            return Color().silver
        case 3:
            return .systemOrange
        default:
            return .white
        }
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        loadData()
    }
    
}

extension ResultsCompetitionsViewController: UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
                
        let const = -scrollView.contentOffset.y
        
        let minValue = min(52.0, view.safeAreaInsets.top - 44.0)
        
        let newValue = min(max(minValue, const), view.safeAreaInsets.top)
        
        headerTopConstraint?.constant = newValue

        if (newValue < view.safeAreaInsets.top - const) {
            switchTitle("2-ой этап Кубка России")
        } else {
            switchTitle("Результаты", subtitle: "12-16 августа 2020")
        }
        
    }
    
    private func switchTitle(_ title: String, subtitle: String? = nil) {
        navigationItem.titleView = TitleViewFactory(title: title, subtitle: subtitle).make()
    }
    
}

fileprivate extension Color {
    
    var silver: UIColor {
        
        let anyLight = Color().systemGray4
        let darkColor = Color().systemGray
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return darkColor
                } else {
                    return anyLight
                }
            }
        } else {
            return anyLight
        }
    }
    
}


fileprivate extension Color {
    
    var highlighter: UIColor {
        
        let anyLight = UIColor(red: 0.898, green: 0.937, blue: 1, alpha: 1)
        let darkColor = UIColor(red: 0.084, green: 0.124, blue: 0.267, alpha: 1)
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return darkColor
                } else {
                    return anyLight
                }
            }
        } else {
            return anyLight
        }
    }
    
}

struct CompetitionResultResponseEntity: Codable {
    let result: [CompetitionsResult]
}

struct CompetitionsResult: Codable {
    let id: Int
    let name, lastName, fullName: String
    let position, index: Int
    let currentRank: Category
    let previuosRank: Category?
    let category: Category
}

// MARK: - Category
struct Category: Codable {
    let id: Int
    let title: String
}

extension String {
    
    /// Generates a `UIImage` instance from this string using a specified
    /// attributes and size.
    ///
    /// - Parameters:
    ///     - attributes: to draw this string with. Default is `nil`.
    ///     - size: of the image to return.
    /// - Returns: a `UIImage` instance from this string using a specified
    /// attributes and size, or `nil` if the operation fails.
    func image(withAttributes attributes: [NSAttributedString.Key: Any]? = nil, size: CGSize? = nil) -> UIImage? {
        let size = size ?? (self as NSString).size(withAttributes: attributes)
        return UIGraphicsImageRenderer(size: size).image { _ in
            (self as NSString).draw(in: CGRect(origin: .zero, size: size),
                                    withAttributes: attributes)
        }
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
