//
//  UICollectionViewComposer.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 04.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class UICollectionViewComposer: NSObject {
        
    var content: [UICollectionViewComponent]
    
    weak var delegate: UICollectionViewDelegate?
    
    init(delegate: UICollectionViewDelegate? = nil,
         content: [UICollectionViewComponent] = []) {
        
        self.content = content
        self.delegate = delegate
    }
    
    func setContent(_ content: [UICollectionViewComponent]) {
        
        self.content = content
    }
    
}

extension UICollectionViewComposer: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        content.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = content[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withItem: item, for: indexPath)
        
        return item.make(cell)
    }
    
}

extension UICollectionViewComposer: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.collectionView?(collectionView, didSelectItemAt: indexPath)
        content[indexPath.row].command?.execute()
    }
    
}

extension UICollectionViewComposer: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        content[indexPath.row].itemSize
    }
}

fileprivate extension UICollectionView {
    
    func dequeueReusableSupplementaryView(withItem item: UIReusableItemProtocol,
                                          for indexPath: IndexPath) {
        
        
    }
    
    func dequeueReusableCell(withItem item: UIReusableItemProtocol,
                             for indexPath: IndexPath) -> UICollectionViewCell {
        
        registerCell(withItem: item)
        
        return dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath)
    }
    
    func registerHeader(withItem item: UIReusableItemProtocol) {
        
        if let nib = item.nib {
            
        }
        
        if let cellClass = item.cellClass {
            
        }
        
    }
    
    func registerCell(withItem item: UIReusableItemProtocol) {
                
        if let nib = item.nib {
            register(nib, forCellWithReuseIdentifier: item.reuseIdentifier)
        }
        
        if let cellClass = item.cellClass {
            register(cellClass, forCellWithReuseIdentifier: item.reuseIdentifier)
        }
        
    }
}
