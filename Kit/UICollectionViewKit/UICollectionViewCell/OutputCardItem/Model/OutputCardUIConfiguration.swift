//
//  OutputCardUIConfiguration.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol OutputCardFooterUIProtocol {
    
    var title: UILabelConfiguration { get }
    var titleMargins: UIEdgeInsets { get }

    var subtitle: UILabelConfiguration { get }
    var subtitleMargins: UIEdgeInsets { get }
    
    var subvalue: UILabelConfiguration { get }
    
    var teriary: UILabelConfiguration { get }
    var teriaryMargins: UIEdgeInsets { get }

    var backgroundColor: UIColor? { get }
    
    func makeAccessoryView(in contentView: UIView) -> UIView?
}

extension OutputCardFooterUIProtocol {
    
    func makeAccessoryView(in contentView: UIView) -> UIView? {
        nil
    }
    
}

struct OutputCardFooterTeriaryAccessory: OutputCardFooterUIProtocol {
    
    var title: UILabelConfiguration = .makeTitle()
    var titleMargins: UIEdgeInsets = .zero

    var subtitle: UILabelConfiguration = .makeSubtitle()
    var subtitleMargins: UIEdgeInsets = .zero
    
    var subvalue: UILabelConfiguration = .makeSubtitle()
    
    var teriary: UILabelConfiguration = .makeTeriary()
    var teriaryMargins: UIEdgeInsets = .zero

    var backgroundColor: UIColor? = Color().systemBackground
    
    let controls: [ControlFactory]
    
    func makeAccessoryView(in contentView: UIView) -> UIView? {
        
        let view = ControlContainerView()
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        view.alpha = 0.0
        
        NSLayoutConstraint.activate([
            view.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16),
            view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8.0),
            view.widthAnchor.constraint(equalToConstant: 200),
            view.heightAnchor.constraint(equalToConstant: 26)

        ])
        
        view.configure(controls: controls)
        
        return view
    }
    
}

struct OutputCardUIConfiguration {
    
    struct Header {
        var title: UILabelConfiguration = .makeTitle()
        var subtitle: UILabelConfiguration = .makeSubtitle()
    }
    
    struct Footer: OutputCardFooterUIProtocol {
        var title: UILabelConfiguration = .makeTitle()
        var titleMargins: UIEdgeInsets = .zero

        var subtitle: UILabelConfiguration = .makeSubtitle()
        var subtitleMargins: UIEdgeInsets = .zero
        
        var subvalue: UILabelConfiguration = .makeSubtitle()
        
        var teriary: UILabelConfiguration = .makeTeriary()
        var teriaryMargins: UIEdgeInsets = .zero

        var backgroundColor: UIColor? = Color().systemBackground
        var titleLeading: CGFloat = 0.0
    }
    
    var footer: OutputCardFooterUIProtocol?
    var header: Header?
    var gradient: GradientFactoryProtocol?
    var imageGradient: GradientFactoryProtocol?
    var cornerRadii: CGFloat = 16.0
    
    var isHiddenHeader: Bool = true
    var isHiddenFooter: Bool = false
    var isRounded: Bool = true
    var isSmooth: Bool = false
    
    var backgroundColor: UIColor?
    var backgroundImageHeight: CGFloat?
    var backgroundImageCornerRadii: CGFloat = .zero
    var shadowFactory: ShadowFactory?
    var highlighter: Highlighter?

}

extension OutputCardUIConfiguration {
    
    static func makeShop(backgroundColor: UIColor = Color().systemBackground) -> OutputCardUIConfiguration {
        
        let title = UILabelConfiguration(font: .preferredFont(forTextStyle: .headline), textColor: Color().label)
        
        let subtitle = UILabelConfiguration(font: UIFont.preferredFont(forTextStyle: .subheadline).withSize(14.0), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let footer = OutputCardUIConfiguration.Footer(title: title,
                                                      subtitle: subtitle)
        
        return OutputCardUIConfiguration(footer: footer,
                                         gradient: nil,
                                         isHiddenHeader: true,
                                         isRounded: false,
                                         isSmooth: true,
                                         backgroundColor: backgroundColor,
                                         backgroundImageHeight: 156.0)
        
    }
    
    static func makeFintech(gradient: GradientFactoryProtocol?,
                            backgroundColor: UIColor? = nil) -> OutputCardUIConfiguration {
        
        // Нужен декоратор
        
        return OutputCardUIConfiguration(footer: makeFooter(),
                                         header: makeHeader(),
                                         gradient: gradient,
                                         isHiddenHeader: false,
                                         isSmooth: true,
                                         backgroundColor: backgroundColor)
    }
    
    private static func makeHeader() -> OutputCardUIConfiguration.Header {
        
        let textColor: UIColor = .white
        let font = UIFont.preferredFont(forTextStyle: .subheadline).withSize(14.0)
        
        let subtitle: UILabelConfiguration = .makeSubtitle(font: font,
                                                           textColor: textColor)
        
        return OutputCardUIConfiguration.Header(subtitle: subtitle)
        
    }
    
    private static func makeFooter() -> OutputCardUIConfiguration.Footer {
        
        let titleColor: UIColor = .white
        var titleFont = UIFont.preferredFont(forTextStyle: .title1).withSize(28.0)
        if #available(iOS 11.0, *) {
            titleFont = UIFontMetrics(forTextStyle: .title1).scaledFont(for: .systemFont(ofSize: 28, weight: .bold))
        }
        
        let subtitleColor: UIColor = .init(white: 1.0, alpha: 0.6)
        let subtitleFont = UIFont.preferredFont(forTextStyle: .subheadline).withSize(14.0)
        
        let title: UILabelConfiguration = .makeTitle(font: titleFont, textColor: titleColor)
        
        var subtitle = UILabelConfiguration.makeSubtitle(font: subtitleFont, textColor: subtitleColor)
        let subtitleMargins = UIEdgeInsets(top: -8, left: 0, bottom: -8, right: 0)
        subtitle.layoutMargins = subtitleMargins
        
        return OutputCardUIConfiguration.Footer(title: title,
                                                subtitle: subtitle,
                                                subtitleMargins: subtitleMargins,
                                                subvalue: subtitle,
                                                backgroundColor: .clear,
                                                titleLeading: 19.0)
    }
    
}
