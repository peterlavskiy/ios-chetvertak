//
//  UITableViewHeaderBuilder.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UITableViewHeaderFactory<View: ConfigurableSection>: UITableViewSectionBuilderProtocol {

    let builder: UITableViewComponentFactoryProtocol
    let items: [UITableViewComponent]
    var nib: UINib?
    var cellClass: AnyClass.Type?
    
    var numberOfItems: Int { items.count }
    var estimatedHeight: CGFloat { builder.estimatedHeight }
    var height: CGFloat { builder.height }
    var reuseIdentifier: String { builder.makeReuseIdentifier() }
    var reuseItem: UIReusableItemProtocol { builder.reuseItem }

    subscript(_ index: Int) -> UITableViewComponent {
        items[index]
    }
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView? {
        builder.makeReusableView(view)
    }
    
    func make(_ view: UITableViewHeaderFooterView?) -> UITableViewHeaderFooterView? {
        guard estimatedHeight != .zero else { return nil }
        return builder.makeReusableView(view) as? View
    }
    
    func makeReuseIdentifier() -> String {
        builder.makeReuseIdentifier()
    }
    
    func build() -> UITableViewComponent {
        
        UITableViewCompositeItem(builder: self, reuseItem: reuseItem)
    }
}
