//
//  SecondaryButton.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct SecondaryButton: ButtonType {
    
    let layout: ButtonLayout
    let model: ButtonModel
    let textColor: UIColor = .systemBlue
    let backgroundColor: UIColor = Color(alpha: 0.12).tertiarySystemFill
    let strokeColor: UIColor? = Color().systemGray5
    var state = ButtonState()

    var title: UILabelConfiguration {
        UILabelConfiguration(font: layout.titleFont, textColor: textColor)
    }
    
    func make() -> Button {
        let button = Button(layout: layout, type: self, controlState: state)
        button.configure(model: model)
        return button
    }
}
