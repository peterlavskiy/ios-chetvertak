//
//  UIOutputTableViewCellProtocol.swift
//  PromoUI
//
//  Created by peter.lavskiy on 05/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import Foundation

protocol UIOutputTableViewCellProtocol {
    
    associatedtype Model
    
    associatedtype UIConfiguration
    
    func configure(_ model: Model)
    
    func configureUI(_ configuration: UIConfiguration)
    
}
