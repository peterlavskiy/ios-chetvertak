//
//  OuputCardView.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol OutputCardHeaderViewProtocol: UIView {}

protocol OuputCardViewProtocol: UIView {
    
    typealias Model = OutputCardModelProtocol
        
    func configure(_ model: Model)
    
    func configure(_ footer: CardFooterBuilderProtocol?)
    
    func configure(_ header: CardHeaderBuilderProtocol?)
    
    func setFooterAccessoryViewHidden(_ isHidden: Bool)

}

class OuputCardView: UIView, OuputCardViewProtocol {
        
    @IBOutlet weak var backgroundImageView: UIImageView! {
        didSet {
            backgroundImageView.contentMode = .scaleAspectFill
        }
    }
    
    @IBOutlet weak var backgroundImageHeight: NSLayoutConstraint!
    
    var cornerRadius: CGFloat = 16.0
    
    var isSmoothConfiguration = false
    
    weak var headerView: OutputCardHeaderViewProtocol?
    
    weak var footerView: OutputCardFooterViewProtocol?
    
    var shadowFactory: ShadowFactory?
    
    func configure(_ model: Model) {
        
    }
    
    func configure(_ footer: CardFooterBuilderProtocol?) {
        
        footerView = footer?.build(footerView,
                                   in: self,
                                   align: backgroundImageView.bottomAnchor)
        footerView?.backgroundColor = Color().systemBackground
        
    }
    
    func configure(_ header: CardHeaderBuilderProtocol?) {
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func makeShadows() {
        
    }
    
    func setFooterAccessoryViewHidden(_ isHidden: Bool) {
        footerView?.setAccessoryViewHidden(isHidden)
    }

}

struct ShadowFactory {
    
    var colorProvider: ColorProvider?
    let cornerRadius: CGFloat = 16.0
    
    func make(in layer: CALayer, bounds: CGRect? = nil) {
        
        layer.shadowColor = colorProvider?.isDark == true ? UIColor.clear.cgColor : Color().shadowColor.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 5.0)
        layer.shadowRadius = 24.0
        layer.shadowOpacity = 1
        layer.shadowPath = UIBezierPath(roundedRect: bounds ?? layer.bounds,
                                        cornerRadius: cornerRadius).cgPath
        layer.masksToBounds = false
    }
}
