//
//  UITableViewCompositeItem.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public struct UITableViewCompositeItem: UITableViewComponent {
    
    let reuseItem: UIReusableItemProtocol

    let builder: UITableViewSectionBuilderProtocol
    
    public var component: UITableViewComponentFactoryProtocol { builder }
            
    public var numberOfItems: Int { builder.numberOfItems }
    
    public var nib: UINib? { reuseItem.nib }
    
    public var cellClass: AnyClass.Type? { reuseItem.cellClass }
    
    public init (builder: UITableViewSectionBuilderProtocol,
                 reuseItem: UIReusableItemProtocol) {
        self.builder = builder
        self.reuseItem = reuseItem
    }
    
    public func make(_ view: UITableViewHeaderFooterView?) -> UITableViewHeaderFooterView? {
        builder.make(view)
    }
    
    public subscript(_ index: Int) -> UITableViewComponent {
        builder[index]
    }
}
