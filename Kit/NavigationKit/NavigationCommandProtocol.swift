//
//  NavigationCommandProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol NavigationCommandProtocol: CommandProtocol {
    
    var source: UIViewController? { get }
    var presented: UIViewController? { get }
    
    func makePresented()
}
