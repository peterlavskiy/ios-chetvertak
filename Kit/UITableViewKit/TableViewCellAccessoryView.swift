//
//  TableViewCellAccessoryView.swift
//  PromoUI
//
//  Created by peter.lavskiy on 04/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import UIKit

enum CellAccessoryType {
    
    case approved(UIImage)
    case transition(UIImage)
    
}

class TableViewCellAccessoryView: UIView {

    var imageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
