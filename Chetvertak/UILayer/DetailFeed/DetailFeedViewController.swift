//
//  DetailFeedViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 25.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

class DetailFeedViewController: UIViewController, CardDetailTransionable {
    
    lazy var cardBottomToRootBottomConstraint: NSLayoutConstraint = {
        tableView.tableHeaderView!.bottomAnchor.constraint(equalTo: cardView!.bottomAnchor)
    }()
    
    lazy var cardTopToRootTopConstraint: NSLayoutConstraint = {
        view.topAnchor.constraint(equalTo: cardView!.topAnchor)
    }()
    
    var cardBottomAnchor: NSLayoutYAxisAnchor {
        cardView?.bottomAnchor ?? NSLayoutYAxisAnchor()
    }
    
    var scrollView: UIScrollView? {
        tableView
    }
    
    var cardHeightAnchor: NSLayoutConstraint = NSLayoutConstraint()
    
    var cardWidthAnchor: NSLayoutConstraint = NSLayoutConstraint()
    
    let composer: UITableViewComposerProtocol
    
    var cardView: OuputCardView?
    
    var dismissButton: UIView?
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .none
            tableView.showsVerticalScrollIndicator = false
        }
    }
    
    init(composer: UITableViewComposer) {
        
        self.composer = composer
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalPresentationCapturesStatusBarAppearance = true
        
        tableView.tableHeaderView = cardView
        
        if let headerView = cardView {
             cardWidthAnchor = headerView.widthAnchor.constraint(equalTo: tableView.widthAnchor)
             NSLayoutConstraint.activate([
                 cardWidthAnchor
             ])
         }

        let dismissButton = UIButton(frame: .zero)
        dismissButton.setImage(UIImage(named: "ic_Close_Active_32_Light"), for: .normal)
        dismissButton.setTitleColor(.red, for: .normal)
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.addTarget(self, action: #selector(dismissAction), for: .touchUpInside)
        view.addSubview(dismissButton)
        
        NSLayoutConstraint.activate([
            dismissButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 32),
            dismissButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            dismissButton.widthAnchor.constraint(equalToConstant: 44.0),
            dismissButton.heightAnchor.constraint(equalToConstant: 44.0),
        ])
        
        self.dismissButton = dismissButton
        
        tableView.dataSource = composer
        tableView.delegate = composer
    }
    
    override func setNeedsStatusBarAppearanceUpdate() {
        super.setNeedsStatusBarAppearanceUpdate()
        cardView?.setFooterAccessoryViewHidden(false)
    }
    
    @objc func dismissAction() {
        cardView?.setFooterAccessoryViewHidden(true)
        dismiss(animated: true, completion: nil)
    }
    
    func loadData() {
        makeProductPage()
    }
    
    func viewWillDissmiss() {
        
    }
    
    func transitionCompleteAnimations() {
        
    }
    
    func makeProductPage() {
        
        typealias Section = UITableViewSectionHeader
        
        let model = UITableViewSectionHeaderModel(title: "")
        
        let configuration = UITableViewSectionHeaderStyle()
        
        let overview = "Хотите узнать больше? Спросите нас! Стрелковый фестиваль «Брянский снайпер» ! Приглашаем желающих поучаствовать в соревнованиях по высокоточной стрельбе из мощного пневматического оружия «Брянский снайпер» и провести стрелковые выходные. Погулять на свежем воздухе, поесть шашлык или пообедать в кафе и ресторанах. Параллельно можно пострелять из различных пневматических винтовок в соседней галерее. Это может быть хорошей тренировкой перед стрельбой в зачет. Задача турнира — вовлечение в стрелковые виды спорта, культивирование семейственности в спорте и досуге. Патриотическое воспитание молодежи, а главное — обучение культуре обращения с оружием. ВРЕМЯ ПРОВЕДЕНИЯ ТУРНИРА 10 АВГУСТА 2019 ГОДА, СУББОТА С 11 ДО 18 ЧАСОВ"
        
        let items = [makeOverview(overview)]
        
        let mainSection = UITableViewFactoryComponent<Section>(model: model, configuration: configuration, estimatedHeight: .zero).buildSection(Section.self, items: items)
        
        composer.setContent([mainSection])
        tableView.reloadData()
    }
    
    func makeOverview(_ overview: String) -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let model = OutputRightTableViewCellModel(title: overview)
        
        let configuration = OutputRightTableViewCellConfiguration(title: .makeTitle(font: UIFont.preferredFont(forTextStyle: .body).withSize(17.0), textColor: Color().label))
        
        return UITableViewFactoryComponent<View>(model: model, configuration: configuration, estimatedHeight: 100.0).buildCell(View.self)
    }
    
    override var prefersStatusBarHidden: Bool {
        true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        .slide
    }

}
