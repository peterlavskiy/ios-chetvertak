//
//  OutlineButton.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct OutlineButton: ButtonType {
    
    let layout: ButtonLayout
    let model: ButtonModel
    let textColor: UIColor = Color().brandBlue
    let strokeColor: UIColor? = Color().brandBlue
    let state = ButtonState()

    var title: UILabelConfiguration {
        UILabelConfiguration(font: layout.titleFont, textColor: textColor)
    }
    func make() -> Button {
        let button = Button(layout: layout, type: self, controlState: state)
        button.configure(model: model)
        return button
    }
}
