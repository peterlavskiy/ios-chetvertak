//
//  ComponentProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 22.02.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public protocol UITableViewSectionBuilderProtocol: UITableViewComponentFactoryProtocol, UITableViewCompositeItemProtocol {}

public protocol UITableViewCellBuilderProtocol: UITableViewComponentFactoryProtocol, UITableViewItemProtocol {}

public protocol UITableViewItemProtocol: UIReusableComponentProtocol {
            
    func make(_ view: UITableViewCell?) -> UITableViewCell

}

public protocol UITableViewCompositeItemProtocol: UIReusableComponentProtocol {
            
    var numberOfItems: Int { get }
    
    subscript(_ index: Int) -> UITableViewComponent { get }
    
    func make(_ view: UITableViewHeaderFooterView?) -> UITableViewHeaderFooterView?

}

public protocol UIReusableComponentProtocol {
    
    var reuseIdentifier: String { get }
        
}

public protocol UITableViewComponent: UITableViewCompositeItemProtocol,
UITableViewItemProtocol, UIReusableItemProtocol {
    
    var component: UITableViewComponentFactoryProtocol { get }
    
    var command: CommandProtocol? { get }
    
}

public extension UITableViewComponent {

    var estimatedHeight: CGFloat { component.estimatedHeight }
    var height: CGFloat { component.height }
    var reuseIdentifier: String { component.reuseIdentifier }

    var numberOfItems: Int { 0 }
    
    var command: CommandProtocol? { nil }

    subscript(_ index: Int) -> UITableViewComponent { self }

    func make(_ cell: UITableViewCell?) -> UITableViewCell {
        assertionFailure("Implementation needed")
        return UITableViewCell()
    }

    func make(_ view: UITableViewHeaderFooterView?) -> UITableViewHeaderFooterView? {
        assertionFailure("Implementation needed")
        return nil
    }
}
