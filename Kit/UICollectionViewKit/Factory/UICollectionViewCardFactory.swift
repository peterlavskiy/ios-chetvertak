//
//  UICollectionViewCardFactory.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 17.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UICollectionViewCardFactory<View: CollectionViewCardProtocol>: UICollectionViewItemFactoryProtocol {
    
    let builder: UICollectionViewComponentFactoryProtocol
    
    var command: CommandProtocol?
        
    var isSelected: Bool { builder.isSelected }
    var reuseItem: UIReusableItemProtocol { builder.reuseItem }

    var footer: CardFooterBuilderProtocol?
    var header: CardHeaderBuilderProtocol?
    
    func make(_ view: UICollectionViewCell?) -> UICollectionViewCell {
        let view = builder.makeReusableView(view) as? View ?? View()
        view.configure(footer: footer)
        view.configure(header: header)
        (command as? CardPresentCommand)?.initialView = view
        return view
    }
    
    func build() -> UICollectionViewComponent {
        UICollectionViewItem(reuseItem: reuseItem, builder: self)
    }
}
