//
//  UIOutputTableViewCellConfigurable.swift
//  PromoUI
//
//  Created by peter.lavskiy on 05/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import UIKit

enum UIOutputConfigurationError: Error {
    case invalidType(String)
}

protocol UIOutputTableViewCellConfigurable: UITableViewCell {
    
    func configure<Model>(model: Model)
    
    func configureUI<UIConfiguration>(configuration: UIConfiguration)
    
}

extension UIOutputTableViewCellConfigurable where Self: UIOutputTableViewCellProtocol {
    
    func configure<Model>(model: Model) {
        
        do {
            guard let model = model as? Self.Model else { throw
                UIOutputConfigurationError.invalidType("Invalid model type. Use model type from Cell") }
            
            self.configure(model)
            
        } catch {
            assertionFailure("\(error.localizedDescription)")
        }
    }
    
    func configureUI<UIConfiguration>(configuration: UIConfiguration) {
        
        do {
            guard let configuration = configuration as? Self.UIConfiguration else {
                throw UIOutputConfigurationError.invalidType("Invalid configuration type. Use configuration type from Cell") }
            
            self.configureUI(configuration)
            
        } catch {
            assertionFailure("\(error.localizedDescription)")
        }
    
    }
    
}
