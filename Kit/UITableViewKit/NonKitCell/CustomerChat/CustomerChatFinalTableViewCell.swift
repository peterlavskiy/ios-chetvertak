//
//  CustomerChatFinalTableViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 05.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct CustomerChatFinalCellConfiguration {
    
    let selectionStyle: UITableViewCell.SelectionStyle = .none
    
    let title: UILabelConfiguration
    
    let subtitle: UILabelConfiguration
}

struct CustomerChatFinalCellModel: ConfigurableImageModel {
    
    let model: EmtyViewModel
    
}

class CustomerChatFinalTableViewCell: UITableViewCell, ConfigurableCell {

    typealias Model = CustomerChatFinalCellModel
    
    typealias UIConfiguration = CustomerChatFinalCellConfiguration
    
    static var reuseIdentifier: String {
        String(describing: CustomerChatFinalTableViewCell.self)
    }
    
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    func configure(_ model: Model, imageProvider: ImageProviderProtocol?) {
        titleLabel.text = model.model.title
        subtitleLabel.text = model.model.subtitle
        topImageView.image = model.model.image
    }
    
    func configureUI(_ configuration: UIConfiguration) {
        titleLabel.configure(configuration.title)
        subtitleLabel.configure(configuration.subtitle)
        selectionStyle = configuration.selectionStyle
    }
    
}
