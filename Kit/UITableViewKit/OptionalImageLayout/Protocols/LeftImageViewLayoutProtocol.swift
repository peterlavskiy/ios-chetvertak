//
//  LeftImageViewLayoutProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 12.04.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol LeftImageViewLayoutProtocol: OptionalImageViewLayoutProtocol {
    
    func make(in superview: UIView,
              centerAnchor: NSLayoutYAxisAnchor?,
              leadingAnchor: NSLayoutXAxisAnchor,
              inactive: [NSLayoutConstraint?]) -> UIImageView?
    
}
