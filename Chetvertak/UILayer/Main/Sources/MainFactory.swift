//
//  MainFactory.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 25.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

struct MainFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider 
    
    func makeViewController() -> UIViewController {
                
        let viewController = MainViewController(colorProvider: colorProvider)
        colorProvider.views.add(viewController)
        viewController.tabBarItem.title = "Новости"
        viewController.tabBarItem.selectedImage = UIImage(named: "news_active_32")?.withRenderingMode(.alwaysTemplate)
        viewController.tabBarItem.image = UIImage(named: "news_normal_32")?.withRenderingMode(.alwaysTemplate)
        return viewController
    }
    
}
