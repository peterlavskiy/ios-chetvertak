//
//  GradientFactoryProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 22.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol GradientFactoryProtocol {
    
    func make(in view: UIView) -> CAGradientLayer
}
