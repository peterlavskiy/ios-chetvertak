//
//  CenteredImageViewLayoutProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 12.04.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import Foundation

protocol CenteredImageViewLayoutProtocol: OptionalImageViewLayoutProtocol {}
