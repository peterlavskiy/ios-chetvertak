//
//  SceneDelegate.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 19.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate, LaunchSplashable {

    var window: UIWindow?

    lazy var colorProvider: ColorProvider = {
        
        var isDark = false
        if #available(iOS 13.0, *) {
            isDark = UIScreen.main.traitCollection.userInterfaceStyle == .dark
        }
        
        return ColorProvider(tint: .blue,
                            brand: Color().brandBlue,
                            isDark: isDark)
        
    }()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        makeLaunch()

        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }
    
    func windowScene(_ windowScene: UIWindowScene, didUpdate previousCoordinateSpace: UICoordinateSpace, interfaceOrientation previousInterfaceOrientation: UIInterfaceOrientation, traitCollection previousTraitCollection: UITraitCollection) {
        
        colorProvider.change(UITraitCollection.current)
    }

}

protocol LaunchSplashable {
    
    var window: UIWindow? { get }
    
    var colorProvider: ColorProvider { get }
}

extension LaunchSplashable {
    
    func makeLaunch() {
        
        let splashViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? ViewController
        splashViewController?.colorProvider = colorProvider
        
        window?.rootViewController = RootFactory(colorProvider: colorProvider).make()
    }
}
