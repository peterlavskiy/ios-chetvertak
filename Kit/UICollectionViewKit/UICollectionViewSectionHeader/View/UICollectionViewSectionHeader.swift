//
//  UICollectionViewSectionHeader.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 25.05.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol CollectionViewReusableHeader: UICollectionReusableView, ReusableView {}


class UICollectionViewSectionHeader: UICollectionReusableView, CollectionViewReusableHeader {
        
    typealias Model = UICollectionViewSectionHeaderModel
    
    typealias UIConfiguration = UICollectionViewSectionHeaderConfiguration
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    static var reuseIdentifier: String { String(describing: UICollectionViewSectionHeader.self) }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(_ model: Model, imageProvider: ImageProviderProtocol?) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
    }
    
    func configureUI(_ configuration: UIConfiguration) {
        titleLabel.configure(configuration.title)
        subtitleLabel.configure(configuration.subtitle)
    }
    
}
