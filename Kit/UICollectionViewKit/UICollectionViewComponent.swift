//
//  UICollectionViewComponent.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public protocol UICollectionViewItemProtocol: UIReusableComponentProtocol {
            
    func make(_ view: UICollectionViewCell?) -> UICollectionViewCell

}

public protocol UICollectionViewCompositeItemProtocol: UIReusableComponentProtocol {
            
    var numberOfItems: Int { get }
    
    subscript(_ index: Int) -> UICollectionViewComponent { get }
    
    func make(_ view: UICollectionReusableView?) -> UICollectionReusableView

}

public protocol UICollectionViewComponent: UICollectionViewItemProtocol, UICollectionViewCompositeItemProtocol, UIReusableItemProtocol {
    
    var component: UICollectionViewComponentFactoryProtocol { get }
    var command: CommandProtocol? { get }
    
}

public extension UICollectionViewComponent {
    
    var insets: UIEdgeInsets { component.insets }
    var estimatedSize: CGSize { component.estimatedSize }
    var itemSize: CGSize { component.itemSize }
    var reuseIdentifier: String { component.reuseIdentifier }
    var isSelected: Bool { component.isSelected }
    var command: CommandProtocol? { nil }

    var numberOfItems: Int { 1 }
    
    subscript(index: Int) -> UICollectionViewComponent {
        self
    }
    
    func make(_ view: UICollectionViewCell?) -> UICollectionViewCell {
        UICollectionViewCell()
    }
    
    func make(_ view: UICollectionReusableView?) -> UICollectionReusableView {
        UICollectionReusableView()
    }
}
