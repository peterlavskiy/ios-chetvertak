//
//  CommandProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 31.03.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import Foundation

public protocol CommandProtocol: AnyObject {
    
    func execute()
}
