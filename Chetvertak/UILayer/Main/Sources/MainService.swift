//
//  MainService.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 22.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import Foundation

class MainService {
    
    func getFeed(_ completion: (Result<MainResult, Error>) -> Void) {
        
        if let path = Bundle.main.path(forResource: "Feed", ofType: "json") {
            do {
                
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
    
                let decoded = try JSONDecoder().decode(MainResult.self, from: data)
                
                completion(.success(decoded))
                
            } catch {
                completion(.failure(error))
            }
        }
        
    }
    
    func getBookmarks(_ completion: (Result<MainResult, Error>) -> Void) {
        
        if let path = Bundle.main.path(forResource: "Bookmarks", ofType: "json") {
            do {
                
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
    
                let decoded = try JSONDecoder().decode(MainResult.self, from: data)
                
                completion(.success(decoded))
                
            } catch {
                completion(.failure(error))
            }
        }
        
    }
    
}

struct MainResult: Codable {
    let result: [MainContent]
}

struct MainContent: Codable {
    
    let id: Int
    let title: String
    let subtitle: String?
    let date: String
    let image: String
    
}
