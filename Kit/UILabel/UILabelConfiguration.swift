//
//  UILabelConfiguration.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct Currency {
    
    let value: Int
    let pennies: Int
    
}

struct UILabelStringAttribute {
    
    let attributes: [NSAttributedString.Key: Any]?
    
    var range: NSRange?
}

struct LabelBackground {
    
    let strokeColor: UIColor
    let backGroundColor: UIColor
}

struct UILabelConfiguration {
    
    var font: UIFont
    
    var textColor: UIColor
            
    var layoutMargins: UIEdgeInsets = .zero
    
    var attributes: [UILabelStringAttribute]?
    
    var numberOfLines: Int = .zero
    
}

extension UILabelConfiguration {
    
    static func makeTitle(font: UIFont? = nil,
                          textColor: UIColor? = nil) -> UILabelConfiguration {
        let font = font ?? .preferredFont(forTextStyle: .title3)
        return UILabelConfiguration(font: font,
                                    textColor: textColor ?? Color().label)
    }
    
    static func makeSubtitle(font: UIFont? = nil,
                             textColor: UIColor? = nil) -> UILabelConfiguration {
        let font = font ?? .preferredFont(forTextStyle: .title3)
        return UILabelConfiguration(font: font,
                                    textColor: textColor ?? Color().systemGray)
    }
    
    static func makeTeriary(font: UIFont? = nil,
                            textColor: UIColor? = nil) -> UILabelConfiguration {
        let font = font ?? UIFont.preferredFont(forTextStyle: .subheadline).withSize(14.0)
        return UILabelConfiguration(font: font,
                                    textColor: textColor ?? Color(alpha: 0.3).tertiaryLabel)
    }
    
}
