//
//  ImageViewCenteredLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 12.04.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct ImageViewCenteredLayout: CenteredImageViewLayoutProtocol {
        
    let layout: OptionalImageViewLayoutProtocol
    
    func make(in superview: UIView) -> UIImageView? {
                
        let view = layout.make(in: superview)
        
        let constraint = view?.centerYAnchor.constraint(equalTo: superview.centerYAnchor)
                
        constraint?.isActive = true
        
        return view
    }
}
