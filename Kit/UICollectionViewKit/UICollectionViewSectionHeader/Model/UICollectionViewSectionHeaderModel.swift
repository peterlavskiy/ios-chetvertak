//
//  UICollectionViewSectionHeaderModel.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 25.05.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import Foundation

struct UICollectionViewSectionHeaderModel: ConfigurableImageModel {
    
    var title: String?
    var subtitle: String?
}

struct UICollectionViewSectionHeaderConfiguration {
    
    var title: UILabelConfiguration = .makeTitle()
    var subtitle: UILabelConfiguration = .makeSubtitle()
}
