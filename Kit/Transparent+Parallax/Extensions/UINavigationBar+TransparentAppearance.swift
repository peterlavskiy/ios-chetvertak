//
//  UINavigationBar+TransparentAppearance.swift
//
//  Created by Lavskiy Peter on 31.03.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//
import UIKit

public extension UINavigationBar {
    
    func enableTransparency(alpha: CGFloat = 0.0) {
        
        let color = UIColor.white.withAlphaComponent(alpha)
        let tintColor = UIColor.white.withAlphaComponent(1.0)
        let titleColor = UIColor.clear.withAlphaComponent(alpha)

        let transparentBackground = makeTransparentBackground(color: color, alpha: alpha)
        
        setBackgroundImage(transparentBackground, for: .default)
        
        applyTransparentBackgroundToNavigationTitle(color: tintColor,
                                                    titleColor: titleColor,
                                                    alpha: alpha)
    }
    
    func disableTransparency(alpha: CGFloat = 1.0) {
        
        let tintColor = UIColor.black.withAlphaComponent(alpha)
        let color = UIColor.white.withAlphaComponent(alpha)
        let titleColor = UIColor.black
        
        let transparentBackground = makeTransparentBackground(color: color, alpha: alpha)
        
        setBackgroundImage(transparentBackground, for: .default)
        
        applyTransparentBackgroundToNavigationTitle(color: tintColor,
                                                    titleColor: titleColor,
                                                    alpha: alpha)
    }
    
    func applyTransparentBackground(color: UIColor = .white,
                                    alpha: CGFloat,
                                    isStatusBarUpdating: Bool) {
                
        assert(isTranslucent, "navigationBar.isTranslucent couldn't be false")
                
        if isStatusBarUpdating {
            disableTransparency()
        } else {
            enableTransparency(alpha: alpha)
        }

    }
    
    private func makeTransparentBackground(color: UIColor, alpha: CGFloat) -> UIImage? {
        
        guard alpha < 1.0 else { return nil }
        
        var transparentBackground: UIImage
            
        /*    The background of a navigation bar switches from being translucent
            to transparent when a background image is applied. The intensity of
               the background image's alpha channel is inversely related to the
              transparency of the bar. That is, a smaller alpha channel intensity
               results in a more transparent bar and vis-versa.

              Below, a background image is dynamically generated with the desired opacity.
        */
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1),
                                               false,
                                               layer.contentsScale)
        
        let context = UIGraphicsGetCurrentContext()
                        
        context?.setFillColor(color.cgColor)
        
        UIRectFill(CGRect(x: 0, y: 0, width: 1, height: 1))
        
        transparentBackground = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
                
        UIGraphicsEndImageContext()
        
        /*    You should use the appearance proxy to customize the appearance of
              UIKit elements. However changes made to an element's appearance
            proxy do not effect any existing instances of that element currently
               in the view hierarchy. Normally this is not an issue because you
               will likely be performing your appearance customizations in
            -application:didFinishLaunchingWithOptions:. However, this example
             allows you to toggle between appearances at runtime which necessitates
               applying appearance customizations directly to the navigation bar.
        */
        
        return transparentBackground
    }
    
    private func applyTransparentBackgroundToNavigationTitle(color: UIColor = .white,
                                                             titleColor: UIColor,
                                                             alpha: CGFloat) {
        
        tintColor = color
        
        titleTextAttributes = [.foregroundColor: titleColor]
        
    }
    
}
