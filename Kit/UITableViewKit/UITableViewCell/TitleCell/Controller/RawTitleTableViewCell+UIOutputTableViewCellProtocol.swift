//
//  RawTitleTableViewCell+UIOutputTableViewCellProtocol.swift
//  PromoUI
//
//  Created by peter.lavskiy on 05/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import Foundation

extension UIOutputTableViewCellProtocol where Self: RawTitleTableViewCell {
        
    func configure(_ model: Self.Model) {
        titleLabel.text = model.title
    }
    
    func configureUI(_ configuration: Self.UIConfiguration) {
        
        accessoryView = configuration.accessoryView
        
        selectionStyle = configuration.selectionStyle
        
        // Шрифт и цвета нужно объединить в объект стиль UILabelStyle
        // Цвета также будут браться из специального объекта цветов
        titleLabel.font = configuration.titleFont
        titleLabel.textColor = configuration.titleTextColor
        
        if let constant = configuration.titleLabelLeadingConstant {
            titleLabelLeading.constant = constant
        }
        
        if let constant = configuration.titleLabelTrailingConstant {
            titleLabelTrailing.constant = constant
        }
        
        if accessoryView != nil {
            titleLabelTrailing.constant /= 2
        }
    }
}
