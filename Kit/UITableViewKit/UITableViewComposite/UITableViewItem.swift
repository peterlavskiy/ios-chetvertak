//
//  UITableViewItem.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public struct UITableViewItem: UITableViewComponent {
    
    let reuseItem: UIReusableItemProtocol
    
    public var builder: UITableViewCellBuilderProtocol
    
    public var component: UITableViewComponentFactoryProtocol { builder }
    
    public var nib: UINib? { reuseItem.nib }
    
    public var cellClass: AnyClass.Type? { reuseItem.cellClass }
            
    public var command: CommandProtocol? 
            
    public init (builder: UITableViewCellBuilderProtocol,
                 reuseItem: UIReusableItemProtocol,
                 command: CommandProtocol? = nil) {
        self.builder = builder
        self.reuseItem = reuseItem
        self.command = command
    }
        
    public func make(_ view: UITableViewCell?) -> UITableViewCell {
        builder.make(view)
    }
}
