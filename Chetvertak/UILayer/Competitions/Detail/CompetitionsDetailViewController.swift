//
//  CompetitionsDetailViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 08.08.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit
import LinkPresentation
import CoreLocation

struct CompetitionsDetailFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider
    
    func makeViewController() -> UIViewController {
        let viewController = CompetitionsDetailViewController(colorProvider: colorProvider)
        
        return viewController
    }
    
}

class CompetitionsDetailViewController: UIViewController {
        
    let composer = UITableViewComposer()
    
    let service = CompetitionsService()
    
    let coordinateProvider = CoordinateProvider()
    
    let colorProvider: ColorProvider
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .none
        }
    }
    
    init(colorProvider: ColorProvider) {
        self.colorProvider = colorProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = composer
        tableView.delegate = composer
        
        navigationItem.largeTitleDisplayMode = .never
        
        loadData()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        view.backgroundColor = Color().systemBackground
    }
    
    func loadData() {
        
        coordinateProvider.regionAddress = "Брянская область, Брянский район, село Октябрьское, ул Заозерная 1"
        coordinateProvider.annotationImage = UIImage(named: "Pin")
        
        coordinateProvider.getAnnotation { [weak self] _ in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.composer.setContent([self.makeCompetitionSection()])
                self.tableView?.reloadData()
            }
        }

    }
    
    func makeCompetitionSection() -> UITableViewComponent {
        
        typealias View = UITableViewSectionHeader
        
        let model = UITableViewSectionHeader.Model(title: "")
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold)), textColor: Color().label)
        
        
        let configuration = UITableViewSectionHeader.UIConfiguration(title: titleConfiguration)
        
        let factory = UITableViewFactoryComponent<View>(model: model,
                                                        configuration: configuration,
                                                        estimatedHeight: .zero)
        
        let items = [makeTitleCell(), makeMapCell(), makeClubCell(), makeContact(), makeContact(), makeContact(), makeControls(), makeCTA()]
        
        return factory.buildSection(View.self, items: items)
        
    }
    
    func makeTitleCell() -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let title = "Чемпионат России"
        let subtitle = "04 – 06 сентября"
        let value = "СК"
        
        let model = OutputRightTableViewCellModel(title: title,
                                                  subTitle: subtitle,
                                                  value: value)
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold)), textColor: Color().label)
        
        let subtitleConfiguration: UILabelConfiguration = .init(font: .preferredFont(forTextStyle: .subheadline), textColor: Color(alpha: 0.6).secondaryLabel, numberOfLines: 1)
        
        let valueConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .caption1).scaledFont(for: .systemFont(ofSize: 12.0, weight: .semibold)), textColor: Color(alpha: 0.6).secondaryLabel, layoutMargins: .init(top: -24.0, left: 0, bottom: 0, right: 0))
        
        let configuration = OutputRightTableViewCellConfiguration(title: titleConfiguration, subTitle: subtitleConfiguration, value: valueConfiguration, valueBackgroundViewColor: Color().systemGray6, valueBackgroundStrokeColor: Color().systemGray4)
        
        return UITableViewFactoryComponent<View>(model: model, configuration: configuration, estimatedHeight: .zero, height: 78.0).buildCell(View.self)
    }
    
    func makeMapCell() -> UITableViewComponent {
        
        typealias View = MapTableViewCell
        
        let model = MapCellModel(annotaions: [], provider: coordinateProvider)
        
        let configuration = MapCellUIConfiguration()
        
        return UITableViewFactoryComponent<View>(model: model, configuration: configuration, estimatedHeight: configuration.height).buildCell(View.self)
    }
    
    func makeClubCell() -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let title = "СК « Брянск»"
        let subtitle = "📍 Брянская область, Брянский район, село Октябрьское, ул Заозерная 1"
        
        let imageLayout = LeftImageViewLayout(leadingConstant: 16.0,
                                              traillingConstant: 16.0,
                                              topConstant: 16.0,
                                              layout: ImageViewLayout(height: 88.0,
                                                                      width: 88.0,
                                                                      cornerRadii: 44.0))
        let imageProvider = MainImageProvider(imageName: "img_club",
                                              leftImageView: imageLayout)
        
        let model = OutputRightTableViewCellModel(title: title,
                                                  subTitle: subtitle)
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .body).scaledFont(for: .systemFont(ofSize: 17.0, weight: .semibold)), textColor: Color().label)
        
        let subtitleConfiguration: UILabelConfiguration = .makeSubtitle(font: .preferredFont(forTextStyle: .subheadline), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let configuration: OutputRightTableViewCellConfiguration = .init(title: titleConfiguration, subTitle: subtitleConfiguration)
        
        return UITableViewFactoryComponent<View>(model: model,
                                                 configuration: configuration,
                                                 estimatedHeight: 144,
                                                 imageProvider: imageProvider,
                                                 reuseIdentifier: "ClubCell").buildCell(View.self)
    }
    
    func makeContact() -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let title = "Телефон"
        let value = "+7 (4832) 37-12-37"
        
        let model = OutputRightTableViewCellModel(title: title,
                                                  value: value)
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font:.preferredFont(forTextStyle: .subheadline), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let valueConfiguration: UILabelConfiguration = .init(font: .preferredFont(forTextStyle: .subheadline), textColor: .systemBlue, layoutMargins: .init(top: 0, left: 0, bottom: 0, right: 16.0))
        
        let configuration = OutputRightTableViewCellConfiguration(title: titleConfiguration, value: valueConfiguration)
        
        return UITableViewFactoryComponent<View>(model: model, configuration: configuration, estimatedHeight: 44.0).buildCell(View.self)
        
    }
    
    func makeControls() -> UITableViewComponent {
        
        typealias View = ControlTableViewCell
        
        let share = SocialShareCommand(source: self, title: "Чемпионат России 04 – 06 сентября", overview: "Брянская область, Брянский район, село Октябрьское, ул Заозерная 1", image: UIImage(named: "img_club"))
        
        let model = ControlTableViewCellModel(controls: [SecondaryControlFactory(title: "В календарь"), SecondaryControlFactory(title: "Поделиться", command: share)])
        
        let configuration = ControlTableViewCellUIConfiguration(distribution: .fillProportionally, spacing: 16.0)
        
        return UITableViewFactoryComponent<View>(model: model, configuration: configuration, estimatedHeight: 100.0, height: 60.0).buildCell(View.self)
        
    }
    
    func makeCTA() -> UITableViewComponent {
        
        class TakePartCommand: CommandProtocol {
            
            let contact: String = "administrator@scbryansk.ru"
            let base: String = "mailto:"

            func execute() {
                guard let url = URL(string: "\(base)\(contact)") else { return }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        typealias View = CTAContainerTableViewCell
        
        let configuration = CTAContainerCellConfiguration()
        
        let layout = LargeLayout(cornerRadii: 14.0,
                                 margins: .init(top: 14.0, left: 16.0, bottom: 14, right: 16.0),
                                 isEqualScreenWidth: true)
        
        let state = ButtonState(normal: ControlState(backgroundColor: colorProvider.brand, textColor: .white))
        
        let buttonFactory = PrimaryButton(layout: layout,
                                          model: .init(title: "Хочу записаться"),
                                          state: state)
        
        let model = CTAContainerCellModel(colorProvider: colorProvider,
                                          buttonFactory: buttonFactory,
                                          command: TakePartCommand())
        
        return UITableViewFactoryComponent<View>(model: model,
                                                 configuration: configuration,
                                                 estimatedHeight: 72.0).buildCell(View.self)
    }
    
}

class MediaShareSource: NSObject, UIActivityItemSource {
    
    let image: UIImage
    let overview: String
    
    init?(image: UIImage?, overview: String) {
        guard let image = image else { return nil }
        self.image = image
        self.overview = overview
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        UIImage()
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        overview
    }
    
    @available(iOS 13.0, *)
    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        
        let metadata = LPLinkMetadata()
        let imageProvider = NSItemProvider(object: image)
        metadata.imageProvider = imageProvider
        metadata.title = overview
        return metadata

    }
    
}

class SocialShareCommand: CommandProtocol {
    
    let title: String
    
    var overview: String?
    
    var image: UIImage?
    
    weak var source: UIViewController?
    
    weak var presented: UIViewController?
    
    init(source: UIViewController? = nil,
         presented: UIViewController? = nil,
         title: String,
         overview: String? = nil,
         image: UIImage? = nil) {
        
        self.source = source
        self.presented = presented
        self.overview = overview
        self.image = image
        self.title = title
    }
    
    func execute() {
        
        let image = self.image ?? UIImage(named: "AppIcon")
        
        let items: [Any?] = [image, MediaShareSource(image: image, overview: title)]
        
        let activityItems = items.compactMap { $0 }
        
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
        
        activityViewController.popoverPresentationController?.sourceView = source?.view
        
        (source ?? presented?.presentedViewController)?.present(activityViewController, animated: true, completion: nil)
    }
    
}

class SecondaryControlFactory: ControlFactory {
    
    let layout = LargeLayout(cornerRadii: 8.0)
    
    var title: String?
    
    var command: CommandProtocol?
    
    init(title: String? = nil, command: CommandProtocol? = nil) {
        self.title = title
        self.command = command
    }
    
    func make() -> UIControl {
        
        let model = ButtonModel(title: title, tintColor: Color().label, highlighter: nil)
        
        let state = ButtonState(normal: ControlState(backgroundColor: Color(alpha: 0.12).tertiarySystemFill, textColor: .systemBlue))
        
        let view = SecondaryButton(layout: layout,
                                   model: model,
                                   state: state).make()
        
        view.addTarget(self, action: #selector(action), for: .touchUpInside)
        
        return view
    }
    
    @objc func action(control: UIControl) {
        
        command?.execute()
    }
}

import EventKit

class EventService {
        
    let store = EKEventStore()
    
    func make() {
        
        store.requestAccess(to: .reminder) { isGranted, error in
            
        }
        
        let reminder = EKReminder(eventStore: store)
        reminder.title = "Новое соревнование"
        reminder.calendar = store.defaultCalendarForNewReminders()
        
    }
    
}
