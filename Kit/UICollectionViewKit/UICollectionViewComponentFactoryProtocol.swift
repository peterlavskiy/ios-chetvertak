//
//  UICollectionViewComponentFactoryProtocol.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public protocol UICollectionViewComponentFactoryProtocol: UIReusableComponentProtocol {
    
    var command: CommandProtocol? { get }
    
    var insets: UIEdgeInsets { get }
    
    var estimatedSize: CGSize { get }

    var itemSize: CGSize { get }
    
    var isSelected: Bool { get }
    
    var reuseItem: UIReusableItemProtocol { get }
    
    func makeReuseIdentifier() -> String
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView?
        
}
