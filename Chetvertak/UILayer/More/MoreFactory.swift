//
//  MoreFactory.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 27.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

class MoreFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider
    
    init(colorProvider: ColorProvider) {
        self.colorProvider = colorProvider
    }
    
    func makeViewController() -> UIViewController {
                
        let viewController = MoreViewController(colorProvider: colorProvider)
        colorProvider.views.add(viewController)
        viewController.tabBarItem.title = "Ещё"
        viewController.tabBarItem.selectedImage = UIImage(named: "more_active")?.withRenderingMode(.alwaysTemplate)
        viewController.tabBarItem.image = UIImage(named: "more_normal")?.withRenderingMode(.alwaysTemplate)
        return viewController
    }
    
}
