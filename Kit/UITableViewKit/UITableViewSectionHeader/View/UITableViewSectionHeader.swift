//
//  UITableViewSectionHeader.swift
//  PromoUI
//
//  Created by peter.lavskiy on 31/01/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class UITableViewSectionHeader: UITableViewHeaderFooterView, ConfigurableSection {    
    
    typealias Model = UITableViewSectionHeaderModel
    
    typealias UIConfiguration = UITableViewSectionHeaderStyle
    
    static var reuseIdentifier: String { String(describing: UITableViewSectionHeader.self) }
     
    var storedColor: UIColor = UIColor.black
    
    weak var accessoryCommand: CommandProtocol?
    
    lazy var blur: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        return blurEffectView
    }()
        
    lazy var customBackgroundView: UIView = {
        let view = UIView(frame: CGRect(origin: .zero,
                                        size: CGSize(width: UIScreen.main.bounds.width, height: 44.0)))
        view.backgroundColor = .groupTableViewBackground
        return view
    }()
    
    @IBOutlet weak var accessoryButton: UIButton! {
        didSet {
            accessoryButton.isHidden = true
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ model: UITableViewSectionHeaderModel, imageProvider: ImageProviderProtocol?) {
        titleLabel.text = model.title
    }
    
    func configureUI(_ configuration: UIConfiguration) {
        
        titleLabel.configure(configuration.title)
        storedColor = configuration.backgroundColor
        
        accessoryButton.isHidden = configuration.accessoryIsHidden
        accessoryButton.setTitleColor(configuration.accessoryButtonTitleColor, for: .normal)
        accessoryButton.addTarget(self, action: #selector(accessoryAction), for: .touchUpInside)
        accessoryCommand = configuration.accessoryCommand
        
        backgroundView = storedColor == .clear ? blur : customBackgroundView
    }
    
    @objc func accessoryAction() {
        
        accessoryCommand?.execute()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = storedColor
        customBackgroundView.backgroundColor = storedColor
        blur.frame = bounds

    }

}
