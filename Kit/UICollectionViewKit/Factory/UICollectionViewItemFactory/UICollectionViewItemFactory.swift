//
//  UICollectionViewItemFactory.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UICollectionViewItemFactory<View: CollectionViewReusableCell>: UICollectionViewItemFactoryProtocol {
    
    let builder: UICollectionViewComponentFactoryProtocol
    
    var nib: UINib?
    var cellClass: AnyClass.Type?
    
    var isSelected: Bool { builder.isSelected }
    var reuseItem: UIReusableItemProtocol { builder.reuseItem }

    func make(_ view: UICollectionViewCell?) -> UICollectionViewCell {
        builder.makeReusableView(view) as? View ?? View()
    }
    
    func build() -> UICollectionViewComponent {

        UICollectionViewItem(reuseItem: reuseItem, builder: self)
    }
}
