//
//  TitleCellConfigurable.swift
//  PromoUI
//
//  Created by peter.lavskiy on 05/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import UIKit

protocol TitleCellConfigurable {
    
    var accessoryView: TableViewCellAccessoryView? { get }
    
    var selectionStyle: UITableViewCell.SelectionStyle { get }
    
    var titleFont: UIFont { get }
    
    var titleTextColor: UIColor { get }
    
    var titleLabelLeadingConstant: CGFloat? { get }
    
    var titleLabelTrailingConstant: CGFloat? { get }
    
}
