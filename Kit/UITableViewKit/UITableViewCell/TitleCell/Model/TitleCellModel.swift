//
//  TitleCellModel.swift
//  PromoUI
//
//  Created by peter.lavskiy on 05/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import Foundation

struct TitleCellModel: TitleCellModelProtocol {
    
    var title: String = "Это Title ячейка"
}
