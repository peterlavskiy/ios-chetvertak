//
//  UIControlState.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 15.08.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol UIControlState {
    
    var backgroundColor: UIColor { get }
    
    var textColor: UIColor { get }

}
