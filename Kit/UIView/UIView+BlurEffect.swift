//
//  UIView+BlurEffect.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 04.05.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

extension UIView {
    
    func makeBlurEffect(_ blurEffect: UIBlurEffect = UIBlurEffect(style: .light),
                        topConstant: CGFloat = 0.0) {
                
        guard (subviews.contains { $0 is UIVisualEffectView} == false) else { return }
        
        let view = UIVisualEffectView(effect: blurEffect)
        view.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(view, at: 0)
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: view.topAnchor, constant: topConstant),
            leftAnchor.constraint(equalTo: view.leftAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor),
            rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }
    
}

extension UIView {
    
    func makeCircularRoundCorners(cornerRadius: CGFloat) {
        
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        
    }
    
    func makeSmoothRoundCorners(cornerRadius: CGFloat) {
        
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        
        if #available(iOS 13.0, *) {
            layer.cornerCurve = .continuous
        }
        
    }
    
    func makeSmoothRounded() {
        
        layer.cornerRadius = bounds.height / 2
        
        clipsToBounds = true
        
        if #available(iOS 13.0, *) {
            layer.cornerCurve = .circular
        }
    }
}
