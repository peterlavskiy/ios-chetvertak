//
//  UITableViewFactoryComponent.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UITableViewFactoryComponent<View: ReusableView>: UITableViewComponentFactoryProtocol {
    
    let model: View.Model
    let configuration: View.UIConfiguration
    let estimatedHeight: CGFloat
    let height: CGFloat
    let imageProvider: ImageProviderProtocol?
    let command: CommandProtocol?

    var reuseIdentifier: String
    var cellClass: AnyClass.Type?
    var nib: UINib? = UINib(nibName: String(describing: View.self), bundle: nil)
    var reuseItem: UIReusableItemProtocol {
        UIReusableItem(reuseIdentifier: reuseIdentifier,
                       nib: nib,
                       cellClass: cellClass)
    }
    
    init(model: View.Model,
         configuration: View.UIConfiguration,
         estimatedHeight: CGFloat,
         height: CGFloat = UITableView.automaticDimension,
         imageProvider: ImageProviderProtocol? = nil,
         reuseIdentifier: String? = nil,
         command: CommandProtocol? = nil) {
        self.model = model
        self.configuration = configuration
        self.estimatedHeight = estimatedHeight
        self.height = height
        self.imageProvider = imageProvider
        self.reuseIdentifier = reuseIdentifier ?? View.reuseIdentifier
        self.command = command
    }
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView? {
        let view = view as? View
        view?.configure(model, imageProvider: imageProvider)
        view?.configureUI(configuration)
        return view
    }
    
    func makeReuseIdentifier() -> String {
        model.isImageAvaliable ? "\(View.reuseIdentifier)Imageable" : reuseIdentifier
    }
}
extension UITableViewFactoryComponent {
    
    func buildCell<T: ConfigurableCell>(_ view: T.Type) -> UITableViewComponent {
        
        return UITablewViewCellFactory<T>(builder: self,
                                          command: command).build()
    }
    
    func buildSection<T: ConfigurableSection>(_ view: T.Type,
                                              items: [UITableViewComponent]) -> UITableViewComponent {
        
        return UITableViewHeaderFactory<T>(builder: self, items: items).build()
    }
    
}
