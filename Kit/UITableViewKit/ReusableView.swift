//
//  ReusableView.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol ConfigurableImageModel {
    
    var isImageAvaliable: Bool { get }
    
}
extension ConfigurableImageModel {
    
    var isImageAvaliable: Bool { false }
}

protocol ReusableView: UIView {
        
    associatedtype Model: ConfigurableImageModel
    
    associatedtype UIConfiguration
    
    static var reuseIdentifier: String { get }
        
    func configure(_ model: Model)
    
    func configureUI(_ configuration: UIConfiguration)
    
    func configure(_ model: Model, imageProvider: ImageProviderProtocol?)
    
}

extension ReusableView {
    
    func configure(_ model: Model) {
        configure(model, imageProvider: nil)
    }

}
