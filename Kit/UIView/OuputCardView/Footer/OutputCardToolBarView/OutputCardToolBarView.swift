//
//  OutputCardToolBarView.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 16.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class OutputCardToolBarView: UIView, OutputCardFooterViewProtocol {
    
    func configure(_ model: Model) {
        
    }
    
    func configureUI(_ configuration: OutputCardFooterUIProtocol) {
        
    }

    func setAccessoryViewHidden(_ isHidden: Bool) {
        
    }
}


