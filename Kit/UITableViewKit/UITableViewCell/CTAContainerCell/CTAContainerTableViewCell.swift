//
//  CTAContainerTableViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 03.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct CTAContainerCellConfiguration {}

struct CTAContainerCellModel: ConfigurableImageModel {
    
    var colorProvider: ColorProvider
        
    var buttonFactory: ButtonType = PrimaryButton(layout: LargeLayout(cornerRadii: 24.0))
    
    var command: CommandProtocol
    
}

class CTAContainerTableViewCell: UITableViewCell, ConfigurableCell {
    
    typealias Model = CTAContainerCellModel
    
    typealias UIConfiguration = CTAContainerCellConfiguration
    
    static var reuseIdentifier: String { String(describing: CTAContainerTableViewCell.self) }
    
    lazy var button: UIControl = {
        
        let button = buttonFactory?.make() ?? UIControl()
        button.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(button)
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            button.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            button.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            button.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        return button
    }()
    
    var command: CommandProtocol?
    
    var buttonFactory: ButtonType?
    
    weak var colorProvider: ColorProvider?
    
    func configure(_ model: CTAContainerCellModel, imageProvider: ImageProviderProtocol?) {

        colorProvider = model.colorProvider
        command = model.command
        buttonFactory = model.buttonFactory
        button.addTarget(self, action: #selector(commandAction), for: .touchUpInside)
    }
    
    func configureUI(_ configuration: CTAContainerCellConfiguration) {
        
        button.backgroundColor = colorProvider?.brand
        
        selectionStyle = .none
    }
    
    @objc func commandAction() {
        
        command?.execute()
    }
}
