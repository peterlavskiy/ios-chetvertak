//
//  DismissAnimatedTransitioning.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class DismissAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    let colorProvider: ColorProvider
    let shadowFactory: ShadowFactory?
    let timeInterval: TimeInterval
    var retriever: ContextRetriever?
    var dismissCallback: (() -> Void)?
    
    weak var initialView: UIView?
    
    init(timeInterval: TimeInterval, colorProvider: ColorProvider, shadowFactory: ShadowFactory? = nil) {
        self.timeInterval = timeInterval
        self.colorProvider = colorProvider
        self.shadowFactory = shadowFactory
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        timeInterval
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        retriever = ContextRetriever(context: transitionContext)
        
        let toViewController = retriever?.toViewController as? UITabBarController
        var tabBarFrame = toViewController?.tabBar.frame ?? .zero
        
        let fromViewController = retriever?.fromViewController as? CardDetailTransionable
        
        let containerView = retriever?.containerView ?? UIView()
        let fromView = retriever?.fromView ?? UIView()
        let cardView = initialView as? OutputCardCollectionViewCell ?? OutputCardCollectionViewCell()
        
        let center = cardView.center
        let size = cardView.bounds.size
        let cardFrameWithoutTransform = CGRect(x: center.x - size.width / 2,
                                               y: center.y - size.height / 2,
                                               width: size.width,
                                               height: size.height)
        let onScreenFrame = cardView.superview?.convert(cardFrameWithoutTransform, to: nil) ?? .zero
        let toView = UIView(frame: onScreenFrame)

        let tabBar = toViewController?.tabBar ?? UIView()

        let animatedContainerView = UIView()
        animatedContainerView.translatesAutoresizingMaskIntoConstraints = false
        fromView.translatesAutoresizingMaskIntoConstraints = false
                
        containerView.removeConstraints(containerView.constraints)
        
        containerView.addSubview(animatedContainerView)
        animatedContainerView.addSubview(fromView)
        containerView.addSubview(toView)
        
        fromView.edges(to: animatedContainerView)
        
        animatedContainerView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        let animatedContainerTopConstraint = animatedContainerView.topAnchor.constraint(equalTo: containerView.topAnchor)
        let animatedContainerWidthConstraint = animatedContainerView.widthAnchor.constraint(equalToConstant: fromView.frame.width)
        let animatedContainerHeightConstraint = animatedContainerView.heightAnchor.constraint(equalToConstant: fromView.frame.height)
        
        NSLayoutConstraint.activate([animatedContainerTopConstraint, animatedContainerWidthConstraint, animatedContainerHeightConstraint])
                
        let constraints = fromViewController?.stretchToView(toView) ?? []
                
        dismissCallback?()
        fromViewController?.viewWillDissmiss()
        
        containerView.addSubview(tabBar)
        
        func makeShadows() {
            shadowFactory?.make(in: containerView.layer, bounds: onScreenFrame)
        }
                
        func animateCardViewBackToPlace() {
            fromView.removeConstraints(constraints)
            makeShadows()
            fromView.layer.cornerRadius = cardView.layer.cornerRadius
            fromView.clipsToBounds = true
            
            animatedContainerTopConstraint.constant = onScreenFrame.origin.y
            animatedContainerWidthConstraint.constant = onScreenFrame.width
            animatedContainerHeightConstraint.constant = onScreenFrame.height
            containerView.layoutIfNeeded()
        }
        
        func completeEverything() {
            let success = !transitionContext.transitionWasCancelled
            animatedContainerView.removeConstraints(animatedContainerView.constraints)
            animatedContainerView.removeFromSuperview()
            
            if success {
                fromView.removeFromSuperview()
                initialView?.isHidden = false
            } else {
                containerView.removeConstraints(containerView.constraints)

                containerView.addSubview(fromView)
                fromView.edges(to: containerView)
            }
            
            toViewController?.view.addSubview(tabBar)
            transitionContext.completeTransition(success)
        }
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
             animateCardViewBackToPlace()
         }) { (finished) in
             completeEverything()
         }

        tabBarFrame = toViewController?.tabBar.frame.offsetBy(dx: 0, dy: -tabBarFrame.height) ?? .zero
        
         UIView.animate(withDuration: transitionDuration(using: transitionContext) * 0.6) {
            fromViewController?.dismissButton?.alpha = 0
            fromViewController?.scrollView?.contentOffset = .zero
            tabBar.frame = tabBarFrame
            toViewController?.tabBar.frame = tabBarFrame
         }
        
    }
    
}
