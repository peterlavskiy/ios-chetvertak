//
//  UITableViewSectionHeaderStyle.swift
//  PromoUI
//
//  Created by peter.lavskiy on 31/01/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UITableViewSectionHeaderStyle {
    
    var title: UILabelConfiguration = .makeTitle()
    
    var backgroundColor: UIColor = Color().systemBackground
    
    var accessoryIsHidden: Bool = true
    
    var accessoryButtonTitleColor: UIColor = Color().systemRed
    
    var accessoryCommand: CommandProtocol?
    
}
