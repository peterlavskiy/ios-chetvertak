//
//  SuggestCollectionViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 03.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class SuggestState {
    
    var isSelected: Bool = false
    
}

struct SuggestItemConfiguration {
    
    let backgroundColor: UIColor
    
    var leftImageView: LeftImageViewLayout?
}

struct SuggestItemModel: ConfigurableImageModel {
    
    let title: String
    
    let state: SuggestState
}

class SuggestCollectionViewCell: UICollectionViewCell, CollectionViewReusableCell {

    typealias Model = SuggestItemModel
    
    typealias UIConfiguration = SuggestItemConfiguration
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var titleLabelLeading: NSLayoutConstraint!
    
    weak var leftImageView: UIImageView?
        
    static var reuseIdentifier: String { "\(SuggestCollectionViewCell.self)" }
    
    private var currentColor: UIColor {
        isSelected ? activeColor : inactiveColor
    }
    
    private var activeColor: UIColor {
        Color().brandBlue
    }
    
    private var inactiveColor: UIColor {
        Color().systemGray6
    }
    
    override var isSelected: Bool {
        didSet {
            titleLabel.textColor = isSelected ? .white : Color().secondaryLabel
            setNeedsDisplay()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundView = UIView(frame: bounds)
        selectedBackgroundView = UIView(frame: bounds)
        
        setNeedsDisplay()
    }
    
    func configure(_ model: SuggestItemModel, imageProvider: ImageProviderProtocol?) {
        
        titleLabel.text = model.title
        leftImageView?.image = imageProvider?.getAssetImage()
        isSelected = model.state.isSelected
        
    }
    
    func configureUI(_ configuration: SuggestItemConfiguration) {
        
        if leftImageView == nil {
            leftImageView = configuration.leftImageView?.make(in: contentView, centerAnchor: nil, leadingAnchor: titleLabel.leadingAnchor, inactive: [titleLabelLeading])
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 24)
        
        currentColor.setFill()
        
        path.fill()
        
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        
        backgroundView?.layer.mask = shape
        selectedBackgroundView?.layer.mask = shape
        contentView.layer.mask = shape
    }
    
}
