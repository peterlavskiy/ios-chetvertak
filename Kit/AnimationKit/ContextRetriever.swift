//
//  ContextRetriever.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class ContextRetriever {
    
    weak var context: UIViewControllerContextTransitioning?
    
    weak var containerView: UIView?
    weak var fromViewController: UIViewController?
    weak var fromView: UIView?
    
    weak var toViewController: UIViewController?
    weak var toView: UIView?
    
    var toViewStartFrame: CGRect?
    
    var toViewFinalFrame: CGRect?
    
    var fromViewFinalFrame: CGRect?
    
    init(context: UIViewControllerContextTransitioning) {
        
        self.context = context
        self.containerView = context.containerView
        self.fromViewController = context.viewController(forKey: .from)
        self.fromView = context.view(forKey: .from)
        self.toViewController = context.viewController(forKey: .to)
        self.toView = context.view(forKey: .to)
        
        if let toViewController = toViewController {
            self.toViewStartFrame = context.initialFrame(for: toViewController)
            self.toViewFinalFrame = context.finalFrame(for: toViewController)
        }
        
        if let fromViewController = fromViewController {
            self.fromViewFinalFrame = context.finalFrame(for: fromViewController)
        }
    }
    
    func configureToView() {
        
        if let toView = toView {
            containerView?.addSubview(toView)
        }
        
        if let frame = toViewStartFrame {
            toView?.frame = frame
        }
        
    }
}
