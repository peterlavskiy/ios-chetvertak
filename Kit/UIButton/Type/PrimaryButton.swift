//
//  PrimaryButton.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct PrimaryButton: ButtonType {
    
    let layout: ButtonLayout
    var model: ButtonModel?
    var colorProvider: ColorProvider?
    var state = ButtonState()

    var title: UILabelConfiguration {
        UILabelConfiguration(font: layout.titleFont, textColor: state.normal.textColor, numberOfLines: 1)
    }
    
    func make() -> Button {
        let button = Button(layout: layout, type: self, controlState: state)
        if let model = model {
            button.configure(model: model)
        }
        return button
    }
}
