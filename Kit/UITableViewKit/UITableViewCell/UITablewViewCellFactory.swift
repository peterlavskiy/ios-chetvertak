//
//  UITablewViewCellFactory.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UITablewViewCellFactory<View: ConfigurableCell>: UITableViewCellBuilderProtocol {

    let builder: UITableViewComponentFactoryProtocol
    
    var estimatedHeight: CGFloat { builder.estimatedHeight }
    var height: CGFloat { builder.height }
    var reuseIdentifier: String { builder.makeReuseIdentifier() }
    var reuseItem: UIReusableItemProtocol { builder.reuseItem }
    var nib: UINib?
    var cellClass: AnyClass.Type?
    var command: CommandProtocol? 
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView? {
        builder.makeReusableView(view)
    }
    
    func make(_ view: UITableViewCell?) -> UITableViewCell {
        builder.makeReusableView(view) as? View ?? View()
    }
    
    func makeReuseIdentifier() -> String {
        builder.makeReuseIdentifier()
    }
    
    func build() -> UITableViewComponent {

        UITableViewItem(builder: self, reuseItem: reuseItem, command: command)
    }
}
