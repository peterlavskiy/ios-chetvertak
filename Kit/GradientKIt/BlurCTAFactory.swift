//
//  BlurCTAFactory.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 22.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct BlurCTAFactory: GradientFactoryProtocol {
    
    let decoratee: GradientFactoryProtocol
                
    func make(in view: UIView) -> CAGradientLayer {
        
        let bounds = view.bounds
        let layer = decoratee.make(in: view)
        
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        layer.bounds = bounds.insetBy(dx: -0.5*bounds.size.width, dy: -0.5*bounds.size.height)
        layer.position = view.center
        
        return layer
    }
    
    static func make() -> GradientFactoryProtocol {
        
        let transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: -0.5, c: 0.5, d: 0, tx: 0.25, ty: 0.5))
        let colors = [
          UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
          UIColor(red: 1, green: 1, blue: 1, alpha: 0).cgColor
        ]
        
        return BlurCTAFactory(decoratee: GradientFactory(colors: colors, transform: transform))
    }
    
}
