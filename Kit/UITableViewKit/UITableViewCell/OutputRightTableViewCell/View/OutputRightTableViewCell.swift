//
//  OutputRightTableViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 14.01.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class OutputRightTableViewCell: UITableViewCell, ConfigurableCell {
    
    typealias Model = OutputRightTableViewCellModel
    
    typealias UIConfiguration = OutputRightTableViewCellConfiguration
                
    private var customBackgroundColor: UIColor?
    
    static var reuseIdentifier: String {
        String(describing: OutputRightTableViewCell.self)
    }
    
    @IBOutlet weak var stackViewTrailing: NSLayoutConstraint?
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!

    @IBOutlet weak var valueBackground: UIView!
    
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var subValueLabel: UILabel!
    
    @IBOutlet weak var teriaryLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var contentStackView: UIStackView!
    
    @IBOutlet weak var leftImageView: UIImageView!
    
    var contentRectCorners: UIRectCorner?
    
    func configure(_ model: Model, imageProvider: ImageProviderProtocol?) {
        
        titleLabel.text = model.title
        
        subTitleLabel.isHidden = model.subTitle == nil
        subTitleLabel.text = model.subTitle
        
        valueLabel.isHidden = model.value == nil
        valueLabel.text = model.value
        
        subValueLabel.text = model.subValue
        
        teriaryLabel.isHidden = model.teriary == nil
        teriaryLabel.text = model.teriary

        leftImageView.isHidden = imageProvider == nil
        imageProvider?.getImage { [weak self] in
            self?.leftImageView.image = $0
            self?.leftImageView.makeCircularRoundCorners(cornerRadius: imageProvider?.leftImageView?.layout.cornerRadii ?? .zero)
            self?.leftImageView.heightAnchor.constraint(equalToConstant: imageProvider?.leftImageView?.layout.height ?? .zero).isActive = true
            self?.leftImageView.widthAnchor.constraint(equalToConstant: imageProvider?.leftImageView?.layout.width ?? .zero).isActive = true
        }
             
        stackViewTrailing?.isActive = model.value == nil
        
    }
    
    func configureUI(_ configuration: UIConfiguration) {
                    
        configureUILabels(configuration)
        
        if let color = configuration.valueBackgroundViewColor {
            valueBackground.backgroundColor = color
            valueBackground.layer.cornerRadius = configuration.valueBackgroundRadius
            valueBackground.clipsToBounds = true
            valueBackground.isHidden = false
        } else {
            valueBackground.isHidden = true
        }
        
        valueBackground.layer.borderColor = configuration.valueBackgroundStrokeColor?.cgColor
                
        selectionStyle = configuration.selectionStyle
        
        accessoryType = configuration.accessoryType
        
        customBackgroundColor = configuration.backgrounColor
        
        contentRectCorners = configuration.contentRectCorners
        
        stackView.distribution = configuration.distribution
        
        contentStackView.layoutMargins = configuration.margins
    }
    
    private func configureUILabels(_ configuration: UIConfiguration) {
        titleLabel.configure(configuration.title)
        if let value = configuration.value {
            valueLabel.configure(value)
        }
        if let subTitle = configuration.subTitle {
            subTitleLabel.configure(subTitle)
        }
        if let subTitle = configuration.subTitle {
            subValueLabel.configure(subTitle)
        }
        if let teriary = configuration.teriary {
            teriaryLabel.configure(teriary)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if valueBackground.isHidden == false, valueBackground.layer.borderColor != nil {
            valueBackground.layer.borderWidth = 1.0
        }
        
        if #available(iOS 13.0, *) {
            
            contentView.backgroundColor = .secondarySystemGroupedBackground
            backgroundColor = .secondarySystemGroupedBackground
            
            if let color = customBackgroundColor, contentRectCorners == nil {
                contentView.backgroundColor = color
                backgroundColor = color
            }
            
            if let color = customBackgroundColor, contentRectCorners != nil {
                contentView.backgroundColor = color
                backgroundColor = .systemBackground
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if let corners = contentRectCorners {
            
            let rect: CGRect = .init(x: 16, y: 4, width: contentView.bounds.width - 32,
                                     height: contentView.bounds.height - 8)
            
            let path = UIBezierPath(roundedRect: rect,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: 16, height: 16))
            path.close()
            customBackgroundColor?.setFill()
            path.fill()
            
            let shape = CAShapeLayer()
            shape.path = path.cgPath
            
            contentView.layer.mask = shape
        }

    }
}
