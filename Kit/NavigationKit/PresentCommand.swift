//
//  PresentCommand.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class PresentCommand: NavigationCommandProtocol {
    
    weak var source: UIViewController?
    var presented: UIViewController?
    
    var destination: UIViewControllerFactory
    var animated: Bool
    var completion: (() -> Void)?
    
    init(source: UIViewController,
         destination: UIViewControllerFactory,
         animated: Bool = true,
         completion: (() -> Void)? = nil) {
        
        self.destination = destination
        self.source = source
        self.animated = animated
        self.completion = completion
    }
    
    func makePresented() {
        
        guard presented == nil else { return }
        
        presented = destination.make()
        
    }
    
    func execute() {
        
        makePresented()
        
        if let presentedViewController = presented {
            source?.present(presentedViewController,
                            animated: animated,
                            completion: completion)
        } else {
            assert(false, "PresentedViewController couldnt be nil")
        }
        
        presented = nil
    }
    
}
