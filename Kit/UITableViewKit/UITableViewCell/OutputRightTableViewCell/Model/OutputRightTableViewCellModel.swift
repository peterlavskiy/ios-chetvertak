//
//  OutputRightTableViewCellModel.swift
//  PromoUI
//
//  Created by peter.lavskiy on 21/01/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct OutputRightTableViewCellModel: ConfigurableImageModel {
    
    var isImageAvaliable: Bool { image != nil }
        
    var title: String
    
    var subTitle: String?
    
    var value: String?
    
    var subValue: String?
    
    var teriary: String?
    
    var imageURL: URL?
    
    var image: UIImage?
            
}
