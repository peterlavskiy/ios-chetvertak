//
//  OutputCardModel.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct OutputCardCollectionViewItemModel: ConfigurableImageModel {
    
    struct Item {
        let title: String
        var icon: String?
        var action: CommandProtocol?
    }
        
    struct Header {
        
        var title: String?
        var subtitle: String?
        var imageName: String?
    }
    
    struct Footer {
        
        let title: String
        var subtitle: String?
        var subvalue: String?
        var teriary: String?
        var items: [Item]?
    }
    
    var header: Header?

    var footer: Footer?
    
}
