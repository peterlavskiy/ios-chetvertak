//
//  ControlItemCollectionViewCell.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 27.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

struct ControlItemModel: ConfigurableImageModel {
    
    let title: String
        
    var darkModeAction: (() -> Void)?
  
}

protocol ControlItemUIProtocol {
        
    var title: UILabelConfiguration { get }
    
    func makeAccessoryView(in item: UICollectionViewCell, action: Selector?) -> UIView
    
    func configureAccessoryView(_ accessoryView: UIView?)
}

struct ControlItemUIDetailConfiguration: ControlItemUIProtocol {
    
    var title: UILabelConfiguration = .makeTitle()
    
    var detailIndicatorImage = UIImage(named: "forward_arrow")?.withRenderingMode(.alwaysTemplate)

    func makeAccessoryView(in item: UICollectionViewCell, action: Selector?) -> UIView {

        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = detailIndicatorImage
        view.tintColor = Color().systemGray4
        view.contentMode = .scaleAspectFit
        item.contentView.addSubview(view)

        NSLayoutConstraint.activate([
            view.heightAnchor.constraint(equalToConstant: 24),
            view.widthAnchor.constraint(equalToConstant: 10),
            view.centerYAnchor.constraint(equalTo: item.contentView.centerYAnchor),
            view.rightAnchor.constraint(equalTo: item.contentView.rightAnchor, constant: -16.0)
        ])

        return view
    }
    
    func configureAccessoryView(_ accessoryView: UIView?) {}
}

struct ControlItemUISwitchConfiguration: ControlItemUIProtocol {
        
    var title: UILabelConfiguration = .makeTitle()
    
    var onTintColor: UIColor = .systemBlue

    var thumbTintColor: UIColor?
    
    var onImage: UIImage?

    var offImage: UIImage?
    
    var isSwitchEnabled: Bool = true
    
    var isSwitchHidden: Bool = false
    
    var switchOn: Bool = false
    
    weak var colorProvider: ColorProvider?
    
    func makeAccessoryView(in item: UICollectionViewCell, action: Selector?) -> UIView {
        
        let view = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.onTintColor = onTintColor
        view.thumbTintColor = thumbTintColor
        view.onImage = onImage
        view.offImage = offImage
        view.isOn = switchOn
        view.isEnabled = isSwitchEnabled
        view.isHidden = isSwitchHidden
        
        if let action = action {
            view.addTarget(item, action: action, for: .valueChanged)
        }
                
        item.contentView.addSubview(view)
        
        NSLayoutConstraint.activate([
            view.centerYAnchor.constraint(equalTo: item.contentView.centerYAnchor),
            view.rightAnchor.constraint(equalTo: item.contentView.rightAnchor, constant: -16.0)
        ])
        
        return view
    }
    
    func configureAccessoryView(_ accessoryView: UIView?) {
        (accessoryView as? UISwitch)?.isOn = switchOn
    }
    
}

class ControlItemCollectionViewCell: UICollectionViewCell, CollectionViewReusableCell {
    
    typealias Model = ControlItemModel
    
    typealias UIConfiguration = ControlItemUIProtocol
    
    static var reuseIdentifier: String { String(describing: ControlItemCollectionViewCell.self) }
    
    @IBOutlet weak var titleLabel: UILabel!
        
    var accessoryView: UIView?
    
    var switchCallback: (() -> Void)?
        
    func configure(_ model: ControlItemModel,
                   imageProvider: ImageProviderProtocol?) {
        
        titleLabel.text = model.title
        
        switchCallback = model.darkModeAction
        
    }
    
    func configureUI(_ configuration: UIConfiguration) {
        
        titleLabel.configure(configuration.title)
        
        if accessoryView == nil {
            let uiSwitch = configuration.makeAccessoryView(in: self, action: #selector(switchAction))
            accessoryView = uiSwitch
        }
        
        configuration.configureAccessoryView(accessoryView)

    }
    
    @objc func switchAction() {
        switchCallback?()
    }

}
