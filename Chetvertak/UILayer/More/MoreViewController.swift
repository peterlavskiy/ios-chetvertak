//
//  MoreViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 27.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController, UIStatusBarConfigurable {

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delaysContentTouches = false
            collectionView.dataSource = composer
            collectionView.delegate = composer
            collectionView.scrollsToTop = false
            collectionView.alwaysBounceVertical = true
            collectionView.showsVerticalScrollIndicator = false
            let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
            layout?.sectionInset = .init(top: 12, left: 0, bottom: 12, right: 0)
            layout?.minimumLineSpacing = 24.0
        }
    }
    
    let composer = UICollectionViewComposer()
    
    let colorProvider: ColorProvider
    
    let service = MainService()
    
    var content: [UICollectionViewComponent] = []
    
    var isStatusBarHidden: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.4) {
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    init(colorProvider: ColorProvider) {
        self.colorProvider = colorProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationItem.title = "Ещё"
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        let fm = FileManager.default
        
        fm.urls(for: .cachesDirectory, in: .userDomainMask).last
        
        reloadContent()
    }
    
    func reloadContent() {
        loadControls()
        loadData()
        collectionView.reloadData()
    }
    
    func loadControls() {
        content = [makeDarkThemeControl(), makeBookmark()]
    }
    
    func loadData() {
        
        service.getBookmarks { [weak self] in
            switch $0 {
            case .success(let entity):
                self?.content.append(contentsOf: entity.result.map(makeBookmarks))
                self?.content.append(makeAgreement())
                self?.composer.setContent(content)
            case .failure(let error):
                print(error)
            }
            
        }
    }
    
    func makeDarkThemeSystemControl() -> UICollectionViewComponent {
        
        typealias View = ControlItemCollectionViewCell
        
        let model = ControlItemModel(title: "Тёмная тема - автоматически") { [weak self] in
            
            self?.switchDarkModeAuto()
        }
        
        let titleFont: UIFont = UIFontMetrics(forTextStyle: .body).scaledFont(for: .systemFont(ofSize: 17.0, weight: .regular))
        let titleColor = Color().label
        
        let configuration = ControlItemUISwitchConfiguration(title: .makeTitle(font: titleFont, textColor: titleColor))
        
        let size: CGSize = .init(width: UIScreen.main.bounds.width, height: 52.0)
        
        return UICollectionViewComponentFactory<View>(model: model, configuration: configuration, estimatedSize: .zero, itemSize: size, reuseIdentifier: "System").buildItem(View.self)
        
    }
    
    func makeDarkThemeControl() -> UICollectionViewComponent {
        
        typealias View = ControlItemCollectionViewCell
        
        let model = ControlItemModel(title: "Тёмная тема") { [weak self] in
            
            self?.switchDarkLightMode()
        }
        
        let titleFont: UIFont = UIFontMetrics(forTextStyle: .body).scaledFont(for: .systemFont(ofSize: 17.0, weight: .regular))
        let titleColor = Color().label

        let configuration = ControlItemUISwitchConfiguration(title: .makeTitle(font: titleFont, textColor: titleColor), isSwitchEnabled: !colorProvider.isDarkAuto, switchOn: colorProvider.isDark, colorProvider: colorProvider)
        
        let size: CGSize = .init(width: UIScreen.main.bounds.width, height: 52.0)
        
        return UICollectionViewComponentFactory<View>(model: model, configuration: configuration, estimatedSize: .zero, itemSize: size, reuseIdentifier: "DarkTheme").buildItem(View.self)
        
    }
    
    func makeBookmark() -> UICollectionViewComponent {
        
        typealias View = ControlItemCollectionViewCell
        
        let model = ControlItemModel(title: "Мои закладки")
        
        let titleFont: UIFont = UIFontMetrics(forTextStyle: .title3).scaledFont(for: .systemFont(ofSize: 20.0, weight: .semibold))
        let titleColor = Color().label
        
        let configuration = ControlItemUISwitchConfiguration(title: .makeTitle(font: titleFont, textColor: titleColor), isSwitchHidden: true)
        
        let size: CGSize = .init(width: UIScreen.main.bounds.width, height: 52.0)
        
        return UICollectionViewComponentFactory<View>(model: model, configuration: configuration, estimatedSize: .zero, itemSize: size, reuseIdentifier: "Bookmark").buildItem(View.self)
    }
    
    func makeBookmarks(_ content: MainContent) -> UICollectionViewComponent {
        
        typealias View = OutputCardCollectionViewCell
        
        let size: CGSize = .init(width: UIScreen.main.bounds.width - 30, height: 313)
        
        let imageProvider = MainImageProvider(imageName: content.image)
        
        let model = OutputCardCollectionViewItemModel(footer: .init(title: content.title, subtitle: content.subtitle, teriary: content.date))
                
        let titleFont: UIFont = UIFontMetrics(forTextStyle: .footnote).scaledFont(for: .systemFont(ofSize: 13.0, weight: .semibold))
        let titleColor = Color(alpha: 0.6).secondaryLabel

        let subtitleFont: UIFont = UIFontMetrics(forTextStyle: .headline).scaledFont(for: .systemFont(ofSize: 18.0, weight: .semibold))
        let subtitleColor = Color().label
        
        let teriaryMargins = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
                
        let footerConfiguration = OutputCardFooterTeriaryAccessory(title: .makeTitle(font: titleFont, textColor: titleColor), subtitle: .makeSubtitle(font: subtitleFont, textColor: subtitleColor), teriaryMargins: teriaryMargins, backgroundColor: Color().footerColor, controls: [ViewsControlFactory(), BookmarkControlFactory(isSelected: true), ShareControlFactory()])
        
        let configuration = OutputCardUIConfiguration(footer: footerConfiguration, shadowFactory: ShadowFactory(colorProvider: colorProvider), highlighter: Highlighter())
                         
        let builder = UICollectionViewComponentFactory<View>(model: model,
                                                    configuration: configuration,
                                                    estimatedSize: .zero,
                                                    itemSize: size,
                                                    imageProvider: imageProvider)
        
        let footer = OutputCardFooterBuilder<OutputCardFooterView>(model: model,
                                             configuration: configuration)
        
        let cardBuilder = UICollectionViewCardFactory<View>(builder: builder, command: makeCommand(content, footer: footer), footer: footer)
        
        return cardBuilder.build()
    }
    
    func makeCommand(_ content: MainContent, footer: CardFooterBuilderProtocol) -> CommandProtocol {
        let presentAnimator = PresentAnimatedTransitioning(timeInterval: 1.0)
        
        let dismissAnimator = DismissAnimatedTransitioning(timeInterval: 0.6, colorProvider: colorProvider, shadowFactory: ShadowFactory(colorProvider: colorProvider))
        
        dismissAnimator.dismissCallback = { [weak self] in
            self?.isStatusBarHidden = false
        }
        
        let destination = DetailFeedFactory(colorProvider: colorProvider,
                                             footer: footer,
                                             image: UIImage(named: content.image))
        
        let presentCommad = PresentCommand(source: self,
                                           destination: destination)
        
        return CardPresentCommand(decoratee: presentCommad, presentAnimator: presentAnimator, dismissAnimator: dismissAnimator)
    }
    
    func makeAgreement() -> UICollectionViewComponent {
        
        typealias View = ControlItemCollectionViewCell
        
        let command = PushCommand(source: self, destination: PrivacyFactory())
        
        let model = ControlItemModel(title: "Пользовательское соглашение")
        
        let titleFont: UIFont = UIFontMetrics(forTextStyle: .body).scaledFont(for: .systemFont(ofSize: 17.0, weight: .regular))
        let titleColor = Color().label
        
        let configuration = ControlItemUIDetailConfiguration(title: .makeTitle(font: titleFont, textColor: titleColor))
        
        let size: CGSize = .init(width: UIScreen.main.bounds.width, height: 52.0)
        
        return UICollectionViewComponentFactory<View>(model: model, configuration: configuration, estimatedSize: .zero, itemSize: size, reuseIdentifier: "Agreement", command: command).buildItem(View.self)
    }
    
    func switchDarkModeAuto() {
        
        colorProvider.isDarkAuto = colorProvider.isDarkAuto ? false : true
        
        loadControls()
        loadData()
        
        collectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
    }
    
    func switchDarkLightMode() {
        UIView.transition(with: self.view,
                          duration: 0.5,
                          options:[.beginFromCurrentState, .transitionCrossDissolve],
                          animations: { [weak self] in
            self?.colorProvider.isDark == true ? self?.colorProvider.disableDarkMode() : self?.colorProvider.enableDarkMode()
        }, completion: nil)

        UIImpactFeedbackGenerator().impactOccurred()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        reloadContent()
    }
}

fileprivate extension Color {
    
    var footerColor: UIColor {
        
        let anyLight = Color().systemBackground
        let darkColor = Color().secondarySystemBackground
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return darkColor
                } else {
                    return anyLight
                }
            }
        } else {
            return anyLight
        }
    }
    
}
