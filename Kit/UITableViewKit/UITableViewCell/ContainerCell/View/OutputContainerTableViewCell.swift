//
//  OutputContainerTableViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 17.05.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct OutputContainerCellModel: ConfigurableImageModel {
    
    let content: [UICollectionViewComponent]
    
}

struct OutputContainerCellUIConfiguration {
    
    let layout: UICollectionViewLayout
    
    let showsHorizontalScrollIndicator = false
    
    var layoutMargins: UIEdgeInsets = .zero
    
}

class OutputContainerTableViewCell: UITableViewCell, ConfigurableCell {
    
    typealias Model = OutputContainerCellModel
    
    typealias UIConfiguration = OutputContainerCellUIConfiguration
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.showsVerticalScrollIndicator = false
        }
    }
    
    static var reuseIdentifier: String {
        String(describing: OutputContainerTableViewCell.self)
    }
    
    var content = [UICollectionViewComponent]()
    
    var layout: UICollectionViewFlowLayout? {
        collectionView.collectionViewLayout as? UICollectionViewFlowLayout
    }
    
    func configure(_ model: Model, imageProvider: ImageProviderProtocol?) {
        content = model.content
        collectionView.reloadData()
    }
    
    func configureUI(_ configuration: UIConfiguration) {
        collectionView.isScrollEnabled = false
        collectionView.showsHorizontalScrollIndicator = configuration.showsHorizontalScrollIndicator
        collectionView.setCollectionViewLayout(configuration.layout, animated: false)
        collectionView.backgroundView?.backgroundColor = Color().systemBackground
        collectionView.backgroundColor = Color().systemBackground
        selectionStyle = .none
    }

}
extension OutputContainerTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        content.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = content[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier,
                                                      for: indexPath)
        if item.isSelected {
            collectionView.selectItem(at: indexPath,
                                      animated: false,
                                      scrollPosition: .centeredHorizontally)
        }
        
        return item.make(cell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        content[indexPath.row].command?.execute()
        collectionView.cellForItem(at: indexPath)?.layoutSubviews()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        content[indexPath.row].itemSize
    }
 
}
