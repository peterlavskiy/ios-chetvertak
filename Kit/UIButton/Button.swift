//
//  Button.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 18.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol ButtonModelProtocol {
    
    var title: String? { get }
        
    var leftIcon: IconProtocol? { get }
    
    var rightIcon: IconProtocol? { get }
        
    var tintColor: UIColor { get }
    
    var highlighter: Highlighter? { get }
    
    var command: CommandProtocol? { get }
}

class Button: UIControl {
        
    var higlighter: Highlighter?
        
    override var isSelected: Bool {
        didSet {
            
            guard oldValue != isSelected else { return }

            switchSelectedIcon()
            
            let state = controlState.switchSelected(isSelected)
            backgroundColor = state.backgroundColor
            titleLabel.textColor = state.textColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            
            guard oldValue != isHighlighted else { return }
            
            higlighter?.make(in: self, isHighlighted: isHighlighted)
            
            let state = controlState.switchHighlighted(isHighlighted, selected: isSelected)
            backgroundColor = state.backgroundColor
            titleLabel.textColor = state.textColor
        }
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: centerYAnchor),
            label.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        return label
    }()
    
    private weak var leftIcon: UIImageView?
    
    private weak var rightIcon: UIImageView?
    
    private var rightIconModel: IconProtocol?
    
    private var leftIconModel: IconProtocol?
        
    let layout: ButtonLayout
    
    let type: ButtonType
    
    let controlState: UIControlStateSwitchable
    
    weak var command: CommandProtocol?
    
    override var intrinsicContentSize: CGSize {
        
        var mainSize = CGSize(width: 22.0, height: 22.0)
        
        if titleLabel.intrinsicContentSize != .zero {
            mainSize = titleLabel.intrinsicContentSize
        }
        
        var width = mainSize.width + layoutMargins.left + layoutMargins.right
        
        if layout.isEqualScreenWidth {
            width = UIScreen.main.bounds.width - layoutMargins.left - layoutMargins.right
        }
        
        let height = mainSize.height + layoutMargins.top + layoutMargins.bottom
        
        return .init(width: width, height: height)
    }
    
    init(layout: ButtonLayout,
         type: ButtonType,
         controlState: UIControlStateSwitchable) {
        
        self.layout = layout
        self.type = type
        self.controlState = controlState
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        layer.mask = layout.makeRounded(for: rect,
                                        strokeColor: type.strokeColor)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        leftIcon?.makeSmoothRounded()
        rightIcon?.makeSmoothRounded()
    }
    
    override func sendAction(_ action: Selector, to target: Any?, for event: UIEvent?) {
        super.sendAction(action, to: target, for: event)
        
        command?.execute()
    }
    
    func configure(model: ButtonModelProtocol) {
        
        command = model.command
        
        tintColor = model.tintColor
        
        backgroundColor = controlState.normal.backgroundColor
        
        layoutMargins = layout.margins
        
        higlighter = model.highlighter
        
        titleLabel.text = model.title
        
        titleLabel.configure(type.title)
                        
        makeLeftIcon(model.leftIcon, isCentered: model.title == nil)
        
        makeRightIcon(model.rightIcon, isCentered: model.title == nil)
        
    }
    
    private func switchSelectedIcon() {
        if let icon = rightIcon, rightIconModel?.selectedImage != nil {
            icon.image = !isSelected ? rightIconModel?.image : rightIconModel?.selectedImage
        }
        
        if let icon = leftIcon, leftIconModel?.selectedImage != nil {
             icon.image = !isSelected ? leftIconModel?.image : leftIconModel?.selectedImage
         }
    }

    private func makeLeftIcon(_ icon: IconProtocol?, isCentered: Bool = false) {
                
        if icon?.image != nil, leftIcon == nil {
            
            let imageView = makeIconWithSize(icon?.size ?? .zero)

            let constraint = isCentered ? imageView.centerXAnchor.constraint(equalTo: centerXAnchor) : imageView.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor, constant: -16.0)
                        
            NSLayoutConstraint.activate([
                constraint
            ])

            imageView.tintColor = tintColor
            
            leftIcon = imageView
            
        }
        
        leftIcon?.image = icon?.image
        
        leftIconModel = icon
    }
    
    private func makeRightIcon(_ icon: IconProtocol?, isCentered: Bool = false) {
        
        if icon != nil, rightIcon == nil {
            
            let imageView = makeIconWithSize(icon?.size ?? .zero)
            
            let constraint = isCentered ? imageView.centerXAnchor.constraint(equalTo: centerXAnchor) : imageView.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 16.0)
            
            NSLayoutConstraint.activate([
                constraint
            ])
            
            imageView.tintColor = tintColor

            rightIcon = imageView
        }
        
        rightIcon?.image = icon?.image
        
        rightIconModel = icon
    }
    
    private func makeIconWithSize(_ size: CGSize) -> UIImageView {
        
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            imageView.widthAnchor.constraint(equalToConstant: size.width),
            imageView.heightAnchor.constraint(equalToConstant: size.height)
        ])
        
        return imageView
    }
}

struct RoundView {
    
    let corners: UIRectCorner
    var cornerRadii: CGFloat = 23.0
    
    func make(for rect: CGRect) -> CAShapeLayer {
        
        let path = UIBezierPath(roundedRect: rect,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: cornerRadii,
                                                    height: cornerRadii))
        path.close()
        
        let shape = CAShapeLayer()
        
        shape.path = path.cgPath
        
        return shape
        
    }
    
}
