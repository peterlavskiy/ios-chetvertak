//
//  OutputCardFooterView.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 16.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol OutputCardFooterViewProtocol: UIView {
    
    typealias Model = OutputCardCollectionViewCell.Model.Footer
        
    func configure(_ model: Model)
    
    func configureUI(_ configuration: OutputCardFooterUIProtocol)
    
    func setAccessoryViewHidden(_ isHidden: Bool)
    
}

class OutputCardFooterView: UIView, OutputCardFooterViewProtocol {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subvalueLabel: UILabel!
    @IBOutlet weak var teriaryLabel: UILabel!
    
    weak var teriaryAccessoryView: UIView?
    
    @IBOutlet weak var titleVerticalSpacing: NSLayoutConstraint?
        
    func configure(_ model: Model) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        subvalueLabel.text = model.subvalue
        teriaryLabel.text = model.teriary
        
        titleVerticalSpacing?.isActive = model.subtitle != nil        
    }
    
    func configureUI(_ configuration: OutputCardFooterUIProtocol) {
        titleLabel.configure(configuration.title)
        subtitleLabel.configure(configuration.subtitle)
        subvalueLabel.configure(configuration.subvalue)
        teriaryLabel.configure(configuration.teriary)
        
        titleLabel.layoutMargins = configuration.titleMargins
        subtitleLabel.layoutMargins = configuration.subtitleMargins
        teriaryLabel.layoutMargins = configuration.teriaryMargins
        
        backgroundColor = configuration.backgroundColor
        
        if teriaryAccessoryView == nil {
            teriaryAccessoryView = configuration.makeAccessoryView(in: self)
        }
    }
    
    func setAccessoryViewHidden(_ isHidden: Bool) {
        teriaryAccessoryView?.alpha = isHidden ? 0.0 : 1.0
    }

}
extension OutputCardFooterViewProtocol {
    
    static func loadView() -> OutputCardFooterViewProtocol? {
        let nib = UINib(nibName: String(describing: self), bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? OutputCardFooterViewProtocol
    }
    
}
