//
//  MainViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 22.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UIStatusBarConfigurable {
    
    let searchController = UISearchController(searchResultsController: nil)
    
    let service = MainService()
    
    let colorProvider: ColorProvider
    
    let composer = UICollectionViewComposer()
        
    var isStatusBarHidden: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.4) {
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        isStatusBarHidden
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        .slide
    }
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delaysContentTouches = false
            collectionView.dataSource = composer
            collectionView.delegate = composer
            composer.delegate = self
            collectionView.scrollsToTop = false
            collectionView.showsVerticalScrollIndicator = false
            let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
            layout?.sectionInset = .init(top: 12, left: 0, bottom: 12, right: 0)
            layout?.minimumLineSpacing = 24.0
        }
    }
    
    init(colorProvider: ColorProvider) {
        self.colorProvider = colorProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if #available(iOS 13.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationItem.title = "Chetvertak"
        }
                
        makeLogo()
        makeSearch()
        
        loadData()
    }
    
    func loadData() {
        
        service.getFeed { [weak self] in
            switch $0 {
            case .success(let entity):
                self?.composer.setContent(entity.result.map(makeItems))
                self?.collectionView.reloadData()
            case .failure(let error):
                print(error)
            }
            
        }
    }
    
    func makeLogo() {
        
        let logoView = UIView(frame: .init(x: 0, y: 0, width: 32, height: 40))
        let imageView = UIImageView(frame: .init(x: 0, y: 0, width: 32, height: 32))
        imageView.backgroundColor = .clear
        imageView.layer.cornerRadius = 16.0
        imageView.clipsToBounds = true
        imageView.image = UIImage(named: "4tak_logo")
        
        logoView.addSubview(imageView)
        navigationItem.titleView = logoView
    }
    
    func makeSearch() {
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true

    }
    
    func makeItems(_ content: MainContent) -> UICollectionViewComponent {
        
        typealias View = OutputCardCollectionViewCell
        
        let size: CGSize = .init(width: UIScreen.main.bounds.width - 30, height: 313)
        
        let imageProvider = MainImageProvider(imageName: content.image)
        
        let model = OutputCardCollectionViewItemModel(footer: .init(title: content.title, subtitle: content.subtitle, teriary: content.date))
                
        let titleFont: UIFont = UIFontMetrics(forTextStyle: .footnote).scaledFont(for: .systemFont(ofSize: 13.0, weight: .semibold))
        let titleColor = Color(alpha: 0.6).secondaryLabel

        let subtitleFont: UIFont = UIFontMetrics(forTextStyle: .headline).scaledFont(for: .systemFont(ofSize: 18.0, weight: .semibold))
        let subtitleColor = Color().label
        
        let teriaryMargins = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
                
        let share = SocialShareCommand(presented: self, title: content.subtitle ?? "", image: UIImage(named: content.image))
        
        let footerConfiguration = OutputCardFooterTeriaryAccessory(title: .makeTitle(font: titleFont, textColor: titleColor), subtitle: .makeSubtitle(font: subtitleFont, textColor: subtitleColor), teriaryMargins: teriaryMargins, backgroundColor: Color().footerColor, controls: [ViewsControlFactory(), BookmarkControlFactory(isSelected: false), ShareControlFactory(command: share)])
        
        let configuration = OutputCardUIConfiguration(footer: footerConfiguration, shadowFactory: ShadowFactory(colorProvider: colorProvider), highlighter: Highlighter())
                         
        let builder = UICollectionViewComponentFactory<View>(model: model,
                                                    configuration: configuration,
                                                    estimatedSize: .zero,
                                                    itemSize: size,
                                                    imageProvider: imageProvider)
        
        let footer = OutputCardFooterBuilder<OutputCardFooterView>(model: model,
                                             configuration: configuration)
        
        let cardBuilder = UICollectionViewCardFactory<View>(builder: builder, command: makeCommand(content, footer: footer), footer: footer)
        
        return cardBuilder.build()
    }
    
    func makeCommand(_ content: MainContent, footer: CardFooterBuilderProtocol) -> CommandProtocol {
        let presentAnimator = PresentAnimatedTransitioning(timeInterval: 1.0)
        
        let dismissAnimator = DismissAnimatedTransitioning(timeInterval: 0.6, colorProvider: colorProvider, shadowFactory: ShadowFactory(colorProvider: colorProvider))
        
        dismissAnimator.dismissCallback = { [weak self] in
            self?.isStatusBarHidden = false
        }
        
        let destination = DetailFeedFactory(colorProvider: colorProvider,
                                             footer: footer,
                                             image: UIImage(named: content.image))
        
        let presentCommad = PresentCommand(source: self,
                                           destination: destination)
        
        return CardPresentCommand(decoratee: presentCommad, presentAnimator: presentAnimator, dismissAnimator: dismissAnimator)
    }


}

fileprivate extension Color {
    
    var footerColor: UIColor {
        
        let anyLight = Color().systemBackground
        let darkColor = Color().secondarySystemBackground
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return darkColor
                } else {
                    return anyLight
                }
            }
        } else {
            return anyLight
        }
    }
    
}

extension MainViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

struct ViewsControlFactory: ControlFactory {
    let layout = SmallLayout(cornerRadii: 0)
    let size: CGSize = .init(width: 24.0, height: 24.0)
    func make() -> UIControl {
        
        let icon = IconModel(image: UIImage(named: "viewed")?.withRenderingMode(.alwaysTemplate), size: size)
        
        let model = ButtonModel(title: "194",
                                rightIcon: icon,
                                tintColor: Color(alpha: 0.3).tertiaryLabel,
                                highlighter: nil)
        
        let state = ButtonState(normal: ControlState(backgroundColor: Color().systemBackground, textColor: Color(alpha: 0.3).tertiaryLabel))
  
        let view = TextButton(layout: layout,
                              model: model,
                              textColor: Color(alpha: 0.3).tertiaryLabel,
                              state: state).make()
        
        view.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        return view
    }
}

class BookmarkControlFactory: ControlFactory {
    
    let layout = SmallLayout(cornerRadii: 0)
    let size: CGSize = .init(width: 24.0, height: 24.0)
    let isSelected: Bool
    
    init(isSelected: Bool) {
        self.isSelected = isSelected
    }

    func make() -> UIControl {
        
        let icon = IconModel(image: UIImage(named: "bookmark_normal")?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage(named: "bookmark_active")?.withRenderingMode(.alwaysOriginal), size: size)
        
        let button = TextButton(layout: layout, model: ButtonModel(leftIcon: icon, tintColor: Color().label, highlighter: nil)).make()
        button.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
        
        button.addTarget(self, action: #selector(switchState), for: .touchUpInside)
        
        return button
    }
    
    @objc func switchState(control: UIControl) {
        control.isSelected = control.isSelected ? false : true
    }
}

class ShareControlFactory: ControlFactory {
    
    var command: CommandProtocol?
    
    let layout = SmallLayout(cornerRadii: 0)
    
    let size: CGSize = .init(width: 24.0, height: 24.0)
    
    init(command: CommandProtocol? = nil) {
        self.command = command
    }
    
    func make() -> UIControl {
        let icon = IconModel(image: UIImage(named: "share")?.withRenderingMode(.alwaysTemplate), size: size)
        let view = TextButton(layout: layout, model: ButtonModel(leftIcon: icon, tintColor: Color().label, highlighter: nil)).make()
        
        view.widthAnchor.constraint(equalToConstant: 24).isActive = true

        view.addTarget(self, action: #selector(action), for: .touchUpInside)

        return view
    }
    
    @objc func action(control: UIControl) {
         
         command?.execute()
     }
}

protocol IconProtocol {
    
    var image: UIImage? { get }
    
    var selectedImage: UIImage? { get }
    
    var size: CGSize { get }
    
}

struct IconModel: IconProtocol {
    
    let image: UIImage?
    var selectedImage: UIImage?
    var size: CGSize = .init(width: 24.0, height: 24.0)

}
