//
//  UITableViewComponentFactoryProtocol.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public protocol UITableViewComponentFactoryProtocol: UIReusableComponentProtocol {
    
    var estimatedHeight: CGFloat { get }

    var height: CGFloat { get }
    
    var reuseItem: UIReusableItemProtocol { get }
    
    func makeReuseIdentifier() -> String
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView?
        
}
