//
//  CardAppStoreLikeTransition.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class CardAppStoreLikeTransition: NSObject, UIViewControllerTransitioningDelegate {
    
    let presentedAnimator: PresentAnimatedTransitioning
    let dismissAnimator: DismissAnimatedTransitioning
    
    init(presentedAnimator: PresentAnimatedTransitioning,
         dismissAnimator: DismissAnimatedTransitioning) {
        self.presentedAnimator = presentedAnimator
        self.dismissAnimator = dismissAnimator
        super.init()
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        presentedAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        dismissAnimator
    }
    
}
