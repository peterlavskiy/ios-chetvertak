//
//  CTAViewContainer.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 21.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class CTAViewContainer: UIView {
    
    weak var button: Button?
    weak var gradient: CALayer? {
        didSet {
            if let oldLayer = oldValue {
                layer.replaceSublayer(oldLayer, with: gradient!)
            } else {
                layer.addSublayer(gradient!)
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        let corners: UIRectCorner = [.topLeft, .topRight, .bottomRight]
        
        
        let path = UIBezierPath(roundedRect: rect,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: 25, height: 25))
        path.close()
        
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        
        layer.mask = shape
        
    }
    
}

struct CTAFactory {
    
    let button: Button
    let superview: UIView
    let colorProvider: ColorProvider
    
    var height: CGFloat = 108.0
    var container: CTAViewContainer?
    
    func make() {
        
    }
    
    func makeAtBottom() -> NSLayoutConstraint? {
                
        if let container = container {
            
            button.translatesAutoresizingMaskIntoConstraints = false
            container.translatesAutoresizingMaskIntoConstraints = false
                        
            container.addSubview(button)
            
            NSLayoutConstraint.activate([
                button.topAnchor.constraint(equalTo: container.topAnchor, constant: 16.0),
                button.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -16.0),
                button.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 16.0),
                button.heightAnchor.constraint(equalToConstant: 50)
            ])
            
            superview.addSubview(container)
            
            let bottom = container.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: height)
            
            NSLayoutConstraint.activate([
                container.trailingAnchor.constraint(equalTo: superview.trailingAnchor),
                container.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
                bottom,
                container.heightAnchor.constraint(equalToConstant: height)
            ])
            
            let blurEffect = colorProvider.isDark ? UIBlurEffect(style: .dark) : UIBlurEffect(style: .light)
            container.makeBlurEffect(blurEffect)
            
            return bottom
        }
        
        return nil
    }
}
