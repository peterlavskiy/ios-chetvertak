//
//  MapTableViewCell.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 09.08.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit
import MapKit

class CoordinateProvider: NSObject, MKMapViewDelegate {
    
    let span = MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1)
    
    var regionAddress: String?

    var annotationImage: UIImage?
    
    var annotations = [MKAnnotation]()
    
    init(regionAddress: String? = nil, annotationImage: UIImage? = nil) {
        self.regionAddress = regionAddress
        self.annotationImage = annotationImage
    }
    
    func getRegion(completion: @escaping (MKCoordinateRegion) -> Void) {
                
        completion(MKCoordinateRegion(center: .init(latitude: 53.23267, longitude: 34.202321),
                                       span: span))
    }
    
    func getAnnotation(completion: @escaping (MKAnnotation) -> Void) {
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = .init(latitude: 53.23267,
                                      longitude: 34.202321)
        annotations.append(annotation)
        
        completion(annotation)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "\(MKAnnotationView.self)")
        annotationView.image = annotationImage
        return annotationView
    }
    
}

struct MapCellModel: ConfigurableImageModel {
    
    let annotaions: [MKPointAnnotation]
    
    let provider: CoordinateProvider
}

struct MapCellUIConfiguration {
    
    let height: CGFloat = 188.0
}

class MapTableViewCell: UITableViewCell, ConfigurableCell {

    typealias Model = MapCellModel
    
    typealias UIConfiguration = MapCellUIConfiguration
    
    static var reuseIdentifier: String {
        String(describing: MapTableViewCell.self)
    }
    
    @IBOutlet weak var mapHeight: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    
    func configure(_ model: Model, imageProvider: ImageProviderProtocol?) {
        
        mapView.delegate = model.provider
        model.provider.getRegion { [weak self] in
            self?.mapView.setRegion($0, animated: false)
        }
        mapView.addAnnotations(model.provider.annotations)
    }
    
    func configureUI(_ configuration: UIConfiguration) {

        mapHeight.constant = configuration.height
        
        selectionStyle = .none
    }
}
