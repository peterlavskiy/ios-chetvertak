//
//  UISegmentView.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 15.08.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UISegment {
    
    let title: String
    let command: CommandProtocol
    
}

class UISegmentView: UIView {
    
    private var selectedIndex: Int? = 0
    
    var controls = NSHashTable<UIControl>.weakObjects()
    
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.spacing = 10
        view.distribution = .equalSpacing
        view.axis = .horizontal
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func configure() {
        
        addSubview(scrollView)
        scrollView.edges(to: self)
        
        scrollView.addSubview(stackView)
        stackView.edges(to: scrollView,
                        top: layoutMargins.top,
                        left: layoutMargins.left,
                        right: -layoutMargins.right)
        
    }
    
    func setContent(_ content: [UISegment]) {
        
        stackView.arrangedSubviews.forEach {
            stackView.removeArrangedSubview($0)
        }
        controls.removeAllObjects()
        
        content.enumerated().forEach {
            
            let model = ButtonModel(title: $0.element.title, command: $0.element.command)
            let layout = MediumLayout(titleFont: .preferredFont(forTextStyle: .subheadline), cornerRadii: 32, margins: .init(top: 8, left: 16, bottom: 8, right: 16))
            let state = ButtonState(normal: ControlState(backgroundColor: Color().systemGray6, textColor: Color().label), selected: ControlState(backgroundColor: Color().systemRed, textColor: .white))
            let factory = PrimaryButton(layout: layout, model: model, state: state)
            let control = factory.make()
            control.tag = $0.offset
            
            control.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
            
            stackView.addArrangedSubview(control)
            
            controls.add(control)
        }
        
    }
    
    func setSelectedIndex(_ index: Int) {
                
        controls.allObjects.first { $0.tag == index }?.sendActions(for: .touchUpInside)
        
    }
    
    @objc func didSelect(_ control: UIControl) {
        
        controls.allObjects.filter { $0 != control }.forEach { $0.isSelected = false }
                
        control.isSelected = !control.isSelected
                
        selectedIndex = controls.allObjects.first { $0.isSelected }?.tag

    }
}
