//
//  ButtonLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol ButtonLayout {
    
    var lineWidth: CGFloat { get }
        
    var cornerRadii: CGFloat { get }
    
    var margins: UIEdgeInsets { get }
    
    var titleFont: UIFont { get }
    
    var isEqualScreenWidth: Bool { get }
    
    func makeRounded(for rect: CGRect, strokeColor: UIColor?) -> CAShapeLayer
}

extension ButtonLayout {
    
    var isEqualScreenWidth: Bool { false }
    
    var lineWidth: CGFloat { .zero }
    
    var titleFont: UIFont {
        UIFont.preferredFont(forTextStyle: .body).withSize(17.0)
    }
    
    func makeRounded(for rect: CGRect, strokeColor: UIColor?) -> CAShapeLayer {
        
        let path = UIBezierPath(roundedRect: rect,
                                byRoundingCorners: [.allCorners],
                                cornerRadii: CGSize(width: cornerRadii,
                                                    height: cornerRadii))
        path.close()
        
        if let color = strokeColor {
            color.setStroke()
            path.lineWidth = lineWidth
            path.stroke()
        }
        
        let shape = CAShapeLayer()
        
        shape.path = path.cgPath
        
        return shape
    }

}
