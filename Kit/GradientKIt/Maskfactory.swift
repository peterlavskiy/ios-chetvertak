//
//  Maskfactory.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 22.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct Maskfactory: GradientFactoryProtocol {
    
    let decoratee: GradientFactoryProtocol
    
    let backgroundColor: UIColor
    
    let alpha: CGFloat
    
    let filter: String
    
    func make(in view: UIView) -> CAGradientLayer {
        
        let bounds = view.bounds
        let layer = decoratee.make(in: view)
        
        layer.bounds = bounds.insetBy(dx: -0.5*bounds.size.width, dy: -0.5*bounds.size.height)
        layer.position = view.center
        layer.compositingFilter = filter
        layer.backgroundColor = backgroundColor.cgColor
        
        view.alpha = alpha
        
        return layer
    }
    
    static func makeMagazine() -> GradientFactoryProtocol {
        
        let backgroundColor = Color(lightOnly: true).brandBlue
        let alpha: CGFloat = 0.5
        
        return Maskfactory(decoratee: GradientFactory(colors: [], transform: nil),
                           backgroundColor: backgroundColor,
                           alpha: alpha,
                           filter: "softLightBlendMode")
    }
}
