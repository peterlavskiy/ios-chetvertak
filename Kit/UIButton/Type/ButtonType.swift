//
//  ButtonType.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol ButtonType {
    
    var strokeColor: UIColor? { get }
    
    var title: UILabelConfiguration { get }
    
    func make() -> Button
}
extension ButtonType {
    
    var strokeColor: UIColor? { nil }
}
