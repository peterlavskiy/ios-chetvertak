//
//  MainImageProvider.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 22.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

struct ResultsImageProvider: ImageProviderProtocol {
    
    var imageContentMode: UIView.ContentMode = .scaleAspectFit
    
    var image: UIImage?
    
    var leftImageView: LeftImageViewLayout?    
    
    func getImage(_ url: URL?, completion: @escaping (UIImage?) -> Void) -> ImageOperationProtocol? {
        nil
    }
    
    class ImageOperation: ImageOperationProtocol {
        func cancel() {}
    }
    
    func getAssetImage() -> UIImage? {
        nil
    }
    
    func getAssetImage(named: String?) -> UIImage? {
        nil
    }
    
    func getImage(completion: ((UIImage?) -> Void)?) -> ImageOperationProtocol? {
        completion?(image)
        return ImageOperation()
    }
    
}

struct MainImageProvider: ImageProviderProtocol {
    
    var imageContentMode: UIImageView.ContentMode = .scaleAspectFill
    
    var imageName: String?
    
    var leftImageView: LeftImageViewLayout?
    
    var image: UIImage?
    
    var renderingMode: UIImage.RenderingMode = .automatic

    class ImageOperation: ImageOperationProtocol {
        func cancel() {}
    }
    
    func getImage(_ url: URL?, completion: @escaping (UIImage?) -> Void) -> ImageOperationProtocol? {
        ImageOperation()
    }
    
    func getAssetImage() -> UIImage? {
        guard let name = imageName else { return nil }
        return UIImage(named: name)?.withRenderingMode(renderingMode)
    }
    
    func getAssetImage(named: String?) -> UIImage? {
        guard let name = named else { return nil }
        return UIImage(named: name)?.withRenderingMode(renderingMode)
    }

    func getImage(completion: ((UIImage?) -> Void)?) -> ImageOperationProtocol? {
        completion?(getAssetImage())
        return ImageOperation()
    }
}
