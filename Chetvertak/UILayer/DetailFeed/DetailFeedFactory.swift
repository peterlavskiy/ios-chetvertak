//
//  DetailFeedFactory.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 25.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

class DetailFeedFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider
    var footer: CardFooterBuilderProtocol?
    var image: UIImage?
    
    init(colorProvider: ColorProvider,
         footer: CardFooterBuilderProtocol? = nil,
         image: UIImage? = nil) {
        self.colorProvider = colorProvider
        self.footer = footer
        self.image = image
    }

    func makeViewController() -> UIViewController {
        
        let composer = UITableViewComposer()
        let cardView = OuputCardView.loadFromNib(withName: OuputCardView.self,
                                               translatesAutoresizingMaskIntoConstraints: false)
        cardView?.backgroundImageView.image = image
        cardView?.configure(footer)
        
        let viewController = DetailFeedViewController(composer: composer)
        colorProvider.views.add(viewController)
        viewController.cardView = cardView
        return viewController
    }
    
}
