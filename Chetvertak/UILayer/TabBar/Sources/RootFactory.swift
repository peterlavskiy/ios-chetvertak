//
//  RootFactory.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 25.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

struct RootFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider
    
    var main: UIViewControllerFactory {
        MainFactory(colorProvider: colorProvider)
    }
    
    var competitions: UIViewControllerFactory {
        CompetitionsFactory(colorProvider: colorProvider)
    }
    
    var more: UIViewControllerFactory {
        MoreFactory(colorProvider: colorProvider)
    }
    
    func makeViewController() -> UIViewController {
        
        let viewController = RootViewController(nibName: nil, bundle: nil)
        colorProvider.views.add(viewController)

        let firstViewController: UIViewController = main.makeEmbededIn()

        let secondViewController: UIViewController = competitions.makeEmbededIn()
        
        let thirdViewController: UIViewController = more.makeEmbededIn()
        
        let content = [firstViewController, secondViewController, thirdViewController]
        
        viewController.setViewControllers(content,
                                          animated: false)
        
        return viewController
    }
    
}
