//
//  MediumLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct MediumLayout: ButtonLayout {
    
    var titleFont: UIFont 
    
    var lineWidth: CGFloat = 1.5
    
    var cornerRadii: CGFloat = 8.0
    
    var margins: UIEdgeInsets = .init(top: 11.0, left: 64.0, bottom: 11.0, right: 64.0)
}
