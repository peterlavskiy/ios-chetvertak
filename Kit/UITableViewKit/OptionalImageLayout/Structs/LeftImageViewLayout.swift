//
//  LeftImageViewLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 12.04.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct LeftImageViewLayout: LeftImageViewLayoutProtocol {
    
    let leadingConstant: CGFloat
    
    let traillingConstant: CGFloat
    
    var topConstant: CGFloat?
                
    let layout: OptionalImageViewLayoutProtocol
    
    func make(in superview: UIView,
              centerAnchor: NSLayoutYAxisAnchor?,
              leadingAnchor: NSLayoutXAxisAnchor,
              inactive: [NSLayoutConstraint?]) -> UIImageView? {
                
        let view = layout.make(in: superview)
        
        if let topConstant = topConstant {
            view?.topAnchor.constraint(equalTo: superview.topAnchor, constant: topConstant).isActive = true
        }
        
        if let centerAnchor = centerAnchor, topConstant == nil {
            view?.centerYAnchor.constraint(equalTo: centerAnchor).isActive = true
        }
        
        view?.leadingAnchor.constraint(equalTo: superview.leadingAnchor,
                                      constant: leadingConstant).isActive = true
        
        let trailingConstraint = view?.rightAnchor.constraint(equalTo: leadingAnchor,
                                                                 constant: -traillingConstant)
        trailingConstraint?.priority = .defaultHigh
        trailingConstraint?.isActive = true
        
        inactive.forEach { $0?.isActive = false }
        
        return view
    }
    
    func make(in superview: UIView) -> UIImageView? {
        layout.make(in: superview)
    }
}
