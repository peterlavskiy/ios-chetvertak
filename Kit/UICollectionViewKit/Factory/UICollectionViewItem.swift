//
//  UICollectionViewItem.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UICollectionViewItem: UICollectionViewComponent {

    let reuseItem: UIReusableItemProtocol

    let builder: UICollectionViewItemFactoryProtocol
    
    var component: UICollectionViewComponentFactoryProtocol { builder }
    
    var command: CommandProtocol? { builder.command }
    
    var reuseIdentifier: String { builder.reuseIdentifier }
    
    var nib: UINib? { reuseItem.nib }
     
    var cellClass: AnyClass.Type? { reuseItem.cellClass }
    
    func make(_ view: UICollectionViewCell?) -> UICollectionViewCell {
        builder.make(view)
    }
}
