//
//  UICollectionViewCompositeItem.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UICollectionViewCompositeItem: UICollectionViewComponent {
    
    let reuseItem: UIReusableItemProtocol
    
    let builder: UICollectionViewSectionFactoryProtocol
    
    var component: UICollectionViewComponentFactoryProtocol { builder }
    
    var reuseIdentifier: String { builder.reuseIdentifier }
    
    var numberOfItems: Int { builder.numberOfItems }
    
    var nib: UINib? { reuseItem.nib }
     
    var cellClass: AnyClass.Type? { reuseItem.cellClass }
    
    subscript(index: Int) -> UICollectionViewComponent {
        builder[index]
    }
    
    func make(_ view: UICollectionReusableView?) -> UICollectionReusableView {
        builder.make(view)
    }
}
