//
//  UIViewMotionLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 23.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct Highlighter {
    
    let animator = UIViewPropertyAnimator(duration: 0.2, curve: .easeOut)
    
    func make(in view: UIView, isHighlighted: Bool) {
                
        let transform: CGAffineTransform = isHighlighted ? .init(scaleX: 0.95, y: 0.95) : .identity
        
        animator.stopAnimation(true)
        
        animator.addAnimations {
            view.transform = transform
        }
        
        animator.startAnimation()
    }
}
