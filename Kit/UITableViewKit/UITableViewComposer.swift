//
//  UITableViewComposer.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 04.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol UITableViewComposerProtocol: UITableViewDataSource, UITableViewDelegate {
    
    var delegate: UITableViewDelegate? { get set }
    
    func setContent(_ content: [UITableViewComponent])
}

class UITableViewComposer: NSObject, UITableViewComposerProtocol {
    
    var content: [UITableViewComponent]
    
    weak var delegate: UITableViewDelegate?
    
    init(delegate: UITableViewDelegate? = nil,
         content: [UITableViewComponent] = []) {
        
        self.content = content
        self.delegate = delegate
    }
    
    func setContent(_ content: [UITableViewComponent]) {
        
        self.content = content
    }
    
}

extension UITableViewComposer: UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        delegate?.scrollViewDidScroll?(scrollView)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        content[indexPath.section][indexPath.row].command?.execute()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView,
                   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        content[indexPath.section][indexPath.row].estimatedHeight
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        content[indexPath.section][indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView,
                   estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        
        content[section].estimatedHeight
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        
        content[section].height
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        .leastNormalMagnitude
    }
    
}

extension UITableViewComposer: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        content.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let item = content[section]
        
        let section = tableView.dequeueReusableHeaderFooter(withItem: item)
        
        return item.make(section)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        nil
    }
    
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        content[section].numberOfItems
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = content[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withItem: item, for: indexPath)
        
        return item.make(cell)
    }
    
}

fileprivate extension UITableView {
    
    func dequeueReusableHeaderFooter(withItem item: UIReusableItemProtocol) -> UITableViewHeaderFooterView? {
        
        registerHeader(withItem: item)
        
        return dequeueReusableHeaderFooterView(withIdentifier: item.reuseIdentifier)
    }
    
    func dequeueReusableCell(withItem item: UIReusableItemProtocol,
                             for indexPath: IndexPath) -> UITableViewCell {
        
        registerCell(withItem: item)
        
        return dequeueReusableCell(withIdentifier: item.reuseIdentifier, for: indexPath)
    }
    
    func registerHeader(withItem item: UIReusableItemProtocol) {
        
        if let nib = item.nib {
            register(nib, forHeaderFooterViewReuseIdentifier: item.reuseIdentifier)
        }
        
        if let cellClass = item.cellClass {
            register(cellClass, forHeaderFooterViewReuseIdentifier: item.reuseIdentifier)
        }
        
    }
    
    func registerCell(withItem item: UIReusableItemProtocol) {
                
        if let nib = item.nib {
            register(nib, forCellReuseIdentifier: item.reuseIdentifier)
        }
        
        if let cellClass = item.cellClass {
            register(cellClass, forCellReuseIdentifier: item.reuseIdentifier)
        }
        
    }
}

struct UIReusableItem: UIReusableItemProtocol {
    
    var reuseIdentifier: String
    
    var nib: UINib?
    
    var cellClass: AnyClass.Type?
    
}

public protocol UIReusableItemProtocol {
    
    var reuseIdentifier: String { get }
    
    var nib: UINib? { get }
    
    var cellClass: AnyClass.Type? { get }
    
}
