//
//  ControlContainerView.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 26.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class ControlContainerView: UIView {
    
    lazy var stackView: UIStackView = {
        
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .equalSpacing
        view.alignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        NSLayoutConstraint.activate([
            view.centerYAnchor.constraint(equalTo: centerYAnchor),
            view.leftAnchor.constraint(equalTo: leftAnchor),
            view.rightAnchor.constraint(equalTo: rightAnchor)
        ])
        
        return view
    }()
    
    func configure(controls: [ControlFactory]) {
        
        guard stackView.arrangedSubviews.count != controls.count else { return }
        
        controls.forEach {
            
            stackView.addArrangedSubview($0.make())
            
        }

    }
}
