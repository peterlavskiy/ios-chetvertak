//
//  PrivacyViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 09.08.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

struct PrivacyFactory: UIViewControllerFactory {
        
    func makeViewController() -> UIViewController {
        let viewController = PrivacyViewController(nibName: nil, bundle: nil)
        
        return viewController
    }
    
}

class PrivacyViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Пользовательское соглашение"
    }

}
