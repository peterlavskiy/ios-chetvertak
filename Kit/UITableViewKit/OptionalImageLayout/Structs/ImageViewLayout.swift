//
//  ImageViewLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 12.04.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct ImageViewLayout: OptionalImageViewLayoutProtocol {
        
    var height: CGFloat = 32
    var width: CGFloat = 32
    var cornerRadii: CGFloat = .zero

    var layout: OptionalImageViewLayoutProtocol { self }
    
    func make(in superview: UIView) -> UIImageView? {
        
        let view = UIImageView()
        view.makeCircularRoundCorners(cornerRadius: cornerRadii)
        view.translatesAutoresizingMaskIntoConstraints = false
        superview.addSubview(view)
                
        view.heightAnchor.constraint(equalToConstant: height).isActive = true
        view.widthAnchor.constraint(equalToConstant: width).isActive = true

        return view
    }
}
