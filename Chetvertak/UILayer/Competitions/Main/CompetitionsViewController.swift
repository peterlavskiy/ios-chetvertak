//
//  CompetitionsViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 03.08.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit
import SwiftDate

struct CompetitionsFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider
    
    func makeViewController() -> UIViewController {
        
        let viewController = CompetitionsViewController(colorProvider: colorProvider)
        colorProvider.views.add(viewController)
        
        viewController.tabBarItem.title = "Соревнования"
        viewController.tabBarItem.selectedImage = UIImage(named: "contest_active")?.withRenderingMode(.alwaysTemplate)
        viewController.tabBarItem.image = UIImage(named: "contest_normal")?.withRenderingMode(.alwaysTemplate)
        
        return viewController
    }
    
}

class CompetitionsViewController: UIViewController {
    
    let composer = UITableViewComposer()
    
    let colorProvider: ColorProvider
    
    let service = CompetitionsService()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .none
        }
    }
    
    init(colorProvider: ColorProvider) {
        self.colorProvider = colorProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = composer
        tableView.delegate = composer
        
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.title = "Соревнования"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        class SegmentCommand: CommandProtocol {
            
            func execute() {
                
            }
            
        }
        
        let content = ["Все", "Россия", "Международные", "Региональные"].map {
            UISegment(title: $0, command: SegmentCommand())
        }
        let segmentView = UISegmentView(frame: .init(origin: .zero, size: .init(width: UIScreen.main.bounds.width, height: 52)))
        segmentView.layoutMargins = .init(top: 8.0, left: 16.0, bottom: 0, right: 16.0)
        segmentView.configure()
        segmentView.setContent(content)
        tableView.tableHeaderView = segmentView
        
        segmentView.setSelectedIndex(0)
        
        loadData()
    }
    
    func loadData() {
        
        service.getProducts { [weak self] in
            switch $0 {
            case .success(let entity):
                let content = [makeCompetitionSection(entity, title: "Ближайшие",
                                                      command: PushCommand(source: self, destination: FutureCompetitionsFactory(colorProvider: colorProvider))),
                               makeResultSection(entity, title: "Результаты", command: PushCommand(source: self, destination: AllResultsFactory(colorProvider: colorProvider)))]
                self?.composer.setContent(content)
                self?.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func makeCompetitionSection(_ content: CompetitionResponseEntity,
                                title: String,
                                command: CommandProtocol) -> UITableViewComponent {
        
        typealias View = UITableViewSectionHeader
        
        let model = UITableViewSectionHeader.Model(title: title)
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold)), textColor: Color().label)
        
        
        let configuration = UITableViewSectionHeader.UIConfiguration(title: titleConfiguration, accessoryIsHidden: false, accessoryCommand: command)
        
        let items = content.result.enumerated().map(makeCompetitionCell)
        
        let factory = UITableViewFactoryComponent<View>(model: model,
                                                        configuration: configuration,
                                                        estimatedHeight: 44.0)
        
        return factory.buildSection(View.self, items: items)
        
    }
    
    func makeResultSection(_ content: CompetitionResponseEntity,
                           title: String,
                           command: CommandProtocol) -> UITableViewComponent {
        
        typealias View = UITableViewSectionHeader
        
        let model = UITableViewSectionHeader.Model(title: title)
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold)), textColor: Color().label)
        
        
        let configuration = UITableViewSectionHeader.UIConfiguration(title: titleConfiguration, accessoryIsHidden: false, accessoryCommand: command)
        
        let items = content.result.enumerated().map(makeResultCell)
        
        let factory = UITableViewFactoryComponent<View>(model: model,
                                                        configuration: configuration,
                                                        estimatedHeight: 44.0)
        
        return factory.buildSection(View.self, items: items)
        
    }
    
    func makeCompetitionCell(offset: Int, content: CompetitionEntity) -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let command = PushCommand(source: self, destination: CompetitionsDetailFactory(colorProvider: colorProvider))
        
        let title = DateFactory(content: content).makeString().uppercased()
        
        let imageLayout = LeftImageViewLayout(leadingConstant: 16, traillingConstant: 16.0, topConstant: 16.0, layout: ImageViewLayout(height: 64, width: 64, cornerRadii: 32.0))
        let imageProvider = MainImageProvider(imageName: content.imageURL, leftImageView: imageLayout)
        
        let model = OutputRightTableViewCellModel(title: title,
                                                  subTitle: content.title,
                                                  value: content.discipline.title,
                                                  teriary: "📍\(content.location.title)")
        
        let backgroundColor = offset % 2 == 0 ? Color().highlighter : Color().systemBackground
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .footnote).scaledFont(for: .systemFont(ofSize: 13.0, weight: .semibold)), textColor: Color().systemIndigo)
        
        let subtitleConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: .systemFont(ofSize: 15.0, weight: .semibold)), textColor: Color().label, numberOfLines: 1)
        
        let valueConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .caption1).scaledFont(for: .systemFont(ofSize: 12.0, weight: .semibold)), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let teriaryConfiguration: UILabelConfiguration = .makeTeriary(font: UIFontMetrics(forTextStyle: .caption2).scaledFont(for: .systemFont(ofSize: 10.0, weight: .regular)), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let configuration: OutputRightTableViewCellConfiguration = .init(title: titleConfiguration, subTitle: subtitleConfiguration, value: valueConfiguration, valueBackgroundViewColor: Color().systemGray6, valueBackgroundStrokeColor: Color().systemGray4, teriary: teriaryConfiguration, backgrounColor: backgroundColor)
        
        return UITableViewFactoryComponent<View>(model: model,
                                                 configuration: configuration,
                                                 estimatedHeight: 98.0,
                                                 imageProvider: imageProvider,
                                                 command: command).buildCell(View.self)
    }
    
    func makeResultCell(offset: Int, content: CompetitionEntity) -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let command = PushCommand(source: self, destination: ResultsCompetitionsFactory(colorProvider: colorProvider))
        
        let title = DateFactory(content: content).makeString().uppercased()
        
        let imageLayout = LeftImageViewLayout(leadingConstant: 16, traillingConstant: 16.0, topConstant: 16.0, layout: ImageViewLayout(height: 64, width: 64, cornerRadii: 32.0))
        let imageProvider = MainImageProvider(imageName: content.imageURL, leftImageView: imageLayout)
        
        let model = OutputRightTableViewCellModel(title: title,
                                                  subTitle: content.title,
                                                  value: content.discipline.title,
                                                  teriary: "📍\(content.location.title)")
        
        let backgroundColor = Color().systemBackground
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .footnote).scaledFont(for: .systemFont(ofSize: 13.0, weight: .semibold)), textColor: Color().systemIndigo)
        
        let subtitleConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: .systemFont(ofSize: 15.0, weight: .semibold)), textColor: Color().label, numberOfLines: 1)
        
        let valueConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .caption1).scaledFont(for: .systemFont(ofSize: 12.0, weight: .semibold)), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let teriaryConfiguration: UILabelConfiguration = .makeTeriary(font: UIFontMetrics(forTextStyle: .caption2).scaledFont(for: .systemFont(ofSize: 10.0, weight: .regular)), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let configuration: OutputRightTableViewCellConfiguration = .init(title: titleConfiguration, subTitle: subtitleConfiguration, value: valueConfiguration, valueBackgroundViewColor: Color().systemGray6, valueBackgroundStrokeColor: Color().systemGray4, teriary: teriaryConfiguration, backgrounColor: backgroundColor)
        
        return UITableViewFactoryComponent<View>(model: model,
                                                 configuration: configuration,
                                                 estimatedHeight: 98.0,
                                                 imageProvider: imageProvider,
                                                 command: command).buildCell(View.self)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        loadData()
    }
    
}

class CompetitionsService {
    
    func getProducts(_ completion: (Result<CompetitionResponseEntity, Error>) -> Void) {
        
        if let path = Bundle.main.path(forResource: "CompetitionsStub", ofType: "json") {
            do {
                
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                
                let decoded = try JSONDecoder().decode(CompetitionResponseEntity.self, from: data)
                
                completion(.success(decoded))
                
            } catch {
                completion(.failure(error))
            }
        }
        
    }
    
    func getResults(_ completion: (Result<CompetitionResultResponseEntity, Error>) -> Void) {
        
        if let path = Bundle.main.path(forResource: "CompetitionsResultsStub", ofType: "json") {
            do {
                
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                
                let decoded = try JSONDecoder().decode(CompetitionResultResponseEntity.self, from: data)
                
                completion(.success(decoded))
                
            } catch {
                completion(.failure(error))
            }
        }
        
    }
    
}

struct DisciplineEntiy: Codable {
    
    let id: Int
    let title: String
}

struct CompetitionTypeEntity: Codable {
    
    let id: Int
    let image: String
}

struct CompetitionLocation: Codable {
    
    let title: String
}

struct CompetitionEntity: Codable {
    
    let id: Int
    let title: String
    let imageURL: String
    let location: CompetitionLocation
    let discipline: DisciplineEntiy
    let dateStart: String
    let dateEnd: String
    let type: CompetitionTypeEntity
    
}

struct CompetitionResponseEntity: Codable {
    
    let result: [CompetitionEntity]
}

struct DateFactory {
    
    let content: CompetitionEntity
    
    func makeString() -> String {
        
        let startDate = content.dateStart.toISODate()
        
        let endDate = content.dateEnd.toISODate()
        
        if startDate?.month == endDate?.month {
            
            let startDate = startDate?.toFormat("dd", locale: Locale.current) ?? ""
            
            let endDate = endDate?.toFormat("dd MMM", locale: Locale.current) ?? ""
            
            return "\(startDate) - \(endDate)"
            
        } else {
            
            let month = String(describing: startDate?.month ?? 1)
            
            let startDate = startDate?.toFormat("dd", locale: Locale.current) ?? ""
            
            let endDate = endDate?.toFormat("dd MMM", locale: Locale.current) ?? ""
            
            return "\(startDate).\(month) - \(endDate)"
        }
    }
    
}

fileprivate extension Color {
    
    var highlighter: UIColor {
        
        let anyLight = UIColor(red: 0.898, green: 0.937, blue: 1, alpha: 1)
        let darkColor = UIColor(red: 0.084, green: 0.124, blue: 0.267, alpha: 1)
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return darkColor
                } else {
                    return anyLight
                }
            }
        } else {
            return anyLight
        }
    }
    
}
