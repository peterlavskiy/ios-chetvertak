//
//  Color.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 17.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class ColorProvider {
    
    let views = NSHashTable<UIViewController>.weakObjects()
    var tint: UIColor
    var brand: UIColor
    
    var isDarkAuto: Bool = false {
        didSet {
            change(UIScreen.main.traitCollection)
        }
    }
    
    var isDark: Bool
    
    init(tint: UIColor, brand: UIColor, isDark: Bool = false) {
        self.tint = tint
        self.brand = brand
        self.isDark = isDark
    }
    
    func change(_ brand: UIColor) {
        self.brand = brand
    }
    
    func change(_ traitCollection: UITraitCollection) {
                
        isDark = traitCollection.userInterfaceStyle == .dark        
    }
    
    func enableDarkMode() {
        
        isDark = true
        
        notifyViews()
    }
    
    func disableDarkMode() {
        
        isDark = false
        
        notifyViews()
    }
    
    func notifyViews() {
        views.allObjects.forEach {
            animateView($0)
        }
    }
    
    func animateView(_ viewController: UIViewController) {
        UIView.transition(with: viewController.view,
                          duration: 0.5,
                          options:[.beginFromCurrentState, .transitionCrossDissolve],
                          animations: { [weak self] in
                            if #available(iOS 13.0, *) {
                                viewController.overrideUserInterfaceStyle = self?.isDark == true ? .dark : .light
                            }
        }, completion: nil)
    }
    
}

struct Color {
    
    var trait: UITraitCollection?
    
    var alpha: CGFloat = 1.0
    
    var lightOnly: Bool = false
    
    var navy: UIColor {
        // в декоратор
        let anyLight = UIColor(red: 0, green: 0.071, blue: 0.188, alpha: 1)
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return UIColor(red: 0.004, green: 0.051, blue: 0.122, alpha: 1)
                } else {
                    return anyLight
                }
            }
        } else {
            return anyLight
        }
    }
    
    var quaternaryLabel: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.quaternaryLabel.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 0.18).withAlphaComponent(alpha)
        }
    }
    
    var secondaryLabel: UIColor {
        
        if #available(iOS 13.0, *) {
            return UIColor.secondaryLabel.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 0.6).withAlphaComponent(alpha)
        }
    }
    
    var systemRed: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemRed.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.345, green: 0.337, blue: 0.839, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var systemIndigo: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemIndigo.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.345, green: 0.337, blue: 0.839, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var systemGray: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemGray.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.949, green: 0.949, blue: 0.969, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var systemGray4: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemGray4.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.82, green: 0.82, blue: 0.839, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var systemGray5: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemGray5.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.898, green: 0.898, blue: 0.918, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var systemGray6: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemGray6.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.949, green: 0.949, blue: 0.969, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var systemBackground: UIColor {
        
        if #available(iOS 13.0, *) {
            return UIColor.systemBackground.withAlphaComponent(alpha)
        } else {
            return UIColor.white.withAlphaComponent(alpha)
        }
    }
    
    var secondarySystemBackground: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.secondarySystemBackground.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.937, green: 0.937, blue: 0.957, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var tertiaryLabel: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.tertiaryLabel.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 0.3).withAlphaComponent(alpha)
        }
    }
    
    var label: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.label.withAlphaComponent(alpha)
        } else {
            return UIColor(red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(alpha)
        }
    }
    
    var tertiarySystemFill: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.tertiarySystemFill.withAlphaComponent(alpha).resolvedColor(with: trait ?? .current)
        } else {
            return UIColor(red: 0.851, green: 0.851, blue: 0.851, alpha: 0.3).withAlphaComponent(alpha)
        }
    }
    
    var brandBlue: UIColor {
        
        let anyLight = (UIColor(hex: "#1229F5") ?? .blue)
        
        var result: UIColor = anyLight
        
        guard !lightOnly else { return result.withAlphaComponent(self.alpha) }
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    result = UIColor(red: 0.216, green: 0.294, blue: 0.996, alpha: 1)
                } else {
                    result = anyLight
                }
                return result.withAlphaComponent(self.alpha)
            }
        } else {
            result = anyLight
        }
        
        return result.withAlphaComponent(alpha)
    }
    
    var shadowColor: UIColor {
        
        let anyLight = UIColor(red: 0.898, green: 0.898, blue: 0.918, alpha: 1)
        
        var result: UIColor = anyLight
        
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    result = UIColor.clear
                } else {
                    result = anyLight
                }
                return result.withAlphaComponent(self.alpha)
            }
        } else {
            result = anyLight
        }
        
        return result.withAlphaComponent(alpha)
    }
    
}

@available(iOS 13.0, *)
extension UIColor {
    
    static var brandBlue: UIColor {
        UIColor { (traitcollection: UITraitCollection) -> UIColor in
            if traitcollection.userInterfaceStyle == .dark {
                return UIColor(hex: "#1229F5") ?? UIColor.blue
            } else {
                return UIColor(hex: "#1229F5") ?? UIColor.blue
            }
        }
    }
}

fileprivate extension UIColor {
    
    convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}

extension UIColor {
    func isEqual(color: UIColor?) -> Bool {
        guard let color = color else { return false }
        
        var red:CGFloat   = 0
        var green:CGFloat = 0
        var blue:CGFloat  = 0
        var alpha:CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        var targetRed:CGFloat   = 0
        var targetGreen:CGFloat = 0
        var targetBlue:CGFloat  = 0
        var targetAlpha:CGFloat = 0
        color.getRed(&targetRed, green: &targetGreen, blue: &targetBlue, alpha: &targetAlpha)
        
        return (Int(red*255.0) == Int(targetRed*255.0) && Int(green*255.0) == Int(targetGreen*255.0) && Int(blue*255.0) == Int(targetBlue*255.0) && alpha == targetAlpha)
    }
}
