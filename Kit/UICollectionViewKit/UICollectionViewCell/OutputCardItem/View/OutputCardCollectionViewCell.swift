//
//  OutputCardCollectionViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 31.03.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class OutputCardCollectionViewCell: UICollectionViewCell, CollectionViewCardProtocol {
    
    static var reuseIdentifier: String { "\(OutputCardCollectionViewCell.self)" }
    
    typealias Model = OutputCardCollectionViewItemModel
    
    typealias UIConfiguration = OutputCardUIConfiguration
        
    var cornerRadius: CGFloat = 16.0
        
    var isCustomBackgroundColor = false
    
    lazy var customBackgroundView: UIView = {
        
        let view = UIView(frame: bounds)

        return view
    }()
    
    private weak var colorProvider: ColorProvider?
        
    private weak var gradientLayer: CAGradientLayer?
    
    override var isHighlighted: Bool {
        didSet {
            highlighter?.make(in: self, isHighlighted: isHighlighted)
        }
    }
    
    // MARK: Shadows
    
    var shadowFactory: ShadowFactory?
    
    // MARK: Toolbar
    
    var toolbar: UIView?
    
    // MARK: Card
    
    weak var cardView: OuputCardViewProtocol?
    
    var highlighter: Highlighter?
        
    // MARK: Header
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!

    @IBOutlet weak var headerRightImageView: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerSubtitleLabel: UILabel!
    
    // MARK: Footer
    
    var footerView: OutputCardFooterViewProtocol?
    
    // MARK: BackgroundImageView
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var backgroundImageHeight: NSLayoutConstraint!
    
    override func layoutSubviews() {
        super.layoutSubviews()
                
        makeShadows()
    }
    
    func configure(_ card: OutputCardFooterViewProtocol) {
        
    }
    
    func configure(_ model: OutputCardCollectionViewItemModel,
                   imageProvider: ImageProviderProtocol?) {
                
        headerTitleLabel.text = model.header?.title
        headerSubtitleLabel.text = model.header?.subtitle
        
        headerRightImageView.image = imageProvider?.getAssetImage(named: model.header?.imageName)
        backgroundImageView.image = imageProvider?.getAssetImage()
        backgroundImageView.contentMode = imageProvider?.imageContentMode ?? UIView.ContentMode.scaleAspectFill
    }
    
    func configureUI(_ configuration: OutputCardUIConfiguration) {
        
        cornerRadius = configuration.cornerRadii
        shadowFactory = configuration.shadowFactory
        highlighter = configuration.highlighter
        
        configureHeader(configuration)
        configureBackground(configuration)
        configureGradient(configuration)

        layoutSubviews()
    }
    
    func configure(footer: CardFooterBuilderProtocol?) {
        
        cardView?.configure(footer)
        
        footerView = footer?.build(footerView,
                                   in: contentView,
                                   align: backgroundImageView.bottomAnchor)
    }
    
    func configure(header: CardHeaderBuilderProtocol?) {
        
    }
    
    private func configureHeader(_ configuration: OutputCardUIConfiguration) {
        
        headerView.isHidden = configuration.isHiddenFooter
        
        if let titleConfig = configuration.header?.title {
            headerTitleLabel.configure(titleConfig)
        }
        if let subtitleConfig = configuration.header?.subtitle {
            headerSubtitleLabel.configure(subtitleConfig)
        }
    }
    
    private func configureBackground(_ configuration: OutputCardUIConfiguration) {
        
        if configuration.isRounded {
            makeRoundedCard()
        }
        
        isCustomBackgroundColor = configuration.gradient != nil
                
        backgroundColor = configuration.backgroundColor ?? Color().secondarySystemBackground
        
        if configuration.backgroundColor != nil {
            isCustomBackgroundColor = true
            contentView.backgroundColor = configuration.backgroundColor
            backgroundImageView.backgroundColor = configuration.backgroundColor
        }
        
        if let height = configuration.backgroundImageHeight {
            backgroundImageHeight.constant = height
        }
        
        backgroundImageView.makeSmoothRoundCorners(cornerRadius: configuration.backgroundImageCornerRadii)
        
    }
    
    private func configureGradient(_ configuration: OutputCardUIConfiguration) {
        
        if let gradientLayer = configuration.gradient?.make(in: contentView) {
            if let oldLayer = self.gradientLayer {
                contentView.layer.replaceSublayer(oldLayer, with: gradientLayer)
            } else {
                contentView.layer.insertSublayer(gradientLayer, at: 0)
            }
            self.gradientLayer = gradientLayer
        }
        
        if let gradientLayer = configuration.imageGradient?.make(in: backgroundImageView) {
            if let oldLayer = self.gradientLayer {
                backgroundImageView.layer.replaceSublayer(oldLayer, with: gradientLayer)
            } else {
                backgroundImageView.layer.insertSublayer(gradientLayer, at: 0)
            }
            self.gradientLayer = gradientLayer
        }
            
    }
    
    private func makeShadows() {
        
        shadowFactory?.make(in: layer)
    }
    
    private func makeRoundedCard() {
        makeSmoothRoundCorners(cornerRadius: cornerRadius)
        contentView.makeSmoothRoundCorners(cornerRadius: cornerRadius)
    }
    
}
