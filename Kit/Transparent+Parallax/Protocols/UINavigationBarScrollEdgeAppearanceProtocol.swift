//
//  UINavigationBarScrollEdgeAppearanceProtocol.swift
//
//  Created by Lavskiy Peter on 31.03.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//
import UIKit

public protocol UINavigationBarScrollEdgeAppearanceProtocol: UIViewController {
    
    var navigationBar: UINavigationBar? { get}
    
    var alpha: CGFloat { get }
    
    var maxTranslation: CGFloat { get }
    
    var scrollViewContentY: CGFloat { get }
    
    var statusBarStyle: UIStatusBarStyle { get }
    
    func setContentOffsetY(_ scrollViewContentY: CGFloat) -> CGFloat
    
    func setAlpha(_ alpha: CGFloat)
    
    func setStatusBarStyle(_ style: UIStatusBarStyle)
            
    func computeTranslation(_ scrollView: UIScrollView, topOffset: CGFloat)
    
}

public extension UINavigationBarScrollEdgeAppearanceProtocol {
    
    var maxTranslation: CGFloat { 0.6 }
    
    func computeTranslation(_ scrollView: UIScrollView, topOffset: CGFloat) {
        
        let alpha = computeTranslation(in: scrollView, topOffset: topOffset)
        
        setAlpha(alpha)
        
        let isStatusBarUpdating = alpha > maxTranslation
        
        let style: UIStatusBarStyle = alpha > maxTranslation ? .default : .lightContent
        
        setStatusBarStyle(style)
                
        navigationBar?.applyTransparentBackground(alpha: alpha,
                                                  isStatusBarUpdating: isStatusBarUpdating)
    }
    
    private func computeTranslation(in scrollView: UIScrollView, topOffset: CGFloat) -> CGFloat {
        
        let contentOffsetY = scrollView.contentOffset.y 
        
        let topInset = scrollView.contentInset.top
        
        let denominator = topInset + 2 * topOffset
        
        switch contentOffsetY {
        case _ where contentOffsetY > 0:
            
            let translation = min(1, scrollViewContentY / denominator)
                        
            return translation
        case _ where contentOffsetY <= 0:
            
            let scrollViewContentY = setContentOffsetY(topInset + contentOffsetY + topOffset)
            
            let translation = min(1, scrollViewContentY / denominator)
            
            return translation
        default:
            return 0
        }
    }
    
}
