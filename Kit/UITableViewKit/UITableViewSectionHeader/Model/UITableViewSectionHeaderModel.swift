//
//  UITableViewSectionHeaderModel.swift
//  PromoUI
//
//  Created by peter.lavskiy on 31/01/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import Foundation

struct UITableViewSectionHeaderModel: ConfigurableImageModel {
    
    var title: String
    
}
