//
//  RootViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 25.07.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

class RootViewController: UITabBarController {
        
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()

    }
    
    func setupUI() {
        
        tabBar.tintColor = .systemRed
        tabBar.barTintColor = Color().systemBackground
                
        if #available(iOS 13.0, *) {

            let appearance = tabBar.standardAppearance
            appearance.backgroundImage = UIImage(color: .white)
            appearance.shadowImage = UIImage()
            appearance.shadowColor = .clear
            tabBar.standardAppearance = appearance
            
        } else {
            tabBar.backgroundImage = UIImage(color: .white)
            tabBar.shadowImage = UIImage()
            tabBar.makeBlurEffect()
        }
        
    }
 
}

extension UIImage {
    
    convenience init(color: UIColor) {
        
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        
        let context = UIGraphicsGetCurrentContext()
                        
        context?.setFillColor(color.cgColor)
        
        UIRectFill(CGRect(x: 0, y: 0, width: 1, height: 1))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()?.ciImage ?? CIImage()
                
        UIGraphicsEndImageContext()
        
        self.init(ciImage: image)
    }
    
}
