//
//  UICollectionViewComponentFactory.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct UICollectionViewComponentFactory<View: ReusableView>: UICollectionViewComponentFactoryProtocol {
    
    let insets: UIEdgeInsets
    let model: View.Model
    let configuration: View.UIConfiguration
    let estimatedSize: CGSize
    let itemSize: CGSize
    let imageProvider: ImageProviderProtocol?
    let isSelected: Bool
    let command: CommandProtocol?
    
    var reuseIdentifier: String
    var cellClass: AnyClass.Type?
    var nib: UINib? = UINib(nibName: String(describing: View.self), bundle: nil)
    var reuseItem: UIReusableItemProtocol {
        UIReusableItem(reuseIdentifier: reuseIdentifier,
                       nib: nib,
                       cellClass: cellClass)
    }
    
    
    init(model: View.Model,
         configuration: View.UIConfiguration,
         insets: UIEdgeInsets = .zero,
         estimatedSize: CGSize,
         itemSize: CGSize = UICollectionViewFlowLayout.automaticSize,
         imageProvider: ImageProviderProtocol? = nil,
         reuseIdentifier: String? = nil,
         isSelected: Bool = false,
         command: CommandProtocol? = nil) {
        
        self.model = model
        self.configuration = configuration
        self.insets = insets
        self.estimatedSize = estimatedSize
        self.itemSize = itemSize
        self.imageProvider = imageProvider
        self.reuseIdentifier = reuseIdentifier ?? View.reuseIdentifier
        self.isSelected = isSelected
        self.command = command
    }
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView? {
        let view = view as? View
        view?.configureUI(configuration)
        view?.configure(model, imageProvider: imageProvider)
        return view
    }
    
    func makeReuseIdentifier() -> String {
        model.isImageAvaliable ? "\(View.reuseIdentifier)Imageable" : View.reuseIdentifier
    }
}
extension UICollectionViewComponentFactory {
    
    func buildSection<T: CollectionViewReusableHeader>(_ view: T.Type, items: [UICollectionViewComponent]) -> UICollectionViewComponent  {
        UICollectionViewSection<T>(builder: self, items: items).build()
    }
    
    func buildItem<T: CollectionViewReusableCell>(_ view: T.Type) -> UICollectionViewComponent {
        UICollectionViewItemFactory<T>(builder: self).build()
    }
    
    func buildCard<T: CollectionViewCardProtocol>(_ view: T.Type,
                                                  footer: CardFooterBuilderProtocol? = nil,
                                                  header: CardHeaderBuilderProtocol? = nil) -> UICollectionViewComponent {
        UICollectionViewCardFactory<T>(builder: self,
                                           footer: footer,
                                           header: header).build()
    }
}






extension UIView {
    class func loadFromNib<T: UIView>(withName nibName: T.Type,
                                      frame: CGRect = .zero,
                                      translatesAutoresizingMaskIntoConstraints: Bool = true) -> T? {
        
        let nib  = UINib.init(nibName: String(describing: nibName.self), bundle: nil)
        let nibObjects = nib.instantiate(withOwner: nil, options: nil)
        for object in nibObjects {
            if let result = object as? T {
                result.frame = frame
                result.translatesAutoresizingMaskIntoConstraints = translatesAutoresizingMaskIntoConstraints
                return result
            }
        }
        return nil
    }
}

extension UIView {
    
    @discardableResult
    func edges(to view: UIView,
               top: CGFloat = 0,
               left: CGFloat = 0,
               bottom: CGFloat = 0,
               right: CGFloat = 0) -> [NSLayoutConstraint] {
        
        let constraints = [
            leftAnchor.constraint(equalTo: view.leftAnchor, constant: left),
            rightAnchor.constraint(equalTo: view.rightAnchor, constant: right),
            topAnchor.constraint(equalTo: view.topAnchor, constant: top),
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottom)
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        return constraints
    }
}
