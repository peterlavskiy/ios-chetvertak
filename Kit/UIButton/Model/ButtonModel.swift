//
//  ButtonModel.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct ButtonModel: ButtonModelProtocol {
    
    var title: String?
        
    var leftIcon: IconProtocol?
    
    var rightIcon: IconProtocol?
    
    var selectedImage: UIImage?
    
    var tintColor: UIColor = .white
    
    var highlighter: Highlighter? = Highlighter()
    
    var command: CommandProtocol?

}

