//
//  UITableViewHeaderParallaxProtocol.swift
//
//  Created by Lavskiy Peter on 31.03.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public typealias UIViewControllerParallaxable = UITableViewHeaderParallaxProtocol & UINavigationBarScrollEdgeAppearanceProtocol

public protocol UITableViewHeaderParallaxProtocol {
    
    var alpha: CGFloat { get }
    
    var parallaxView: UIView { get }
    
    var isTransparencyDisableNeeded: Bool { get }
    
    func computeParallaxFrame(in scrollView: UIScrollView) -> CGRect
    
    func computeParallaxFrame(in scrollView: UIScrollView, heightMultiplier: CGFloat) -> CGRect
    
    func computeParallaxFrame(in scrollView: UIScrollView, heightMultiplier: CGFloat, topOffset: CGFloat) -> CGRect
        
}

public extension UITableViewHeaderParallaxProtocol {
        
    func computeParallaxFrame(in scrollView: UIScrollView) -> CGRect {
        computeParallaxFrame(in: scrollView, heightMultiplier: 2)
    }
    
    func computeParallaxFrame(in scrollView: UIScrollView, heightMultiplier: CGFloat, topOffset: CGFloat) -> CGRect {
                
        let contentOffsetY = scrollView.contentOffset.y
        
        let height = min(max(-contentOffsetY, topOffset), parallaxView.frame.height)
        
        return CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)

    }
    
    func computeParallaxFrame(in scrollView: UIScrollView, heightMultiplier: CGFloat) -> CGRect {
        
        let topMargin = scrollView.layoutMargins.top
        
        let contentOffsetY = scrollView.contentOffset.y
        
        let height = min(max(-contentOffsetY, topMargin),
                         parallaxView.frame.height * heightMultiplier)
        
        return CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)

    }
    
}
