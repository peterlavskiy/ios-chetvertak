//
//  CardPresentCommand.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class CardPresentCommand: NavigationCommandProtocol {
    
    let decoratee: NavigationCommandProtocol
    
    let presentAnimator: PresentAnimatedTransitioning
    
    let dismissAnimator: DismissAnimatedTransitioning
    
    weak var initialView: UIView? {
        didSet {
            presentAnimator.initialView = initialView
            dismissAnimator.initialView = initialView
        }
    }
    
    var transitionDelegate: UIViewControllerTransitioningDelegate?
    
    var source: UIViewController? { decoratee.source }
    
    var presented: UIViewController? { decoratee.presented }
    
    init(decoratee: NavigationCommandProtocol,
         presentAnimator: PresentAnimatedTransitioning,
         dismissAnimator: DismissAnimatedTransitioning) {
        
        self.decoratee = decoratee
        self.presentAnimator = presentAnimator
        self.dismissAnimator = dismissAnimator
    }
    
    func makePresented() {
        decoratee.makePresented()
    }
    
    func execute() {
        
        makePresented()
        
        presented?.modalPresentationStyle = .custom
        transitionDelegate = CardAppStoreLikeTransition(presentedAnimator: presentAnimator, dismissAnimator: dismissAnimator)
        presented?.transitioningDelegate = transitionDelegate
        (source as? UIStatusBarConfigurable)?.isStatusBarHidden = true
        
        decoratee.execute()
        
    }
}

class PushCommand: NavigationCommandProtocol {
            
    var source: UIViewController?
    
    var presented: UIViewController?
    
    var destination: UIViewControllerFactory
    
    var animated: Bool = true
    
    var hidesBottomBarWhenPushed: Bool = true
    
    init(source: UIViewController?, destination: UIViewControllerFactory) {
        
        self.source = source
        self.destination = destination
    }
    
    func makePresented() {
        presented = destination.make()
        presented?.hidesBottomBarWhenPushed = hidesBottomBarWhenPushed
    }
    
    func execute() {
        
        makePresented()
        
        if let destination = presented {
            source?.navigationController?.pushViewController(destination,
                                                             animated: animated)
        }

    }
}
