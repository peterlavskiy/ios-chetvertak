//
//  TitleCellConfiguration.swift
//  PromoUI
//
//  Created by peter.lavskiy on 05/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import UIKit

struct TitleCellConfiguration: TitleCellConfigurable {
    
    var selectionStyle: UITableViewCell.SelectionStyle = .none
    
    var accessoryView: TableViewCellAccessoryView?
    
    var titleFont: UIFont
    
    var titleTextColor: UIColor
    
    var titleLabelLeadingConstant: CGFloat?
    
    var titleLabelTrailingConstant: CGFloat?
        
}
