//
//  CollectionViewReusbaleCell.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol CardHeaderBuilderProtocol {}

protocol CardFooterBuilderProtocol {
    
    func build(_ view: OutputCardFooterViewProtocol?,
               in superview: UIView,
               align vertical: NSLayoutYAxisAnchor?) -> OutputCardFooterViewProtocol?
    
}

struct OutputCardInternalFooterBuilder<View: OutputCardFooterViewProtocol>: CardFooterBuilderProtocol {

    let decoratee: OutputCardFooterBuilder<View>
    
    func build(_ view: OutputCardFooterViewProtocol?, in superview: UIView, align vertical: NSLayoutYAxisAnchor?) -> OutputCardFooterViewProtocol? {
        
        let footer = view ?? decoratee.make(in: superview, align: nil)
                
        return decoratee.build(footer, in: superview, align: nil)
    }
    
    static func make(model: OutputCardCollectionViewItemModel,
                     configuration: OutputCardUIConfiguration) -> CardFooterBuilderProtocol {
        
        typealias View = OutputCardFooterView
        
        let decoratee = OutputCardFooterBuilder<View>(model: model,
                                                configuration: configuration)
        
        return OutputCardInternalFooterBuilder<View>(decoratee: decoratee)
    }
}

struct OutputCardFooterBuilder<View: OutputCardFooterViewProtocol>: CardFooterBuilderProtocol {
    
    let model: OutputCardCollectionViewItemModel
    let configuration: OutputCardUIConfiguration
    
    func build(_ view: OutputCardFooterViewProtocol?,
                   in superview: UIView,
                   align vertical: NSLayoutYAxisAnchor?) -> OutputCardFooterViewProtocol? {
        
        let footer = view ?? make(in: superview, align: vertical)
        
        if let model = model.footer {
            footer?.configure(model)
            footer?.isHidden = false
        } else {
            footer?.isHidden = true
        }
                
        if let configuration = configuration.footer {
            footer?.configureUI(configuration)
        }

        return footer
    }
    
    func make(in superview: UIView,
              align vertical: NSLayoutYAxisAnchor?) -> OutputCardFooterViewProtocol? {
        
        guard let view = View.loadView() else { return nil }
        
        view.translatesAutoresizingMaskIntoConstraints = false
                
        superview.addSubview(view)
        
        var constraints = [view.bottomAnchor.constraint(equalTo: superview.bottomAnchor),
                           view.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
                           view.trailingAnchor.constraint(equalTo: superview.trailingAnchor)]
        
        if let vertical = vertical {
            let topAnchor = view.topAnchor.constraint(equalTo: vertical)
            constraints.append(topAnchor)
        }
        
        NSLayoutConstraint.activate(constraints)
        
        return view
    }
    
}

protocol CollectionViewReusableCell: UICollectionViewCell, ReusableView {}

protocol CollectionViewCardProtocol: CollectionViewReusableCell {
    
    func configure(footer: CardFooterBuilderProtocol?)
    
    func configure(header: CardHeaderBuilderProtocol?)
    
}
