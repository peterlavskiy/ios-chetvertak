//
//  ControlTableViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 26.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct ControlTableViewCellModel: ConfigurableImageModel {
    
    let controls: [ControlFactory]
    
}

struct ControlTableViewCellUIConfiguration {
    
    var distribution: UIStackView.Distribution = .equalSpacing
    
    var spacing: CGFloat = .zero
}

class ControlTableViewCell: UITableViewCell, ConfigurableCell {

    typealias Model = ControlTableViewCellModel
    
    typealias UIConfiguration = ControlTableViewCellUIConfiguration
    
    lazy var containerView: ControlContainerView = {
        let view = ControlContainerView()
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16.0),
            view.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16.0),
            view.topAnchor.constraint(equalTo: contentView.topAnchor),
            view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        
        return view
    }()
    
    static var reuseIdentifier: String {
        String(describing: ControlTableViewCell.self)
    }
    
    func configure(_ model: ControlTableViewCellModel,
                   imageProvider: ImageProviderProtocol?) {
        
        containerView.configure(controls: model.controls)
    }

    func configureUI(_ configuration: ControlTableViewCellUIConfiguration) {
        
        selectionStyle = .none
        backgroundColor = Color().systemBackground
        contentView.backgroundColor = Color().systemBackground
        
        containerView.stackView.distribution = configuration.distribution
        containerView.stackView.spacing = configuration.spacing
    }
}
