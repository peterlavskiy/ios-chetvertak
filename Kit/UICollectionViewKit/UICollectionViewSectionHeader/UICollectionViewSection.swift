//
//  UICollectionViewSectionFactoryProtocol.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public protocol UICollectionViewSectionFactoryProtocol: UICollectionViewComponentFactoryProtocol, UICollectionViewCompositeItemProtocol {
    
    var numberOfItems: Int { get }
}

struct UICollectionViewSection<View: CollectionViewReusableHeader>: UICollectionViewSectionFactoryProtocol {

    let builder: UICollectionViewComponentFactoryProtocol
    let items: [UICollectionViewComponent]
    
    var nib: UINib?
    var cellClass: AnyClass.Type?
    
    var isSelected: Bool { builder.isSelected }
    var reuseItem: UIReusableItemProtocol { builder.reuseItem }

    var insets: UIEdgeInsets { builder.insets }
    var numberOfItems: Int { items.count }
    var estimatedSize: CGSize { builder.estimatedSize }
    var itemSize: CGSize { builder.itemSize }
    var reuseIdentifier: String { builder.makeReuseIdentifier() }
    var command: CommandProtocol? { builder.command }
    
    subscript(_ index: Int) -> UICollectionViewComponent {
        items[index]
    }
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView? {
        builder.makeReusableView(view)
    }
    
    func make(_ view: UICollectionReusableView?) -> UICollectionReusableView {
        builder.makeReusableView(view) as? View ?? View()
    }
    
    func makeReuseIdentifier() -> String {
        builder.makeReuseIdentifier()
    }
    
    func build() -> UICollectionViewComponent {
        
        UICollectionViewCompositeItem(reuseItem: reuseItem, builder: self)
    }
}
