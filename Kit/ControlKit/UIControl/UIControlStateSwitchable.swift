//
//  UIControlStateSwitchable.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 15.08.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import Foundation

protocol UIControlStateSwitchable {
    
    var normal: UIControlState { get }
    
    func switchSelected(_ isSelected: Bool) -> UIControlState
    
    func switchHighlighted(_ isHighlighted: Bool, selected: Bool) -> UIControlState
}
