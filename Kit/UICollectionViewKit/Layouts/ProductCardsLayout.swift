//
//  ProductCardsLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 07.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

fileprivate enum MosaicSegmentStyle {
    case fullWidth
    case fiftyFifty
    case twoThirdsOneThird
    case oneThirdTwoThirds
}

class ProductCardsLayout: UICollectionViewLayout {
    
    var contentBounds = CGRect.zero
    var cachedAttributes = [UICollectionViewLayoutAttributes]()
    
    let horizontalSpacing: CGFloat
    let insets: UIEdgeInsets
    let cvWidth: CGFloat?
    let squared: Bool
    
    var itemHeight: CGFloat

    init(itemHeight: CGFloat = 234.0,
         cvWidth: CGFloat? = nil,
         insets: UIEdgeInsets = UIEdgeInsets(top: 16.0, left: 24.0, bottom: 0.0, right: 24.0),
         horizontalSpacing: CGFloat = 16.0,
         squared: Bool = false) {
        self.itemHeight = itemHeight
        self.cvWidth = cvWidth
        self.insets = insets
        self.horizontalSpacing = horizontalSpacing
        self.squared = squared
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Layout Overrides

    /// - Tag: ColumnFlowExample
    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }
        
        // Reset cached information.
        cachedAttributes.removeAll()
        contentBounds = CGRect(origin: .zero, size: collectionView.bounds.size)
        
        // For every item in the collection view:
        //  - Prepare the attributes.
        //  - Store attributes in the cachedAttributes array.
        //  - Combine contentBounds with attributes.frame.
        let count = collectionView.numberOfItems(inSection: 0)
        
        var currentIndex = 0
        var lastFrame: CGRect = .zero
        
        let cvWidth = self.cvWidth ?? collectionView.bounds.size.width
        let width = cvWidth - (insets.right + insets.left + horizontalSpacing)
        
        var itemSize = CGSize(width: width, height: itemHeight)
        
        if squared {
//            itemSize.height = width
        }
        
        while currentIndex < count {
            
            let segmentFrame = CGRect(x: insets.left,
                                      y: lastFrame.maxY + insets.top,
                                      width: itemSize.width,
                                      height: itemSize.height)
            
            var segmentRects = [CGRect]()
            let horizontalSlices = segmentFrame.dividedIntegral(fraction: 0.5,
                                                                from: .minXEdge)
            segmentRects = [horizontalSlices.first, horizontalSlices.second]
            
            // Create and cache layout attributes for calculated frames.
            for rect in segmentRects {
                let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: currentIndex, section: 0))
                attributes.frame = rect
                
                cachedAttributes.append(attributes)
                contentBounds = contentBounds.union(lastFrame)
                
                currentIndex += 1
                lastFrame = rect
            }

        }
    }
    
    
    /// - Tag: CollectionViewContentSize
    override var collectionViewContentSize: CGSize {
        return contentBounds.size
    }
    
    /// - Tag: ShouldInvalidateLayout
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        guard let collectionView = collectionView else { return false }
        return !newBounds.size.equalTo(collectionView.bounds.size)
    }
    
    /// - Tag: LayoutAttributesForItem
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cachedAttributes[indexPath.item]
    }
    
    /// - Tag: LayoutAttributesForElements
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributesArray = [UICollectionViewLayoutAttributes]()
        
        // Find any cell that sits within the query rect.
        guard let lastIndex = cachedAttributes.indices.last,
              let firstMatchIndex = binSearch(rect, start: 0, end: lastIndex) else { return attributesArray }
        
        // Starting from the match, loop up and down through the array until all the attributes
        // have been added within the query rect.
        for attributes in cachedAttributes[..<firstMatchIndex].reversed() {
            guard attributes.frame.maxY >= rect.minY else { break }
            attributesArray.append(attributes)
        }
        
        for attributes in cachedAttributes[firstMatchIndex...] {
            guard attributes.frame.minY <= rect.maxY else { break }
            attributesArray.append(attributes)
        }
        
        return attributesArray
    }
    
    // Perform a binary search on the cached attributes array.
    func binSearch(_ rect: CGRect, start: Int, end: Int) -> Int? {
        if end < start { return nil }
        
        let mid = (start + end) / 2
        let attr = cachedAttributes[mid]
        
        if attr.frame.intersects(rect) {
            return mid
        } else {
            if attr.frame.maxY < rect.minY {
                return binSearch(rect, start: (mid + 1), end: end)
            } else {
                return binSearch(rect, start: start, end: (mid - 1))
            }
        }
    }
}

fileprivate extension CGRect {
    
    func dividedIntegral(fraction: CGFloat, from fromEdge: CGRectEdge) -> (first: CGRect, second: CGRect) {
        let dimension: CGFloat
        
        switch fromEdge {
        case .minXEdge, .maxXEdge:
            dimension = self.size.width
        case .minYEdge, .maxYEdge:
            dimension = self.size.height
        }
        
        let distance = (dimension * fraction).rounded(.up)
        var slices = self.divided(atDistance: distance, from: fromEdge)
        
        switch fromEdge {
        case .minXEdge, .maxXEdge:
            slices.remainder.origin.x += 16
            slices.remainder.size.width -= 0
        case .minYEdge, .maxYEdge:
            slices.remainder.origin.y += 0
            slices.remainder.size.height -= 0
        }
        
        return (first: slices.slice, second: slices.remainder)
    }
}
