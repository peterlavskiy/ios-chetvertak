//
//  OptionalImageViewLayoutProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 12.04.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol OptionalImageViewLayoutProtocol {
    
    var superview: UIView? { get }
    var height: CGFloat { get }
    var width: CGFloat { get }
    var cornerRadii: CGFloat { get }

    var layout: OptionalImageViewLayoutProtocol { get }
    
    func make(in superview: UIView) -> UIImageView?
}

extension OptionalImageViewLayoutProtocol {
    
    var cornerRadii: CGFloat { layout.cornerRadii }
    
    var superview: UIView? { layout.superview }
    
    var height: CGFloat { layout.height }
    
    var width: CGFloat { layout.width }
    
}
