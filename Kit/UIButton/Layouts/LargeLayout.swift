//
//  LargeLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct LargeLayout: ButtonLayout {
    
    var lineWidth: CGFloat = 2.0
    
    var cornerRadii: CGFloat = 14.0
        
    var margins: UIEdgeInsets = .init(top: 14.0, left: 64.0, bottom: 14.0, right: 64.0)
    
    var isEqualScreenWidth: Bool = false

}
