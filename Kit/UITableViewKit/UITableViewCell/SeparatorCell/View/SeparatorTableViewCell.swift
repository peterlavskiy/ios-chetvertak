//
//  SeparatorTableViewCell.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 03.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class SeparatorTableViewCell: UITableViewCell, ConfigurableCell {
    
    typealias Model = SeparatorCellModel
    
    typealias UIConfiguration = SeparatorcCellConfiguration
    
    static var reuseIdentifier: String {
         String(describing: SeparatorTableViewCell.self)
    }
    
    @IBOutlet weak var lineHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorLine: UIView!
    
    func configure(_ model: SeparatorCellModel, imageProvider: ImageProviderProtocol?) {
        
    }
    
    func configureUI(_ configuration: SeparatorcCellConfiguration) {
        separatorLine.backgroundColor = configuration.lineColor
        selectionStyle = .none
    }
    
}
