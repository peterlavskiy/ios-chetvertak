//
//  RawTitleTableViewCell.swift
//  PromoUI
//
//  Created by peter.lavskiy on 03/12/2019.
//  Copyright © 2019 peter.lavskiy. All rights reserved.
//

import UIKit

protocol RawTitleTableViewCellProtocol: UIOutputTableViewCellProtocol, UIOutputTableViewCellConfigurable {}

class RawTitleTableViewCell: UITableViewCell, RawTitleTableViewCellProtocol {
    
    typealias Model = TitleCellModelProtocol
    
    typealias UIConfiguration = TitleCellConfigurable

    @IBOutlet weak var titleLabel: UILabel!
        
    @IBOutlet weak var titleLabelLeading: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabelTrailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
