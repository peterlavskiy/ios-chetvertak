//
//  SmallLayout.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 24.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct SmallLayout: ButtonLayout {
    
    var titleFont: UIFont {
        if #available(iOS 11.0, *) {
            return UIFontMetrics(forTextStyle: .body).scaledFont(for: .systemFont(ofSize: 13.0, weight: .semibold))
        } else {
            return UIFont.systemFont(ofSize: 13.0, weight: .semibold)
        }
    }
    
    var lineWidth: CGFloat = 1.0
    
    var cornerRadii: CGFloat = 4.0
    
    var margins: UIEdgeInsets = .init(top: 4.0, left: 64.0, bottom: 4.0, right: 64.0)
}
