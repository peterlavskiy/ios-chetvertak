//
//  UILabel+Configurable.swift
//  PromoUI
//
//  Created by peter.lavskiy on 21/01/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

extension UILabel {
    
    func configure(_ configuration: UILabelConfiguration) {
        
        font = configuration.font
        textColor = configuration.textColor
        layoutMargins = configuration.layoutMargins
        numberOfLines = configuration.numberOfLines
        
        makeAttributedString(configuration)
    }
    
    func makeAttributedString(_ configuration: UILabelConfiguration) {
        
        if let string = text, let attributes = configuration.attributes {

            let attributedString = NSMutableAttributedString(string: string)

            attributes.forEach {

                let range = $0.range ?? (string as NSString).range(of: string)

                attributedString.setAttributes($0.attributes, range: range)

            }

            attributedText = attributedString

        }

    }
    
}
