//
//  CardDetailTransionable.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol CardDetailTransionable: UIViewController {
    
    var cardHeightAnchor: NSLayoutConstraint { get }
        
    var cardBottomAnchor: NSLayoutYAxisAnchor { get }
    
    var cardBottomToRootBottomConstraint: NSLayoutConstraint { get }
    
    var cardTopToRootTopConstraint: NSLayoutConstraint { get }
        
    var scrollView: UIScrollView? { get }
    
    var cardView: OuputCardView? { get }
    
    var dismissButton: UIView? { get }
    
    func loadData()
    
    func viewWillDissmiss()
    
    func transitionCompleteAnimations()
    
}

extension CardDetailTransionable {
    
    func stretchToView(_ view: UIView) -> [NSLayoutConstraint] {
        
        let cardView = self.cardView ?? UIView()
        
        return cardView.edges(to: view)
        
    }
    
}
