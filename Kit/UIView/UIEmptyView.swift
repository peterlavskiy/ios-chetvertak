//
//  UIEmptyView.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 08.05.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct CTAModel {
    
    var title: String
    var titleColor: UIColor
    var backgroundColor: UIColor
    var verticalSpace: CGFloat?
    var action: CommandProtocol
    
}

struct EmtyViewModel {
    
    let title: String
    var subtitle: String?
    var image: UIImage?
    var cta: CTAModel?
    
}

class UIEmptyView: UIView {
    
    class ResizableButton: UIButton {
        
        override var intrinsicContentSize: CGSize {
            let heightInsets = titleEdgeInsets.top + titleEdgeInsets.bottom
            let widthInsets = titleEdgeInsets.left + titleEdgeInsets.right
            let titleSize = titleLabel?.intrinsicContentSize ?? .zero
            let width = titleSize.width + widthInsets
            let height = titleSize.height + heightInsets
            return .init(width: width, height: height)
        }
    }
    
    var model: EmtyViewModel?
    
    lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .center
        view.isLayoutMarginsRelativeArrangement = true
        if #available(iOS 11.0, *) {
            view.spacing = UIStackView.spacingUseSystem
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var title: UILabel = UILabel()
    weak var subtitle: UILabel?
    var imageView: UIImageView?
    var ctaButton: UIButton?
    
    init(model: EmtyViewModel, frame: CGRect,
         layoutMargins stackViewMargins: UIEdgeInsets = .zero) {
        self.model = model
        super.init(frame: frame)
        if #available(iOS 11.0, *) {
            self.stackView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: stackViewMargins.top,
                                                                              leading: stackViewMargins.left,
                                                                              bottom: stackViewMargins.bottom,
                                                                              trailing: stackViewMargins.right)
        } else {
            self.stackView.layoutMargins = stackViewMargins
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func make(image: UIImage) -> UIImageView {
        let imageView = UIImageView()
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
    
    func make(subtitle: String) -> UILabel {
        let label = UILabel()
        label.preferredMaxLayoutWidth = UIScreen.main.bounds.width / 1.5
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Color().secondaryLabel
        label.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        label.text = subtitle
        
        return label
    }
    
    func make(title: String) -> UILabel {
        let label = UILabel()
        label.preferredMaxLayoutWidth = UIScreen.main.bounds.width / 2
        label.numberOfLines = 0
        label.textColor = Color().secondaryLabel
        label.font = UIFont.systemFont(ofSize: 28.0, weight: .bold)
        label.text = title
        return label
    }
    
    func make(model: CTAModel) -> Button {
        let button = PrimaryButton(layout: LargeLayout(cornerRadii: 24.0),
                                   model: ButtonModel(title: model.title)).make()
        button.addTarget(self, action: #selector(actionCTA), for: .touchUpInside)
        return button
    }
    
    @objc func actionCTA() {
        model?.cta?.action.execute()
    }
    
    func loadView() {
        
        backgroundColor = Color().systemBackground
        
        if let image = model?.image {
            let imageView = make(image: image)
            stackView.addArrangedSubview(imageView)
            if #available(iOS 11.0, *) {
                stackView.setCustomSpacing(8.0, after: imageView)
            }
        }
        
        stackView.addArrangedSubview(make(title: model?.title ?? ""))
        if #available(iOS 11.0, *) {
            stackView.setCustomSpacing(8.0, after: title)
        }

        if let subtitle = model?.subtitle {
            let subtitle = make(subtitle: subtitle)
            stackView.addArrangedSubview(subtitle)
            if #available(iOS 11.0, *) {
                stackView.setCustomSpacing(model?.cta?.verticalSpace ?? 56.0, after: subtitle)
            }
        }

        
        if let model = model?.cta {
            stackView.addArrangedSubview(make(model: model))
        }
        
        addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }

}
