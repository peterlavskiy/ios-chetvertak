//
//  ConfigurableCell.swift
//  PromoUI
//
//  Created by peter.lavskiy on 21/01/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol CellImageable {
    
    var isImageAvaliable: Bool { get }
}

protocol ConfigurableCell: UITableViewCell, ReusableView {}
