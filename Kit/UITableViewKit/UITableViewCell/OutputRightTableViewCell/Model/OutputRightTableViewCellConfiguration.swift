//
//  OutputRightTableViewCellConfiguration.swift
//  PromoUI
//
//  Created by peter.lavskiy on 21/01/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct OutputRightTableViewCellConfiguration {
    
    var title: UILabelConfiguration
    
    var subTitle: UILabelConfiguration?
    
    var value: UILabelConfiguration?
    
    var valueBackgroundViewColor: UIColor?
    
    var valueBackgroundRadius: CGFloat = 4.0
    
    var valueBackgroundStrokeColor: UIColor?
    
    var teriary: UILabelConfiguration?
        
    var backgrounColor: UIColor = Color().systemBackground
    
    var backgroundImageHeight: CGFloat?
    
    var selectionStyle: UITableViewCell.SelectionStyle = .none
    
    var accessoryType: UITableViewCell.AccessoryType = .none
    
    var accessoryView: UIView?
    
    var contentRectCorners: UIRectCorner?
    
    var distribution: UIStackView.Distribution = .equalSpacing
    
    var margins: UIEdgeInsets = .zero
    
}

extension OutputRightTableViewCellConfiguration {
    
    static var `default`: OutputRightTableViewCellConfiguration {
        
        let titleColor: UIColor = Color().label
        let subTitleColor: UIColor = Color().secondaryLabel
        
        let valueConfiguration = UILabelConfiguration(font: .preferredFont(forTextStyle: .title3),
                                                         textColor: titleColor)
        
        let subtitleConfiguration = UILabelConfiguration(font: .preferredFont(forTextStyle: .subheadline),
                                                         textColor: subTitleColor)
        
        let titleConfiguration = UILabelConfiguration(font: .preferredFont(forTextStyle: .title3),
                                                      textColor: titleColor)
        
        return OutputRightTableViewCellConfiguration(title: titleConfiguration,
                                                     subTitle: subtitleConfiguration,
                                                     value: valueConfiguration,
                                                     selectionStyle: .none,
                                                     accessoryType: .none)
    }
    
    init(_ configuration: OutputRightTableViewCellConfiguration,
         accessoryType: UITableViewCell.AccessoryType) {
        self.title = configuration.title
        self.subTitle = configuration.subTitle
        self.value = configuration.value
        self.selectionStyle = configuration.selectionStyle
        self.accessoryType = accessoryType
    }
    
    init(_ imageLayout: LeftImageViewLayout) {
        
        let facade = Color()
        
        self.title = UILabelConfiguration(font: .preferredFont(forTextStyle: .title3),
                                          textColor: facade.label)
        self.subTitle = UILabelConfiguration(font: .preferredFont(forTextStyle: .subheadline),
                                             textColor: facade.secondaryLabel)
        self.value = UILabelConfiguration(font: .preferredFont(forTextStyle: .title3),
                                          textColor: facade.secondaryLabel)
        self.selectionStyle = .none
        self.accessoryType = .none
    }
}
