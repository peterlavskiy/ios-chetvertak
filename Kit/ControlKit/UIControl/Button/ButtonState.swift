//
//  ButtonState.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 15.08.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct ControlState: UIControlState {
    
    var backgroundColor: UIColor
    
    var textColor: UIColor
}

struct ButtonState: UIControlStateSwitchable {
    
    var normal: UIControlState = ControlState(backgroundColor: Color().systemBackground,
                                              textColor: Color().label)
    var selected: UIControlState?
    var highlighted: UIControlState?
    
    func switchSelected(_ isSelected: Bool) -> UIControlState {
        isSelected ? (selected ?? normal) : normal
    }
    
    func switchHighlighted(_ isHighlighted: Bool, selected: Bool) -> UIControlState {
        isHighlighted ? (highlighted ?? normal) : switchSelected(selected)
    }
}
