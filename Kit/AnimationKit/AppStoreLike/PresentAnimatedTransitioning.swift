//
//  PresentAnimatedTransitioning.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

class PresentAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    let transitionDuration: Double = 0.8
    let shrinkDuration: Double = 0.2
    
    let timeInterval: TimeInterval
    var retriever: ContextRetriever?
    
    weak var toViewController: CardDetailTransionable?
    
    private let whiteScrollView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private var isDebugMode = false
    
    private var onScreenFrame: CGRect = .zero
    
    private var animator: UIViewPropertyAnimator = UIViewPropertyAnimator()
    
    private var constraints: [NSLayoutConstraint] = []
    
    weak var initialView: UIView?
    
    init(timeInterval: TimeInterval, initialView: UIView? = nil) {
        self.timeInterval = timeInterval
        self.initialView = initialView
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        timeInterval
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        retriever = ContextRetriever(context: transitionContext)
        
        let fromViewController = retriever?.fromViewController as? UITabBarController
        let tabBarFrame = fromViewController?.tabBar.frame ?? .zero
        
        let containerView = retriever?.containerView ?? UIView()
        let toView = retriever?.toView ?? UIView()
        let cardView = initialView as? OutputCardCollectionViewCell ?? OutputCardCollectionViewCell()
        
        let toViewController = retriever?.toViewController as? CardDetailTransionable
        self.toViewController = toViewController
        let cardPresentation = cardView.layer.presentation()?.frame ?? .zero
        
        onScreenFrame = cardView.superview?.convert(cardPresentation, to: nil) ?? .zero
        
        let animatedContainerView = UIView()
        animatedContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        if isDebugMode {
            animatedContainerView.layer.borderColor = UIColor.yellow.cgColor
            animatedContainerView.layer.borderWidth = 4
            toView.layer.borderColor = UIColor.red.cgColor
            toView.layer.borderWidth = 2
        }
        
        containerView.addSubview(animatedContainerView)
        
        let animatedContainerConstraints = [
            animatedContainerView.widthAnchor.constraint(equalToConstant: containerView.bounds.width),
            animatedContainerView.heightAnchor.constraint(equalToConstant: containerView.bounds.height),
            animatedContainerView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ]
        NSLayoutConstraint.activate(animatedContainerConstraints)
        
        let centerConstant = (cardView.superview?.convert(cardView.center, to: nil).y ?? .zero) - containerView.bounds.height / 2
        let animatedContainerVerticalConstraint = animatedContainerView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: centerConstant)
        
        animatedContainerVerticalConstraint.isActive = true
        
        animatedContainerView.addSubview(toView)
        toView.translatesAutoresizingMaskIntoConstraints = false
        
        let verticalAnchor = toView.centerYAnchor.constraint(equalTo: animatedContainerView.centerYAnchor)
        verticalAnchor.isActive = true
        toView.centerXAnchor.constraint(equalTo: animatedContainerView.centerXAnchor).isActive = true
        
        let cardWidthConstraint = toView.widthAnchor.constraint(equalToConstant: onScreenFrame.width)
        let cardHeightConstraint = toView.heightAnchor.constraint(equalToConstant: onScreenFrame.height)
        
        NSLayoutConstraint.activate([cardWidthConstraint, cardHeightConstraint])
        toView.clipsToBounds = true
        
        let constraints = toViewController?.stretchToView(toView) ?? []
        
        cardView.isHidden = true
        containerView.layoutIfNeeded()
        
        toView.layer.cornerRadius = cardView.layer.cornerRadius

        func animateContainerBouncingUp() {
            animatedContainerVerticalConstraint.constant = 0
            fromViewController?.setNeedsStatusBarAppearanceUpdate()
            containerView.layoutIfNeeded()
        }
        
        func animateCardDetailViewSizing() {
            toViewController?.setNeedsStatusBarAppearanceUpdate()
            toView.removeConstraints(constraints)
            cardWidthConstraint.constant = animatedContainerView.bounds.width
            cardHeightConstraint.constant = animatedContainerView.bounds.height
            toView.layer.cornerRadius = 0
            
            fromViewController?.tabBar.frame = fromViewController?.tabBar.frame.offsetBy(dx: 0, dy: tabBarFrame.height) ?? .zero
                                    
            containerView.layoutIfNeeded()
        }
        
        func completeEverything() {
            
            animatedContainerView.removeConstraints(animatedContainerView.constraints)
            animatedContainerView.removeFromSuperview()
            
            containerView.addSubview(toView)
            
            toView.removeConstraints([cardWidthConstraint, cardHeightConstraint])
            
            toView.edges(to: containerView)
             
            let success = !transitionContext.transitionWasCancelled
            transitionContext.completeTransition(success)
        }
        
        let baseAnimator: UIViewPropertyAnimator = .createBaseSpringAnimator(cardFrame: onScreenFrame)
        self.animator = baseAnimator
        
        baseAnimator.addAnimations {
            
            animateContainerBouncingUp()
                        
            let cardExpanding = UIViewPropertyAnimator(duration: baseAnimator.duration * 0.6, curve: .linear) {
                animateCardDetailViewSizing()
            }
            cardExpanding.startAnimation()
            
        }
        
        baseAnimator.addCompletion { _ in
            completeEverything()
        }
                
        interruptibleAnimator(using: transitionContext).startAnimation()
        
        toViewController?.loadData()
    }
    
    func interruptibleAnimator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        
        animator
    }
    
    func animationEnded(_ transitionCompleted: Bool) {
        
        toViewController?.transitionCompleteAnimations()
    }
}
