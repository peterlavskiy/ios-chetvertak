//
//  ImageProviderProtocol.swift
//  PromoUI
//
//  Created by peter.lavskiy on 24/04/2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol ImageOperationProtocol: AnyObject {
    
    func cancel()
}

protocol ImageProviderProtocol {
    
    var imageContentMode: UIImageView.ContentMode { get }
    
    var leftImageView: LeftImageViewLayout? { get }
    
    func getImage(_ url: URL?,
                  completion: @escaping (UIImage?) -> Void) -> ImageOperationProtocol?
    
    @discardableResult
    func getImage(completion: ((UIImage?) -> Void)?) -> ImageOperationProtocol?
    
    func getAssetImage() -> UIImage?

    func getAssetImage(named: String?) -> UIImage?
    
}
