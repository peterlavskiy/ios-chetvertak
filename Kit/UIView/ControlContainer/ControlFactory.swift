//
//  ControlFactory.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 29.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

protocol ControlFactory {
    
    func make() -> UIControl
    
}
