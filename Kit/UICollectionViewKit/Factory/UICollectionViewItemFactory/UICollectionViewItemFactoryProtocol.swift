//
//  UICollectionViewItemFactoryProtocol.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 17.06.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

public protocol UICollectionViewItemFactoryProtocol: UICollectionViewComponentFactoryProtocol, UICollectionViewItemProtocol {
    
    var builder: UICollectionViewComponentFactoryProtocol { get }
}

extension UICollectionViewItemFactoryProtocol {
    
    var insets: UIEdgeInsets { builder.insets }
    var estimatedSize: CGSize { builder.estimatedSize }
    var itemSize: CGSize { builder.itemSize }
    var reuseIdentifier: String { builder.reuseIdentifier }
    var command: CommandProtocol? { builder.command }
    
    func makeReusableView<T: UIView>(_ view: T?) -> UIView? {
        builder.makeReusableView(view)
    }
    
    func makeReuseIdentifier() -> String {
        builder.makeReuseIdentifier()
    }
    
}
