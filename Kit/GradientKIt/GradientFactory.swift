//
//  GradientFactory.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 22.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import UIKit

struct GradientFactory: GradientFactoryProtocol {
    
    let colors: [Any]
    
    var transform: CATransform3D? = CATransform3DMakeRotation(CGFloat.pi / 2, 0, 0, 1)
    
    func make(in view: UIView) -> CAGradientLayer {
        
        let layer = CAGradientLayer()
        
        layer.colors = colors
        
        if let transform = transform {
            layer.transform = transform
        }
        
        layer.bounds = view.bounds
        
        return layer
    }
    
}
