//
//  AllResultsViewController.swift
//  Chetvertak
//
//  Created by Lavskiy Peter on 16.08.2020.
//  Copyright © 2020 Lavskiy Peter. All rights reserved.
//

import UIKit

struct AllResultsFactory: UIViewControllerFactory {
    
    let colorProvider: ColorProvider
    
    func makeViewController() -> UIViewController {
        let viewController = AllResultsViewController(colorProvider: colorProvider)
        
        return viewController
    }
    
}

class AllResultsViewController: UIViewController {
    
    let composer = UITableViewComposer()
    
    let service = CompetitionsService()
    
    let colorProvider: ColorProvider
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .none
        }
    }
    
    init(colorProvider: ColorProvider) {
        self.colorProvider = colorProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = composer
        tableView.delegate = composer
        
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.titleView = TitleViewFactory(title: "Результаты", subtitle: "Соревнований").make()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        class SegmentCommand: CommandProtocol {
            
            func execute() {
                
            }
            
        }
        
        let content = ["Все", "Россия", "Международные", "Региональные"].map {
            UISegment(title: $0, command: SegmentCommand())
        }
        let segmentView = UISegmentView(frame: .init(origin: .zero, size: .init(width: UIScreen.main.bounds.width, height: 52)))
        segmentView.layoutMargins = .init(top: 8.0, left: 16.0, bottom: 0, right: 16.0)
        segmentView.configure()
        segmentView.setContent(content)
        tableView.tableHeaderView = segmentView
        
        segmentView.setSelectedIndex(0)
        
        loadData()
    }
    
    func loadData() {
        
        service.getProducts { [weak self] in
            switch $0 {
            case .success(let entity):
                let content = [makeCompetitionSection(entity)]
                self?.composer.setContent(content)
                self?.tableView?.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func makeCompetitionSection(_ content: CompetitionResponseEntity) -> UITableViewComponent {
        
        typealias View = UITableViewSectionHeader
        
        let model = UITableViewSectionHeader.Model(title: "")
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .title2).scaledFont(for: .systemFont(ofSize: 22.0, weight: .bold)), textColor: Color().label)
        
        let configuration = UITableViewSectionHeader.UIConfiguration(title: titleConfiguration)
        
        let items = content.result.enumerated().map(makeCompetitionCell)
        
        let factory = UITableViewFactoryComponent<View>(model: model,
                                                        configuration: configuration,
                                                        estimatedHeight: .zero)
        
        return factory.buildSection(View.self, items: items)
        
    }
    
    func makeCompetitionCell(offset: Int, content: CompetitionEntity) -> UITableViewComponent {
        
        typealias View = OutputRightTableViewCell
        
        let title = DateFactory(content: content).makeString().uppercased()
        
        let command = PushCommand(source: self, destination: ResultsCompetitionsFactory(colorProvider: colorProvider))
        
        let imageLayout = LeftImageViewLayout(leadingConstant: 16, traillingConstant: 16.0, topConstant: 16.0, layout: ImageViewLayout(height: 64, width: 64, cornerRadii: 32.0))
        let imageProvider = MainImageProvider(imageName: content.imageURL, leftImageView: imageLayout)
        
        let model = OutputRightTableViewCellModel(title: title,
                                                  subTitle: content.title,
                                                  value: content.discipline.title,
                                                  teriary: "📍\(content.location.title)")
        
        let backgroundColor = Color().systemBackground
        
        let titleConfiguration: UILabelConfiguration = .makeTitle(font: UIFontMetrics(forTextStyle: .footnote).scaledFont(for: .systemFont(ofSize: 13.0, weight: .semibold)), textColor: Color().systemIndigo)
        
        let subtitleConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: .systemFont(ofSize: 15.0, weight: .semibold)), textColor: Color().label, numberOfLines: 1)
        
        let valueConfiguration: UILabelConfiguration = .init(font: UIFontMetrics(forTextStyle: .caption1).scaledFont(for: .systemFont(ofSize: 12.0, weight: .semibold)), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let teriaryConfiguration: UILabelConfiguration = .makeTeriary(font: UIFontMetrics(forTextStyle: .caption2).scaledFont(for: .systemFont(ofSize: 10.0, weight: .regular)), textColor: Color(alpha: 0.6).secondaryLabel)
        
        let configuration: OutputRightTableViewCellConfiguration = .init(title: titleConfiguration, subTitle: subtitleConfiguration, value: valueConfiguration, valueBackgroundViewColor: Color().systemGray6, valueBackgroundStrokeColor: Color().systemGray4, teriary: teriaryConfiguration, backgrounColor: backgroundColor)
        
        return UITableViewFactoryComponent<View>(model: model,
                                                 configuration: configuration,
                                                 estimatedHeight: 98.0,
                                                 imageProvider: imageProvider, command: command).buildCell(View.self)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        loadData()
    }
    
}
