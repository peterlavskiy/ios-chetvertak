//
//  OutputCardModel.swift
//  PromoUI
//
//  Created by Lavskiy Peter on 09.07.2020.
//  Copyright © 2020 peter.lavskiy. All rights reserved.
//

import Foundation

protocol OutputCardHeaderModelProtocol {}

protocol OutputCardFooterModelProtocol {}

protocol OutputCardModelProtocol {
    
    var footer: OutputCardFooterModelProtocol { get }
    
    var header: OutputCardFooterModelProtocol { get }
    
}

struct OutputCardModel: OutputCardModelProtocol {
    
    var footer: OutputCardFooterModelProtocol
    
    var header: OutputCardFooterModelProtocol
    
}
